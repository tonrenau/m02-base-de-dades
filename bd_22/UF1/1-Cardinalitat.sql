Les següents interrelacions són 1:1, 1:N o N:M? (Per aquest exercici considerarem que només
volem guardar la informació actual, no tindrem en compte anys anteriors, cursos anteriors o adreces
anteriors)
1. Cotxes i Matrícules  1:1
pregunta :
    UN cotxe esta relacionat amb UNA matricula
    UNA matricula esta relacionade amb UN cotxe 
2. Cotxes i multes  1:N      
pregunta :
    ...
    UNA multa esta relacionada amb N(molts) cotxes 
3. Productes i fabricants   1:N
4. Clients i venedors   1:N
5. Productes i venedors N:M
6. Productes i factures N:M
7. Productes i clients  N:M
8. Productes i TipusProductes   N:1
9. Clients i TipusClients   N:1
10. Clients i Localitat     1:N
11. Alumnat i CiclesFormatius N:M
12. Alumnat i Assignatures  N:M
13. Alumnat i Professorat   N:M
14. Alumnat i CentresEducatius  N:M
15. Usuaris i missatges de mail enviats 1:N
16. Usuaris i missatges de mail rebuts  N:M
17. Pacients i doctors  N:M
18. Doctors i especialitats N:M
19. Pacients i proves mèdiques  N:M
20. Pacients i centres d''atenció mèdica    N:M