Exercici Olimpiadas

Las sedes olímpicas se dividen en complejos deportivos (edificios). Los complejos deportivos se subdividen en aquellos en los que se desarrolla un único deporte y en los polideportivos. Los complejos polideportivos tienen áreas designadas para cada deporte con un indicador de localización (ejemplo: centro, esquinaNE, etc.). Un complejo tiene una localización, un jefe de organización individual y un área total ocupada. Los dos tipos de complejos (deporte único y polideportivo) tendrán diferentes tipos de información. 

Para cada tipo de sede, se conservará el número de complejos junto con su presupuesto aproximado. Cada complejo celebra una serie de eventos (ejemplo: la pista del estadio puede celebrar muchas carreras distintas.). 

Para cada evento está prevista una fecha, duración, número de participantes, número de comisarios. Una lista de todos los comisarios se conservará junto con la lista de los eventos en los que esté involucrado cada comisario ya sea cumpliendo la tarea de juez u observador. 

Tanto para cada evento como para el mantenimiento se necesitará cierto equipamiento (ejemplo: arcos, pértigas, barras paralelas, etc)




Academia

Diseñad un esquema E/R para gestionar los datos de una academia:

Se dan clases a trabajadores y desempleados. Los datos que se almacenan de los alumnos son el DNI, dirección, nombre, teléfono, edad y estudios.
Además de los que trabajan necesitamos saber el CIF, nombre, teléfono, dirección de la empresa en la que trabajan, el cargo que tienen y los años que llevan en la misma. De los desempleados el tiempo que llevan desempleados y el último trabajo que desempeñaron.
Los cursos que imparte la academia se identifican con un código de curso. Además se almacena el nombre, programa,las horas de duración del curso, y cada vez que se imparte se anotará las fechas de inicio y fin del curso junto con un número concreto de curso (distinto del código) y los datos del profesor o profesora (sólo uno por curso) que son: dni, nombre, dirección, teléfono y especialidad.
Se almacena la nota obtenida por cada alumno en cada curso teniendo en cuenta que un mismo alumno o alumna puede realizar varios cursos.
