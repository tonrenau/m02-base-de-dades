## Exercisis de examen fallats 

## EX 9 
Seleccionar les oficines de la regio 'est' amb uners ventes que nio siguin inferiors al 10% de lobjectiu ni inferiors a 400.000. 

SELECT * 
FROM oficines 
WHERE (regio = 'Est') 
AND NOT (vendes < objectiu*0.1) 
AND (vendes >= 400.000);
 oficina |  ciutat  | regio | director | objectiu  |  vendes   
---------+----------+-------+----------+-----------+-----------
      11 | New York | Est   |      106 | 575000.00 | 692637.00
      12 | Chicago  | Est   |      104 | 800000.00 | 735042.00
      13 | Atlanta  | Est   |      105 | 350000.00 | 367911.00
(3 rows)


## EX 10 
seleccionar els clients que tenen un nom dempresa que comenña per 'C' o per 'M' o per 'R' o per 'O' conte 'et' o 'at' o 'st' o 'ts' i no conte ni 'em' ni 'aa' 'se' 

 SELECT empresa 
 FROM clients 
 WHERE (empresa LIKE 'C%' OR empresa  LIKE 'M%' OR empresa LIKE 'R%' OR empresa LIKE 'O%')                                                                                
AND ( empresa LIKE '%at%' OR empresa LIKE '%at%' OR empresa LIKE '%st%' OR empresa LIKE '%ts%') 
AND NOT (empresa LIKE '%em%' OR empresa LIKE '%ni%' OR empresa LIKE '%se%');

     empresa     
-----------------
 Chen Associates
(1 row)


