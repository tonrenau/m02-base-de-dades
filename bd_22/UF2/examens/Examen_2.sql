EX1: 

SELECT empresa,descripcio, quantitat,preu 
FROM comandes JOIN clients 
ON clie = num_clie
JOIN productes 
On fabricant = id_fabricant AND producte = id_producte 
WHERE quantitat > 5 AND preu <= 1000 
AND descripcio LIKE '% % %' ;
      empresa      |    descripcio    | quantitat |  preu  
-------------------+------------------+-----------+--------
 JCP Inc.          | Article Tipus 3  |        35 | 107.00
 First Corp.       | Article Tipus 4  |        34 | 117.00
 Acme Mfg.         | Article Tipus 4  |        28 | 117.00
 Acme Mfg.         | Article Tipus 4  |         6 | 117.00
 Acme Mfg.         | Article Tipus 2  |        54 |  76.00
 Ace International | V Stago Trinquet |         8 |  79.00
 Midwest Systems   | Article Tipus 2  |        10 |  76.00
 Holm & Landis     | Peu de rei       |         6 |  25.00
 Fred Lewis Corp.  | V Stago Trinquet |        24 |  79.00
 JCP Inc.          | Peu de rei       |        24 |  25.00
(10 rows)

EX2: 

SELECT empresa , fabricant, id_producte, preu, rep_vendes.nom, capo.nom
FROM productes JOIN comandes 
ON id_fabricant = fabricant AND id_producte = producte 
JOIN clients ON clie = num_clie 
JOIN rep_vendes ON rep = rep_vendes. num_empl
LEFT JOIN rep_vendes AS capo ON rep_vendes.cap = capo.num_empl 
WHERE (preu < 80 OR preu > 1000) AND (id_fabricant = 'imm' OR id_fabricant = 'rei')
training-> ORDER BY 1,2,3;
      empresa      | fabricant | id_producte |  preu   |      nom      |     nom     
-------------------+-----------+-------------+---------+---------------+-------------
 Ace International | rei       | 2a45c       |   79.00 | Tom Snyder    | Dan Roberts
 Chen Associates   | imm       | 775c        | 1425.00 | Nancy Angelli | Larry Fitch
 Fred Lewis Corp.  | rei       | 2a45c       |   79.00 | Sue Smith     | Larry Fitch
 Holm & Landis     | imm       | 779c        | 1875.00 | Mary Jones    | Sam Clark
 Ian & Schmidt     | rei       | 2a44r       | 4500.00 | Dan Roberts   | Bob Smith
 J.P. Sinclair     | rei       | 2a44l       | 4500.00 | Sam Clark     | 
 Rico Enterprises  | imm       | 779c        | 1875.00 | Sue Smith     | Larry Fitch
 Zetacorp          | rei       | 2a44r       | 4500.00 | Larry Fitch   | Sam Clark
(8 rows)


EX3:

SELECT nom, num_comanda, descripcio, preu, empresa
FROM comandes JOIN productes 
ON fabricant = id_fabricant AND producte = id_producte
JOIN clients ON clie = num_clie 
JOIN rep_vendes ON rep_clie = num_empl 
WHERE NOT (nom LIKE 'S%' OR nom LIKE 'I%' OR nom LIKE 'J%') AND import > 4000;
     nom     | num_comanda |   descripcio    |  preu   |      empresa      
-------------+-------------+-----------------+---------+-------------------
 Tom Snyder  |      110036 | Muntador        | 2500.00 | Ace International
 Larry Fitch |      113045 | Frontissa Dta.  | 4500.00 | Zetacorp
 Bill Adams  |      113027 | Article Tipus 2 |   76.00 | Acme Mfg.
 Paul Cruz   |      113069 | Riosta 1-Tm     | 1425.00 | Chen Associates
 Mary Jones  |      113003 | Riosta 2-Tm     | 1875.00 | Holm & Landis
 Bill Adams  |      112987 | Extractor       | 2750.00 | Acme Mfg.
 Bob Smith   |      113042 | Frontissa Dta.  | 4500.00 | Ian & Schmidt
(7 rows)

EX4:

SELECT nom, regio, ciutat 
FROM oficines RIGHT JOIN rep_vendes 
ON oficina = oficina_rep 
WHERE ((regio = 'Est' AND edat > 35) OR (regio = 'Oest' AND edat < 60 ) OR (regio IS NULL AND edat > 40 AND edat < 50)) AND (nom ILIKE '%m%' OR nom ILIKE '%n%'); 
      nom      | regio |   ciutat    
---------------+-------+-------------
 Bill Adams    | Est   | Atlanta
 Sue Smith     | Oest  | Los Angeles
 Sam Clark     | Est   | New York
 Dan Roberts   | Est   | Chicago
 Tom Snyder    |       | 
 Nancy Angelli | Oest  | Denver
(6 rows)

EX5: 

SELECT import, empresa, rep_vendes.nom, rep_client.nom, oficina_rep 
FROM  comandes 
JOIN rep_vendes ON rep = num_empl 
JOIN clients ON clie = num_clie
JOIN rep_vendes AS rep_client 
ON clients.rep_clie = rep_client.num_empl 
WHERE rep_clie <> rep;


EX6: perfect



EX7:

SELECT num_comanda, regio, nom, import,empresa
FROM comandes JOIN clients ON clie = num_clie
JOIN rep_vendes ON rep = num_empl 
JOIN oficines ON oficina_rep = oficina
ORDER BY 1,2;
 num_comanda | regio |      nom      |  import  |     empresa      
-------------+-------+---------------+----------+------------------
      112961 | Est   | Sam Clark     | 31500.00 | J.P. Sinclair
      112963 | Est   | Bill Adams    |  3276.00 | Acme Mfg.
      112968 | Est   | Dan Roberts   |  3978.00 | First Corp.
      112975 | Est   | Paul Cruz     |  2100.00 | JCP Inc.
      112979 | Oest  | Sue Smith     | 15000.00 | Orion Corp
      112983 | Est   | Bill Adams    |   702.00 | Acme Mfg.
      112987 | Est   | Bill Adams    | 27500.00 | Acme Mfg.
      112989 | Est   | Sam Clark     |  1458.00 | Jones Mfg.
      112992 | Oest  | Larry Fitch   |   760.00 | Midwest Systems
      112993 | Oest  | Sue Smith     |  1896.00 | Fred Lewis Corp.
      112997 | Oest  | Nancy Angelli |   652.00 | Peter Brothers
      113003 | Est   | Mary Jones    |  5625.00 | Holm & Landis
      113007 | Oest  | Larry Fitch   |  2925.00 | Zetacorp
      113012 | Est   | Bill Adams    |  3745.00 | JCP Inc.
      113013 | Oest  | Larry Fitch   |   652.00 | Midwest Systems
      113024 | Oest  | Larry Fitch   |  7100.00 | Orion Corp
      113027 | Est   | Bill Adams    |  4104.00 | Acme Mfg.
      113042 | Est   | Dan Roberts   | 22500.00 | Ian & Schmidt
      113045 | Oest  | Larry Fitch   | 45000.00 | Zetacorp
      113048 | Oest  | Sue Smith     |  3750.00 | Rico Enterprises
      113049 | Oest  | Larry Fitch   |   776.00 | Midwest Systems
      113051 | Oest  | Larry Fitch   |  1420.00 | Midwest Systems
      113055 | Est   | Dan Roberts   |   150.00 | Holm & Landis
      113057 | Est   | Paul Cruz     |   600.00 | JCP Inc.
      113058 | Est   | Mary Jones    |  1480.00 | Holm & Landis
      113062 | Oest  | Nancy Angelli |  2430.00 | Peter Brothers
      113065 | Oest  | Sue Smith     |  2130.00 | Fred Lewis Corp.
      113069 | Oest  | Nancy Angelli | 31350.00 | Chen Associates
(28 rows)


EX8:

SELECT empresa, ass.nom, capo.nom, oficina, ciutat, dir.nom
FROM clients JOIN rep_vendes AS ass 
ON rep_clie = ass.num_empl 
LEFT JOIN rep_vendes AS capo 
ON ass.cap = capo.num_empl
LEFT JOIN oficines 
ON ass.oficina_rep = oficina
LEFT JOIN rep_vendes AS dir 
ON director = dir.num_empl ;

      empresa      |      nom      |     nom     | oficina |   ciutat    |     nom     
-------------------+---------------+-------------+---------+-------------+-------------
 JCP Inc.          | Paul Cruz     | Bob Smith   |      12 | Chicago     | Bob Smith
 First Corp.       | Dan Roberts   | Bob Smith   |      12 | Chicago     | Bob Smith
 Acme Mfg.         | Bill Adams    | Bob Smith   |      13 | Atlanta     | Bill Adams
 Carter & Sons     | Sue Smith     | Larry Fitch |      21 | Los Angeles | Larry Fitch
 Ace International | Tom Snyder    | Dan Roberts |         |             | 
 Smithson Corp.    | Dan Roberts   | Bob Smith   |      12 | Chicago     | Bob Smith
 Jones Mfg.        | Sam Clark     |             |      11 | New York    | Sam Clark
 Zetacorp          | Larry Fitch   | Sam Clark   |      21 | Los Angeles | Larry Fitch
 QMA Assoc.        | Paul Cruz     | Bob Smith   |      12 | Chicago     | Bob Smith
 Orion Corp        | Sue Smith     | Larry Fitch |      21 | Los Angeles | Larry Fitch
 Peter Brothers    | Nancy Angelli | Larry Fitch |      22 | Denver      | Larry Fitch
 Holm & Landis     | Mary Jones    | Sam Clark   |      11 | New York    | Sam Clark
 J.P. Sinclair     | Sam Clark     |             |      11 | New York    | Sam Clark
 Three-Way Lines   | Bill Adams    | Bob Smith   |      13 | Atlanta     | Bill Adams
 Rico Enterprises  | Sue Smith     | Larry Fitch |      21 | Los Angeles | Larry Fitch
 Fred Lewis Corp.  | Sue Smith     | Larry Fitch |      21 | Los Angeles | Larry Fitch
 Solomon Inc.      | Mary Jones    | Sam Clark   |      11 | New York    | Sam Clark
 Midwest Systems   | Larry Fitch   | Sam Clark   |      21 | Los Angeles | Larry Fitch
 Ian & Schmidt     | Bob Smith     | Sam Clark   |      12 | Chicago     | Bob Smith
 Chen Associates   | Paul Cruz     | Bob Smith   |      12 | Chicago     | Bob Smith
 AAA Investments   | Dan Roberts   | Bob Smith   |      12 | Chicago     | Bob Smith
(21 rows)


