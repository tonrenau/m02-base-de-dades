# Ton Renau Mirambell  
# EXAM A
2 punts cada exercici 

## 1 [1p]


SELECT id_fabricant, id_producte, descripcio, ciutat, COUNT(*)
FROM productes JOIN comandes ON id_fabricant = fabricant AND id_producte = producte
JOIN rep_vendes ON rep = num_empl
JOIN oficines ON oficina_rep = oficina   
WHERE preu >= 250
GROUP BY 1,2,3,4;

 id_fabricant | id_producte |     descripcio     |   ciutat    | count 
--------------+-------------+--------------------+-------------+-------
 aci          | 4100y       | Extractor          | Atlanta     |     1
 aci          | 4100z       | Muntador           | Los Angeles |     1
 bic          | 41003       | Manovella          | Denver      |     1
 bic          | 41003       | Manovella          | Los Angeles |     1
 imm          | 773c        | Riosta 1/2-Tm      | Los Angeles |     1
 imm          | 775c        | Riosta 1-Tm        | Denver      |     1
 imm          | 779c        | Riosta 2-Tm        | Los Angeles |     1
 imm          | 779c        | Riosta 2-Tm        | New York    |     1
 qsa          | xk47        | Reductor           | Los Angeles |     3
 rei          | 2a44g       | Passador Frontissa | Chicago     |     1
 rei          | 2a44l       | Frontissa Esq.     | New York    |     1
 rei          | 2a44r       | Frontissa Dta.     | Chicago     |     1
 rei          | 2a44r       | Frontissa Dta.     | Los Angeles |     1
(13 rows)


[solucio ex1]-----------------------------------------------------------------------------

SELECT id_fabricant, id_producte, descripcio, SUM(quantitat), oficina_rep, coalesce( ciutat, 'No venut enlloc') 
FROM productes LEFT JOIN comandes ON id_fabricant = fabricant AND id_producte = producte
JOIN rep_vendes ON rep = num_empl
JOIN oficines ON oficina_rep = oficina   
WHERE preu >= 250
GROUP BY 1,2,5, ciutat
ORDER BY 1,2,3;

 id_fabricant | id_producte |     descripcio     | sum | oficina_rep |  coalesce   
--------------+-------------+--------------------+-----+-------------+-------------
 aci          | 4100y       | Extractor          |  11 |          13 | Atlanta
 aci          | 4100z       | Muntador           |   6 |          21 | Los Angeles
 bic          | 41003       | Manovella          |   1 |          21 | Los Angeles
 bic          | 41003       | Manovella          |   1 |          22 | Denver
 imm          | 773c        | Riosta 1/2-Tm      |   3 |          21 | Los Angeles
 imm          | 775c        | Riosta 1-Tm        |  22 |          22 | Denver
 imm          | 779c        | Riosta 2-Tm        |   3 |          11 | New York
 imm          | 779c        | Riosta 2-Tm        |   2 |          21 | Los Angeles
 qsa          | xk47        | Reductor           |  28 |          21 | Los Angeles
 rei          | 2a44g       | Passador Frontissa |   6 |          12 | Chicago
 rei          | 2a44l       | Frontissa Esq.     |   7 |          11 | New York
 rei          | 2a44r       | Frontissa Dta.     |   5 |          12 | Chicago
 rei          | 2a44r       | Frontissa Dta.     |  10 |          21 | Los Angeles
(13 rows)

------------------------------------------------------------------------------------------------

## 2 [2p]


SELECT rep_vendes.nom, ciutat, capo.nom as cap, COUNT( DISTINCT clie) as total_clients, COUNT(*) as total_com 
FROM rep_vendes JOIN oficines ON oficina_rep = oficina
JOIN rep_vendes AS capo ON rep_vendes.cap = capo.num_empl
JOIN comandes ON rep_vendes.num_empl = rep 
WHERE NOT ( ciutat = 'New York' OR ciutat = 'Atlanta') 
AND (fabricant = 'imm' OR fabricant = 'aci' Or fabricant = 'rei' )
GROUP BY 1,2,3;

      nom      |   ciutat    |     cap     | total_clients | total_com 
---------------+-------------+-------------+---------------+-----------
 Dan Roberts   | Chicago     | Bob Smith   |             3 |         3
 Larry Fitch   | Los Angeles | Sam Clark   |             2 |         3
 Nancy Angelli | Denver      | Larry Fitch |             1 |         1
 Paul Cruz     | Chicago     | Bob Smith   |             1 |         2
 Sue Smith     | Los Angeles | Larry Fitch |             3 |         3
(5 rows)



## 3 [0p]


SELECT ciutat, SUM(import) as import, num_empl, oficina 
FROM oficines JOIN rep_vendes ON oficina = oficina_rep
JOIN  comandes ON rep = num_empl 
GROUP BY 1,3,4 
ORDER BY import DESC;
   ciutat    |  import  | num_empl | oficina 
-------------+----------+----------+---------
 Los Angeles | 58633.00 |      108 |      21
 Atlanta     | 39327.00 |      105 |      13
 Denver      | 34432.00 |      107 |      22
 New York    | 32958.00 |      106 |      11
 Chicago     | 26628.00 |      101 |      12
 Los Angeles | 22776.00 |      102 |      21
 New York    |  7105.00 |      109 |      11
 Chicago     |  2700.00 |      103 |      12
(8 rows)

[Solucio ex3] ------------------------------------------------------------------------------





## 4 [0p]

SELECT DISTINCT nom, CAST(import/quantitat AS NUMERIC (8,2)) AS preu, MAX(preu) 
FROM rep_vendes LEFT JOIN comandes ON rep = num_empl
JOIN productes ON id_fabricant = fabricant AND id_producte = producte 
GROUP BY 1,2 
ORDER BY 2 DESC ;
      nom      |  preu   |   max   
---------------+---------+---------
 Dan Roberts   | 4500.00 | 4500.00
 Larry Fitch   | 4500.00 | 4500.00
 Sam Clark     | 4500.00 | 4500.00
 Bill Adams    | 2500.00 | 2750.00
 Sue Smith     | 2500.00 | 2500.00
 Tom Snyder    | 2500.00 | 2500.00
 Mary Jones    | 1875.00 | 1875.00
 Sue Smith     | 1875.00 | 1875.00
 Nancy Angelli | 1425.00 | 1425.00
 Larry Fitch   |  975.00 |  975.00
 Larry Fitch   |  652.00 |  652.00
 Nancy Angelli |  652.00 |  652.00
 Larry Fitch   |  388.00 |  355.00
 Larry Fitch   |  355.00 |  355.00
 Sue Smith     |  355.00 |  355.00
 Paul Cruz     |  350.00 |  350.00
 Nancy Angelli |  243.00 |  243.00
 Sam Clark     |  243.00 |  243.00
 Mary Jones    |  148.00 |  148.00
 Bill Adams    |  117.00 |  117.00
 Dan Roberts   |  117.00 |  117.00
 Bill Adams    |  107.00 |  107.00
 Sue Smith     |   79.00 |   79.00
 Tom Snyder    |   79.00 |   79.00
 Bill Adams    |   76.00 |   76.00
 Larry Fitch   |   76.00 |   76.00
 Dan Roberts   |   25.00 |   25.00
 Paul Cruz     |   25.00 |   25.00
(28 rows)


[Solucio ex4] -----------------------------------------------------------------------------------

SELECT ciutat, SUM(import) AS import_total_oficina
FROM oficines JOIN rep_vendes ON oficina_rep = oficina
JOIN comandes ON rep = num_empl
GROUP BY oficina
ORDER BY 2 DESC
LIMIT 1;

   ciutat    | import_total_oficina 
-------------+----------------------
 Los Angeles |             81409.00
(1 row)



SELECT ciutat, SUM (import)
FROM comandes
JOIN productes ON id_fabricant = fabricant AND id_producte = producte
JOIN rep_vendes ON rep = num_empl
JOIN oficines ON oficina_rep = oficina
GROUP BY ciutat 
ORDER BY 2 DESC 
LIMIT 1;
   ciutat    |   sum    
-------------+----------
 Los Angeles | 79989.00
(1 row)


---------------------------------------------------------



## 5 [0p]

SELECT nom, descripcio, COUNT(*), SUM(import) as import
FROM comandes JOIN rep_vendes ON rep = num_empl
JOIN clients ON clie = num_clie
JOIN productes ON id_fabricant = fabricant AND id_producte = producte
WHERE (import >= 3000 AND import <= 32000) AND (nom = 'Bill Adams' OR nom = 'Dan Roberts')
GROUP BY 1,2
ORDER BY nom, import;

     nom     |   descripcio    | com_fetes |  import  
-------------+-----------------+-----------+----------
 Bill Adams  | Article Tipus 4 |         1 |  3276.00
 Bill Adams  | Article Tipus 3 |         1 |  3745.00
 Bill Adams  | Article Tipus 2 |         1 |  4104.00
 Bill Adams  | Extractor       |         1 | 27500.00
 Dan Roberts | Article Tipus 4 |         1 |  3978.00
 Dan Roberts | Frontissa Dta.  |         1 | 22500.00
(6 rows)


[solucio ex5]-----------------------------------------------------------------------------

SELECT nom, descripcio, COUNT(num_comanda) AS comandes_fetes, SUM(import) AS import_comandes_fetes
FROM comandes JOIN productes ON producte = id_producte AND fabricant = id_fabricant
JOIN rep_vendes ON rep = num_empl
WHERE (nom = 'Bill Adams' OR nom = 'Dan Roberts')
GROUP BY nom, id_fabricant, id_producte
HAVING SUM(import) >= 3000 AND SUM(import) <= 32000
ORDER BY 1,4;

     nom     |   descripcio    | comandes_fetes | import_comandes_fetes 
-------------+-----------------+----------------+-----------------------
 Bill Adams  | Article Tipus 3 |              1 |               3745.00
 Bill Adams  | Article Tipus 4 |              2 |               3978.00
 Bill Adams  | Article Tipus 2 |              1 |               4104.00
 Bill Adams  | Extractor       |              1 |              27500.00
 Dan Roberts | Article Tipus 4 |              1 |               3978.00
 Dan Roberts | Frontissa Dta.  |              1 |              22500.00
(6 rows)

--------------------------------------------------------------------------------------------















































