
1- (1 punt) Seleccionar els representants de ventes que tinguin:
	- un nom que comenci per 'S' o acabi en 's' i 
	- el càrrec de 'Rep Ventas' o 'Dir Ventas'

SELECT nom, carrec 
FROM rep_vendes 
WHERE (nom LIKE 'S%' OR nom LIKE '%s') 
AND (carrec = ‘Representant Vendes’ OR carrec = ‘Dir Vendes’); 



2- (1 punt) Seleccionar els representants de ventes que tinguin un nom que no contingui 'am', que
tinguin una edat inferior a 35 o superior a 48 i que treballin a alguna de les següents oficines : 22,
13, 12, 11.

select * from rep_vendes 
where nom not like '%am%' and (edat < 35 or edat > 48) and ( oficina_rep IN (22,13,12,11));


3- (1 punt) Seleccionar les oficines de la regió 'Este' amb unes ventes que no siguin inferiors al 10%
de lobjectiu ni inferiors a 400.000

select * from oficines 
where regio = 'Est' and vendes >= (0.1*objectiu) and vendes > 400000; 


4- (1 punt) Seleccionar els venedors que tenen un nom que no conté ni 'ul' ni 'ch' ni 'om', que comença per 'B' o per 'D' o per 'M' o per 'S' o per ‘L’ o per ‘N’, conté una ‘i’ i conté una ‘y’.

SELECT nom 
FROM rep_vendes
WHERE NOT (nom LIKE '%ul%' OR nom LIKE '%om%' OR nom LIKE '%ch%') 
AND (nom LIKE 'B%' OR nom LIKE 'D%' OR nom LIKE 'M%' OR nom LIKE 'S%' OR nom LIKE 'L%' OR nom LIKE 'N%') AND (nom LIKE '%i%' AND nom LIKE '%y%');

5- (1 punt) Seleccionar les comandes dels productes amb un import entre 30000 i 40000 o entre 600 i 800 que siguin de les fàbriques 'rei' o 'imm' o 'bic'.

6- (1 punt) Mostrar totes les dades dels productes que són d’alguna de les fàbriques següents : rei,
aci, imm, i tenen un preu superior a 2000 i inferior a 3000 i acaben amb 'tor'

7- (1 punt) Mostrar totes les dades dels productes que tenen un nom que conté les lletres 'vel' o les
lletres 'ra' amb unes existencies-estoc inferiors a 5 o superiors a 30 i amb un preu entre 400 i 700.


8- (1 punt) Mostrar les comandes on shan venut productes de les fàbriques que no comencen ni
amb 'a' ni amb 'i' però sí que contenen una 'a' o una 'i'.

SELECT * 
FROM comandes
WHERE NOT fabricant LIKE 'a%' AND NOT fabricant LIKE 'i%' AND (fabricant LIKE '%a%'OR fabricant LIKE '%i%');

9- (1 punt) Mostrar els productes amb un nom que no té cap espai en blanc i de les fàbriques 
que no contenen 2 lletres ‘m’ 

SELECT * 
FROM productes 
WHERE NOT descripcio LIKE '% %' AND NOT id_fabricant LIKE '%m%m%';

10- (1 punt) Mostrar el producte més car que ha venut cada representant de ventes. Mostrar el codi
de representant i el preu aplicat al producte més car ordenat per preu i codi de representant.

SELECT rep, import /quantitat, fabricant, producte 
FROM comandes
 ORDER BY 2 DESC LIMIT 1
