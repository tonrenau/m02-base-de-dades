1. Selecciona els treballadors que han venut menys quantitat de productes que la Sue Smith

SELECT num_empl, nom
FROM rep_vendes
WHERE EXISTS (SELECT rep FROM comandes 
		WHERE rep = num_empl 
		GROUP BY rep 
		HAVING SUM(quantitat) < (SELECT SUM(b.quantitat) 
						FROM comandes b 
						WHERE b.rep = (SELECT c.num_empl 
								FROM rep_vendes c 
								WHERE c.nom='Sue Smith')));

 num_empl |      nom      
----------+---------------
      109 | Mary Jones
      106 | Sam Clark
      110 | Tom Snyder
      103 | Paul Cruz
      107 | Nancy Angelli
(5 filas)


	AMB EL IN :

SELECT num_empl, nom
FROM rep_vendes
WHERE num_empl IN (SELECT rep FROM comandes 
		WHERE rep = num_empl 
		GROUP BY rep 
		HAVING SUM(quantitat) < (SELECT SUM(b.quantitat) 
						FROM comandes b 
						WHERE b.rep = (SELECT c.num_empl 
								FROM rep_vendes c 
								WHERE c.nom='Sue Smith')));				
								
								
								
2. Llista els treballadors que han venut mes en import que la Sue Smith, la Mary Jones i els Will Adams


SELECT num_empl, nom
FROM rep_vendes 
WHERE num_empl IN (SELECT rep FROM comandes WHERE rep = num_empl GROUP BY rep HAVING SUM(import) > ANY 
(SELECT SUM(b.import) FROM comandes as b WHERE b.rep IN 
(SELECT c.num_empl FROM rep_vendes as c WHERE c.nom IN ('Sue Smith' , ' Mary Jones' , 'Will Adams'))GROUP BY rep ));

 num_empl |      nom      
----------+---------------
      105 | Bill Adams
      106 | Sam Clark
      101 | Dan Roberts
      110 | Tom Snyder
      108 | Larry Fitch
      107 | Nancy Angelli
(6 rows)



3. Llista els treballadors que han venut mes que alguns dels seguents : Sue Smith, la Mary Jones i el Will Adams

SELECT num_empl, nom
FROM rep_vendes
WHERE EXISTS (SELECT rep FROM comandes 
		WHERE rep = num_empl 
		GROUP BY rep 
		HAVING SUM(import) > ANY (SELECT SUM(b.import) 
						FROM comandes b 
						WHERE b.rep IN (SELECT c.num_empl 
								FROM rep_vendes c 
								WHERE c.nom IN ('Sue Smith', 'Mary Jones', 'Will Adams')) GROUP BY b.rep));

 num_empl |      nom      
----------+---------------
      105 | Bill Adams
      102 | Sue Smith
      106 | Sam Clark
      101 | Dan Roberts
      110 | Tom Snyder
      108 | Larry Fitch
      107 | Nancy Angelli
(7 rows)

4. Llista els treballadors que han fet mes comandes que els seus directors. 

SELECT rv.num_empl, rv.nom, (SELECT COUNT(*) FROM comandes WHERE rep = rv.num_empl), 
	rv.cap,(SELECT c.nom FROM rep_vendes as c WHERE c.num_empl=rv.cap), (SELECT COUNT(*) FROM comandes WHERE rep = rv.cap)
FROM rep_vendes as rv 
WHERE rv.cap IS NOT NULL AND (SELECT COUNT(*) FROM comandes WHERE rep = rv.num_empl) > 
			(SELECT COUNT(*) FROM comandes WHERE rep = rv.cap);

 num_empl |     nom     | count | cap |    nom    | count 
----------+-------------+-------+-----+-----------+-------
      105 | Bill Adams  |     5 | 104 | Bob Smith |     0
      101 | Dan Roberts |     3 | 104 | Bob Smith |     0
      108 | Larry Fitch |     7 | 106 | Sam Clark |     2
      103 | Paul Cruz   |     2 | 104 | Bob Smith |     0
(4 rows)

SELECT num_empl, nom, (SELECT COUNT(*) FROM comandes WHERE rep = num_empl), 
cap, (SELECT a.nom FROM rep_vendes as a WHERE a.num_empl = cap), (SELECT COUNT(*) FROM comandes WHERE rep = cap)   
FROM rep_vendes 
WHERE cap IS NOT NULL AND 
(SELECT COUNT (*) FROM comandes WHERE rep = num_empl) > (SELECT COUNT(*) FROM comandes WHERE rep = cap);


5. Llista els treballadors que en el ranking de ventes estan entre el Dan Roberts i la Mary Jones

SELECT rep, SUM(import), (SELECT nom 
			FROM rep_vendes as c 
			WHERE c.num_empl=rep)
FROM comandes 
GROUP BY 1,3
HAVING SUM(import) > (SELECT SUM(import) 
			FROM comandes as b 
			WHERE b.rep = (SELECT c.num_empl 
					FROM rep_vendes as c 
					WHERE c.nom='Mary Jones'))
	AND SUM(import) < (SELECT SUM(import) 
				FROM comandes as b 
				WHERE b.rep = (SELECT c.num_empl 
						FROM rep_vendes as c 
						WHERE c.nom='Dan Roberts'))
ORDER BY 3 desc;

 rep |   sum    |    nom     
-----+----------+------------
 110 | 23132.00 | Tom Snyder
 102 | 22776.00 | Sue Smith
(2 rows)

6. Mostra les oficines (codi i ciutat) tals que el seu objectiu sigui inferior o igual a les quotes de tots els seus treballadors.

SELECT oficina, ciutat
FROM oficines
WHERE objectiu <= ALL (SELECT quota 
			FROM rep_vendes 
			WHERE oficina_rep = oficina);  

 oficina | ciutat  
---------+---------
      22 | Denver
      13 | Atlanta
(2 rows)

AMB EL ANY:   'WHERE NOT objectiu > ANY  (SELECT quota'
 

7. Llista els representants de vendes (codi de treballador i nom) que tenen un director més jove que algun dels seus empleats.
					
select num_empl, nom
from rep_vendes
where edat > any (select b.edat from rep_vendes as b where b.oficina_rep = any 
	(select oficina_rep from oficines where b.num_empl=director))

 num_empl |      nom      
----------+---------------
      105 | Bill Adams
      102 | Sue Smith
      106 | Sam Clark
      101 | Dan Roberts
      110 | Tom Snyder
      108 | Larry Fitch
      107 | Nancy Angelli
(7 rows)

8. Mostrar el codi de treballador, el seu nom i un camp anomenat i_m. El camp i_m és l''import més gran de les 
comandes que ha fet aquest treballador. 
Només s''han de llistar els treballadors que tinguin tots els clients amb alguna comanda amb import superior a 
la mitjana dels imports de totes les comandes.

SELECT num_empl, nom, (SELECT MAX(import) FROM comandes WHERE rep=num_empl) AS i_m
FROM rep_vendes
WHERE (SELECT AVG(import) FROM comandes) < ALL (SELECT MAX(import) FROM comandes WHERE rep=num_empl GROUP BY clie) 
AND EXISTS (SELECT * FROM comandes WHERE rep=num_empl);

 num_empl |    nom     |   i_m    
----------+------------+----------
      110 | Tom Snyder | 22500.00
(1 row)

9. Mostra el codi de fabricant i de producte i un camp de nom n_p. n_p és el nombre de comandes que s''han fet d''aquell producte.
Només s''han de llistar aquells productes tals que se n''ha fet alguna comanda amb una quantitat inferior a les seves existències.  

select id_fabricant, id_producte, (select count(*) from comandes where id_producte =producte and id_fabricant = fabricant )as n_p
from productes
where estoc > any (select quantitat from comandes where id_producte=producte and id_fabricant = fabricant);

		
 id_fabricant | id_producte | n_p 
--------------+-------------+-----
 rei          | 2a45c       |   2
 aci          | 4100y       |   1
 qsa          | xk47        |   3
 imm          | 779c        |   2
 aci          | 41003       |   1
 aci          | 41004       |   3
 bic          | 41003       |   2
 rei          | 2a44l       |   1
 fea          | 112         |   1
 aci          | 4100z       |   2
 aci          | 41002       |   2
 rei          | 2a44r       |   2
 imm          | 773c        |   1
 aci          | 4100x       |   2
 fea          | 114         |   2
 rei          | 2a44g       |   1
(16 rows)

10. Mostra el codi de client, el nom de client, un camp c_r i un camp n_p. El camp c_r ha de mostrar la quota del representant
de vendes del client. El camp n_p ha de mostrar el nombre de comandes que ha fet aquest client. 
Només s''han de mostrar els clients que l''import total de totes les seves comandes sigui superior a la mitjana 
de l''import de totes les comandes. Si en un espai de la taula queda un espai en blanc, ficar un 0.

select num_clie, empresa, coalesce((select quota from rep_vendes where rep_clie = num_empl),0)as c_r, 
(select count(*) from comandes where clie = num_clie ) as n_p
from clients
where (select sum(import) from comandes where clie = num_clie) > (select avg(import)from comandes);

 num_clie |      empresa      |    c_r    | n_p 
----------+-------------------+-----------+-----
     2103 | Acme Mfg.         | 350000.00 |   4
     2107 | Ace International |         0 |   2
     2112 | Zetacorp          | 350000.00 |   2
     2114 | Orion Corp        | 350000.00 |   2
     2117 | J.P. Sinclair     | 275000.00 |   1
     2113 | Ian & Schmidt     | 200000.00 |   1
     2109 | Chen Associates   | 275000.00 |   1
(7 rows)



per enves de un 0, ficar '------'


select num_clie, empresa, 
      coalesce(cast((select quota from rep_vendes where rep_clie = num_empl)as varchar), '-------')as c_r, 
      (select count(*) from comandes where clie = num_clie ) as n_p
from clients
where (select sum(import) from comandes where clie = num_clie) > (select avg(import)from comandes);

11. Mostrar lidentificador i el nom de l''empresa dels clients i un camp anomenat m_importe. 
Només s''ha de llistar aquells clients que han fet comandes de productes que tenen un preu que supera la mitjana 
del preu de tots els productes. 
El camp m_importe ha de mostrar limport més petit de les comandes del client..


select num_clie, empresa, (select min(import)from comandes where clie = num_clie) as m_import
from clients
where exists (select * from comandes where clie = num_clie
      and EXISTS (select * from productes where id_producte= producte and id_fabricant= fabricant and  
      preu > (select avg(preu)from productes)));		
										
 num_clie |      empresa      |   min    
----------+-------------------+----------
     2112 | Zetacorp          |  2925.00
     2114 | Orion Corp        |  7100.00
     2109 | Chen Associates   | 31350.00
     2120 | Rico Enterprises  |  3750.00
     2108 | Holm & Landis     |   150.00
     2103 | Acme Mfg.         |   702.00
     2107 | Ace International |   632.00
     2113 | Ian & Schmidt     | 22500.00
     2117 | J.P. Sinclair     | 31500.00
(9 rows)

