# Consultes resum

# Exercici 0
En una mateixa consulta, calcula la suma de les quotes, quants venedors nhi ha, i finalment mostra la quota promig calculada a partir de les funcions anteriors i també amb una funció especial per al promig. Compara aquests dos últims resultats. Si volguéssim que la darrera funció actui com la penúltima com ho faries?

 SELECT SUM(quota), COUNT(*), CAST( SUM(quota) / COUNT (*) AS NUMERIC (8,2)) AS mitjana, CAST(AVG(quota) AS NUMERIC (8,2))  
FROM rep_vendes;
    sum     | count |  mitjana  |    avg    
------------+-------+-----------+-----------
 2700000.00 |    10 | 270000.00 | 300000.00
(1 row)

## Exercici 1
Quina és la quota promig mostrada com a "prom_quota" i la venda promig
mostrades com a "prom_vendes" dels venedors?

SELECT ROUND(AVG(quota),2) AS "prom_quota", CAST(AVG(vendes) AS NUMERIC (8,2)) AS "prom_vendes" FROM rep_vendes;
 prom_quota | prom_vendes 
------------+-------------
  300000.00 |   289353.20
(1 row)

 
## Exercici 2
Quin és el promig del rendiment dels venedors (promig del percentatge de les vendes
respecte la quota)?

SELECT CAST(100 * AVG (vendes/quota) AS NUMERIC (5,2)) 
FROM rep_vendes;
 numeric 
---------
  102.60
(1 row)



## Exercici 3

Quines són les quotes totals com a *t_quota* i vendes totals com a *t_vendes*
de tots els venedors?

SELECT SUM(quota) AS "t_quota", SUM(vendes) AS "t_vendes" FROM rep_vendes;
  t_quota   |  t_vendes  
------------+------------
 2700000.00 | 2893532.00
(1 row)


## Exercici 4
Calcula el preu mig dels productes del fabricant amb identificador "aci".

SELECT CAST(SUM(preu)/ COUNT(preu) AS NUMERIC (5,2))
FROM productes 
WHERE id_fabricant = 'aci';
 numeric 
---------
  804.29


## Exercici 5
Quines són les quotes assignades mínima i màxima?

SELECT MAX(quota), MIN(quota) 
FROM rep_vendes;
    max    |    min    
-----------+-----------
 350000.00 | 200000.00
(1 row)


## Exercici 6
Quina és la data de comanda més antiga?

SELECT MIN(data) 
FROM comandes;
    min     
------------
 1989-01-04
(1 row)


## Exercici 7
Quin és el major percentatge de rendiment de vendes respecte les quotes de tots els venedors?
(o sigui el major percentatge de les vendes respecte a la quota)

SELECT CAST (100 * MAX(vendes / quota) AS NUMERIC (5,2)) 
FROM rep_vendes;
 numeric 
---------
  135.44
(1 row)


## Exercici 8

Quants clients hi ha?
SELECT COUNT(clients) 
FROM clients;
 count 
-------
    21
(1 row)

## Exercici 9
Quants venedors superen la seva quota?

SELECT COUNT(*) 
FROM rep_vendes 
WHERE vendes > quota;
 count 
-------
     7
(1 row)

## Exercici 10
Quantes comandes amb un import superior a 25000 hi ha en els registres?

SELECT COUNT(*) 
FROM comandes 
WHERE import > 25000;
 count 
-------
     5
(1 row)

## Exercici 11

Trobar l'import mitjà de les comandes, l'import total de les comandes, la
mitjana del percentatge de l'import de les comandes respecte del límit de
crèdit del client i la mitjana del percentatge de l'import de les comandes
respecte a la quota del venedor.

SELECT AVG(import), SUM(import), CAST( AVG(import/limit_credit) AS NUMERIC (6,2)), CAST (100* AVG(import/quota) AS NUMERIC (6,2)) 
FROM comandes
JOIN clients ON comandes.clie = clients.num_clie
JOIN rep_vendes 
ON comandes.rep = rep_vendes.num_empl;

          avg          |    sum    | avg  | numeric 
-----------------------+-----------+------+---------
 8256.3666666666666667 | 247691.00 | 0.24 |    2.51
(1 row)


## Exercici 12
Compta les files que hi ha a repventas, les files del camp vendes i les del camp quota.

SELECT COUNT 
FROM rep_vendes ;
    sum    | ?column?  
-----------+-----------
 117547.00 | 193532.00
(1 row)

## Exercici 13
Demostra que la suma de restar vendes menys quota és diferent que sumar vendes i restar-li la suma de quotes.
SELECT SUM(vendes-quota), SUM (vendes)- SUM(quota) 
FROM rep_vendes;

    sum    | ?column?  
-----------+-----------
 117547.00 | 193532.00
(1 row)


## Exercici 14
Quants tipus de càrrecs hi ha de venedors?

SELECT COUNT(DISTINCT(carrec))
FROM rep_vendes;
 count 
-------
     3
(1 row)

## Exercici 15
Quantes oficines de vendes tenen venedors que superen les seves quotes?

SELECT COUNT(oficina_rep)
FROM rep_vendes 
WHERE vendes > quota;
 count 
-------
     7
(1 row)

## Exercici 16
De la taula clients quants clients diferents i venedors diferents hi ha.

SELECT COUNT(DISTINCT(num_clie)) AS clients_diferents, COUNT(DISTINCT(rep_clie))ASvenedors_diferents 
FROM clients;
 clients_diferents | asvenedors_diferents 
-------------------+----------------------
                21 |                   10

## Exercici 17
De la taula comandes seleccionar quantes comandes diferents i clients diferents hi ha

SELECT COUNT(num_comanda), COUNT(DISTINCT(clie)) 
FROM comandes;
 count | count 
-------+-------
    31 |    15
(1 row)
## Exercici 18
Calcular la mitjana dels imports de les comandes.

SELECT CAST(AVG(import) AS NUMERIC (8,2))
FROM comandes ;
   avg   
---------
 8828.74

## Exercici 19
Calcula la mitjana de limport d'una comanda realitzada pel client amb nom d'empresa "Acme Mfg."

SELECT CAST(AVG(import) AS numeric (8,2)), COUNT(*)
FROM comandes 
JOIN clients 
ON comandes.clie = clients.num_clie
WHERE empresa = 'Acme Mfg.';
   avg   | count 
---------+-------
 8895.50 |     4
(1 row)

# Consultes resum (PART II)

## Exercici 20

Quina és limport promig de les comandes de cada venedor, amb el seu nom? 

SELECT rep, nom, CAST(AVG(import) AS NUMERIC (8,2))  
FROM comandes 
RIGHT JOIN rep_vendes
ON comandes.rep = rep_vendes.num_empl
GROUP BY rep, nom.
ORDER BY 1;
 rep |      nom      |   avg    
-----+---------------+----------
 105 | Bill Adams    |  7865.40
 108 | Larry Fitch   |  8376.14
 103 | Paul Cruz     |  1350.00
     | Bob Smith     |         
 110 | Tom Snyder    | 11566.00
 109 | Mary Jones    |  3552.50
 102 | Sue Smith     |  5694.00
 101 | Dan Roberts   |  8876.00
 107 | Nancy Angelli | 11477.33
 106 | Sam Clark     | 16479.00
(10 rows)


## Exercici 21

Quin és el rang (màxim i mínim) de quotes dels venedors per cada oficina?

SELECT MAX(quota), MIN(quota), nom , oficina 
FROM rep_vendes LEFT JOIN oficines 
ON rep_vendes.oficina_rep = oficines.oficina 
GROUP BY nom, oficina;
    max    |    min    |      nom      | oficina 
-----------+-----------+---------------+---------
 275000.00 | 275000.00 | Paul Cruz     |      12
 350000.00 | 350000.00 | Sue Smith     |      21
 300000.00 | 300000.00 | Mary Jones    |      11
           |           | Tom Snyder    |        
 275000.00 | 275000.00 | Sam Clark     |      11
 350000.00 | 350000.00 | Bill Adams    |      13
 300000.00 | 300000.00 | Nancy Angelli |      22
 200000.00 | 200000.00 | Bob Smith     |      12
 350000.00 | 350000.00 | Larry Fitch   |      21
 300000.00 | 300000.00 | Dan Roberts   |      12
(10 rows)

## Exercici 22

Quants venedors estan asignats a cada oficina?

SELECT oficina, ciutat, COUNT(*) 
FROM rep_vendes LEFT JOIN oficines 
ON rep_vendes.oficina_rep = oficines.oficina 
GROUP BY oficina
ORDER BY 1;
 oficina |   ciutat    | count 
---------+-------------+-------
      11 | New York    |     2
      12 | Chicago     |     3
      13 | Atlanta     |     1
      21 | Los Angeles |     2
      22 | Denver      |     1
         |             |     1
(6 rows)



## Exercici 23

Per cada venedor calcular quants clients diferents ha atès ( atès = atendre una comanda)? Amb el nom del vendedor

SELECT rep, clie, COUNT(*), nom, empresa
FROM comandes RIGHT JOIN rep_vendes
ON comandes.rep = rep_vendes.num_empl
JOIN clients 
ON comandes.clie = clients.num_clie  
GROUP BY rep, clie, nom, empresa 
ORDER BY rep, clie;
 rep | clie | count |      nom      |      empresa      
-----+------+-------+---------------+-------------------
 101 | 2102 |     1 | Dan Roberts   | First Corp.
 101 | 2108 |     1 | Dan Roberts   | Holm & Landis
 101 | 2113 |     1 | Dan Roberts   | Ian & Schmidt
 102 | 2106 |     2 | Sue Smith     | Fred Lewis Corp.
 102 | 2114 |     1 | Sue Smith     | Orion Corp
 102 | 2120 |     1 | Sue Smith     | Rico Enterprises
 103 | 2111 |     2 | Paul Cruz     | JCP Inc.
 105 | 2103 |     4 | Bill Adams    | Acme Mfg.
 105 | 2111 |     1 | Bill Adams    | JCP Inc.
 106 | 2101 |     1 | Sam Clark     | Jones Mfg.
 106 | 2117 |     1 | Sam Clark     | J.P. Sinclair
 107 | 2109 |     1 | Nancy Angelli | Chen Associates
 107 | 2124 |     2 | Nancy Angelli | Peter Brothers
 108 | 2112 |     2 | Larry Fitch   | Zetacorp
 108 | 2114 |     1 | Larry Fitch   | Orion Corp
 108 | 2118 |     4 | Larry Fitch   | Midwest Systems
 109 | 2108 |     2 | Mary Jones    | Holm & Landis
 110 | 2107 |     2 | Tom Snyder    | Ace International
(18 rows)

## Exercici 24

Calcula el total dels imports de les comandes fetes per cada client a cada
vendedor.
SELECT clie, empresa, rep, nom, SUM(import), COUNT(*)
FROM comandes JOIN rep_vendes 
ON comandes.rep = rep_vendes.num_empl 
JOIN clients ON comandes.clie = clients.num_clie 
GROUP BY rep,clie, nom, empresa;
 clie |      empresa      | rep |      nom      |   sum    | count 
------+-------------------+-----+---------------+----------+-------
 2101 | Jones Mfg.        | 106 | Sam Clark     |  1458.00 |     1
 2108 | Holm & Landis     | 109 | Mary Jones    |  7105.00 |     2
 2124 | Peter Brothers    | 107 | Nancy Angelli |  3082.00 |     2
 2118 | Midwest Systems   | 108 | Larry Fitch   |  3608.00 |     4
 2114 | Orion Corp        | 108 | Larry Fitch   |  7100.00 |     1
 2114 | Orion Corp        | 102 | Sue Smith     | 15000.00 |     1
 2107 | Ace International | 110 | Tom Snyder    | 23132.00 |     2
 2112 | Zetacorp          | 108 | Larry Fitch   | 47925.00 |     2
 2111 | JCP Inc.          | 105 | Bill Adams    |  3745.00 |     1
 2102 | First Corp.       | 101 | Dan Roberts   |  3978.00 |     1
 2109 | Chen Associates   | 107 | Nancy Angelli | 31350.00 |     1
 2111 | JCP Inc.          | 103 | Paul Cruz     |  2700.00 |     2
 2117 | J.P. Sinclair     | 106 | Sam Clark     | 31500.00 |     1
 2113 | Ian & Schmidt     | 101 | Dan Roberts   | 22500.00 |     1
 2103 | Acme Mfg.         | 105 | Bill Adams    | 35582.00 |     4
 2106 | Fred Lewis Corp.  | 102 | Sue Smith     |  4026.00 |     2
 2108 | Holm & Landis     | 101 | Dan Roberts   |   150.00 |     1
 2120 | Rico Enterprises  | 102 | Sue Smith     |  3750.00 |     1
(18 rows)


## Exercici 25

El mateix que a la qüestió anterior, però ordenat per client i dintre de client
per venedor.

SELECT clie, empresa, rep, nom, SUM(import), COUNT(*)
FROM comandes JOIN rep_vendes 
ON comandes.rep = rep_vendes.num_empl 
JOIN clients ON comandes.clie = clients.num_clie 
GROUP BY rep,clie, nom, empresa 
ORDER BY rep, clie;
 clie |      empresa      | rep |      nom      |   sum    | count 
------+-------------------+-----+---------------+----------+-------
 2102 | First Corp.       | 101 | Dan Roberts   |  3978.00 |     1
 2108 | Holm & Landis     | 101 | Dan Roberts   |   150.00 |     1
 2113 | Ian & Schmidt     | 101 | Dan Roberts   | 22500.00 |     1
 2106 | Fred Lewis Corp.  | 102 | Sue Smith     |  4026.00 |     2
 2114 | Orion Corp        | 102 | Sue Smith     | 15000.00 |     1
 2120 | Rico Enterprises  | 102 | Sue Smith     |  3750.00 |     1
 2111 | JCP Inc.          | 103 | Paul Cruz     |  2700.00 |     2
 2103 | Acme Mfg.         | 105 | Bill Adams    | 35582.00 |     4
 2111 | JCP Inc.          | 105 | Bill Adams    |  3745.00 |     1
 2101 | Jones Mfg.        | 106 | Sam Clark     |  1458.00 |     1
 2117 | J.P. Sinclair     | 106 | Sam Clark     | 31500.00 |     1
 2109 | Chen Associates   | 107 | Nancy Angelli | 31350.00 |     1
 2124 | Peter Brothers    | 107 | Nancy Angelli |  3082.00 |     2
 2112 | Zetacorp          | 108 | Larry Fitch   | 47925.00 |     2
 2114 | Orion Corp        | 108 | Larry Fitch   |  7100.00 |     1
 2118 | Midwest Systems   | 108 | Larry Fitch   |  3608.00 |     4
 2108 | Holm & Landis     | 109 | Mary Jones    |  7105.00 |     2
 2107 | Ace International | 110 | Tom Snyder    | 23132.00 |     2
(18 rows)

## Exercici 26

Calcula les comandes totals per a cada venedor.

SELECT nom, rep, COUNT(*)
FROM comandes JOIN rep_vendes 
ON comandes.rep = rep_vendes.num_empl 
GROUP BY nom, rep 
ORDER BY 2; 
      nom      | rep | count 
---------------+-----+-------
 Dan Roberts   | 101 |     3
 Sue Smith     | 102 |     4
 Paul Cruz     | 103 |     2
 Bill Adams    | 105 |     5
 Sam Clark     | 106 |     2
 Nancy Angelli | 107 |     3
 Larry Fitch   | 108 |     7
 Mary Jones    | 109 |     2
 Tom Snyder    | 110 |     2
(9 rows)

## Exercici 27

Quin és limport promig de les comandes per cada venedor, les comandes dels
quals sumen més de 30000?

SELECT rep, round(AVG(import),2) AS "import promig", SUM (import) 
FROM comandes 
GROUP BY rep 
having SUM(import) > 30000;
 rep | import promig |   sum    
-----+---------------+----------
 108 |       8376.14 | 58633.00
 105 |       7865.40 | 39327.00
 107 |      11477.33 | 34432.00
 106 |      16479.00 | 32958.00
(4 rows)

## Exercici 28

Per cada oficina amb dos o més empleats, calcular la quota total i les vendes
totals per a tots els venedors que treballen a la oficina (volem mostrar la
ciutat de loficina a la consulta)

SELECT SUM(quota) AS "quota_t", SUM(rep_vendes.vendes) AS "vendes_t", oficina_rep, ciutat
FROM rep_vendes JOIN oficines  
ON oficina_rep = oficina 
GROUP BY oficina_rep, ciutat
HAVING COUNT(*) >= 2;

  quota_t  | vendes_t  | oficina_rep |   ciutat    
-----------+-----------+-------------+-------------
 700000.00 | 835915.00 |          21 | Los Angeles
 775000.00 | 735042.00 |          12 | Chicago
 575000.00 | 692637.00 |          11 | New York
(3 rows)


## Exercici 29

Mostra el preu, lestoc i la quantitat total de les comandes de cada producte
per als quals la quantitat total demanada està per sobre del 75% de lestoc.

SELECT id_fabricant, id_producte, preu, estoc, COUNT (*) AS "num_comanda", SUM(quantitat) 
FROM comandes JOIN productes 
ON fabricant = id_fabricant AND producte = id_producte
GROUP BY id_fabricant, id_producte 
HAVING SUM(quantitat) > (0.75*estoc);.

 id_fabricant | id_producte |  preu   | estoc | num_comanda | sum 
--------------+-------------+---------+-------+-------------+-----
 aci          | 4100x       |   25.00 |    37 |           2 |  30
 fea          | 114         |  243.00 |    15 |           2 |  16
 imm          | 775c        | 1425.00 |     5 |           1 |  22
 rei          | 2a44r       | 4500.00 |    12 |           2 |  15
(4 rows)



## Exercici 30

Es desitja un llistat didentificadors de fabricants de productes. Només volem
tenir en compte els productes de preu superior a 54. Només volem que apareguin
els fabricants amb un nombre total dunitats superior a 300.

SELECT id_fabricant, SUM(estoc)                                                 
FROM productes              
WHERE preu > 54 
GROUP BY id_fabricant 
HAVING SUM(estoc) > 300
ORDER BY 1;
 id_fabricant | sum 
--------------+-----
 aci          | 843
(1 row)

## Exercici 31

Es desitja un llistat dels productes amb les seves descripcions, ordenat per la
suma total dimports facturats (comandes) de cada producte de lany 1989.

SELECT fabricant, producte, descripcio, data, SUM(import)
FROM comandes JOIN productes ON fabricant= id_fabricant AND producte = id_producte 
WHERE data <= '1989-12-31' AND data >= '1989-1-1'
GROUP BY fabricant, producte, descripcio, data  
ORDER BY fabricant, producte, descripcio,data
training-> ;
 fabricant | producte |     descripcio     |    data    |   sum    
-----------+----------+--------------------+------------+----------
 aci       | 41002    | Article Tipus 2    | 1989-11-04 |   760.00
 aci       | 41004    | Article Tipus 4    | 1989-10-12 |  3978.00
 aci       | 41004    | Article Tipus 4    | 1989-12-17 |  3276.00
 aci       | 41004    | Article Tipus 4    | 1989-12-27 |   702.00
 aci       | 4100y    | Extractor          | 1989-12-31 | 27500.00
 aci       | 4100z    | Muntador           | 1989-10-12 | 15000.00
 rei       | 2a44g    | Passador Frontissa | 1989-12-12 |  2100.00
 rei       | 2a44l    | Frontissa Esq.     | 1989-12-17 | 31500.00
 rei       | 2a45c    | V Stago Trinquet   | 1989-01-04 |  1896.00

## Exercici 32

Per a cada director (de personal, no doficina) excepte per al gerent (el
venedor que no té director), vull saber el total de vendes dels seus
subordinats. Mostreu codi i nom dels directors.

SELECT caps.num_empl, caps.nom ,SUM(subordinats.vendes)
FROM rep_vendes subordinats JOIN rep_vendes caps 
ON subordinats.cap = caps.num_empl 
GROUP BY caps.num_empl, caps.nom;

 num_empl |     nom     |    sum    
----------+-------------+-----------
      104 | Bob Smith   | 960359.00
      106 | Sam Clark   | 897184.00
      101 | Dan Roberts |  75985.00
      108 | Larry Fitch | 660092.00
(4 rows)



## Exercici 33

Quins són els 5 productes que han estat venuts a més clients diferents? Mostreu
el número de clients per cada producte. A igualtat de nombre de clients es
volen ordenats per ordre decreixent d'estoc i, a igualtat d'estoc, per
descripció. Mostreu tots els camps pels quals sordena.

SELECT fabricant, producte, estoc, descripcio, COUNT(DISTINCT clie)
FROM comandes JOIN productes 
ON producte = id_producte
AND fabricant = id_fabricant
GROUP BY fabricant, producte, estoc, descripcio
ORDER BY 5 DESC, 3 DESC, 4 DESC
LIMIT 5 ;

SELECT id_fabricant, id_producte, estoc, descripcio, COUNT(DISTINCT clie)
FROM comandes JOIN productes 
ON producte = id_producte
AND fabricant = id_fabricant
GROUP BY id_fabricant, id_producte
ORDER BY 5 DESC, 3 DESC, 4 DESC
LIMIT 5 ;

 id_fabricant | id_producte | count | estoc |    descripcio    
--------------+-------------+-------+-------+------------------
 rei          | 2a45c       |     2 |   210 | V Stago Trinquet
 imm          | 779c        |     2 |     9 | Riosta 2-Tm
 imm          | 775c        |     1 |     5 | Riosta 1-Tm
 imm          | 773c        |     1 |    28 | Riosta 1/2-Tm
 qsa          | xk47        |     3 |    38 | Reductor
(5 rows)
## Exercici 34

Es vol llistar el clients (codi i empresa) tals que no hagin comprat cap tipus
de Frontissa (figura a la descripció) i hagin comprat articles de més dun
fabricant diferent.

SELECT num_clie, empresa, COUNT(DISTINCT fabricant) 
FROM productes JOIN comandes 
ON id_fabricant = fabricant
AND id_producte = producte
JOIN clients 
ON num_clie = clie 
WHERE descripcio NOT LIKE '%Frontissa%' 
GROUP BY num_clie, empresa
HAVING COUNT(DISTINCT fabricant) > 1
ORDER BY 3;

 num_clie |      empresa      | count 
----------+-------------------+-------
     2106 | Fred Lewis Corp.  |     2
     2107 | Ace International |     2
     2114 | Orion Corp        |     2
     2124 | Peter Brothers    |     2
     2108 | Holm & Landis     |     3
     2118 | Midwest Systems   |     3
(6 rows)


## Exercici 35

Llisteu les oficines per ordre descendent de nombre total de clients diferents
amb comandes realitzades pels venedors daquella oficina, i, a igualtat de
clients, ordenat per ordre ascendent del nom del director de l'oficina. Només
s'ha de mostrar el codi i la ciutat de loficina.

SELECT oficina, ciutat, COUNT(DISTINCT clie)
FROM oficines JOIN rep_vendes
ON oficina_rep = oficina 
JOIN comandes 
ON num_empl = rep
JOIN rep_vendes direccio 
ON direccio.num_empl = oficines.director 
GROUP BY oficina, ciutat;

 oficina |   ciutat    | count 
---------+-------------+-------
      11 | New York    |     3
      12 | Chicago     |     4
      13 | Atlanta     |     2
      21 | Los Angeles |     5
      22 | Denver      |     2
(5 rows)

Exercici 36
Quantes oficines tenim a cada regió?

SELECT regio, COUNT(DISTINCT oficina)
FROM oficines
GROUP BY regio;
 regio | count 
-------+-------
 Est   |     3
 Oest  |     2
(2 rows)

Exercici 37
Quants representants de vendes hi ha a cada oficina?

SELECT oficina_rep, COUNT(DISTINCT num_empl) AS representant
FROM rep_vendes
GROUP BY oficina_rep
ORDER BY oficina_rep;
 oficina_rep | representant 
-------------+--------------
          11 |            2
          12 |            3
          13 |            1
          21 |            2
          22 |            1
             |            1
(6 rows)



Exercici 38
Quants representants de vendes té assignats cada cap de respresentants? Amb el seu nom

SELECT cp.num_empl, cp.nom, rep_vendes.cap, COUNT(*)
FROM rep_vendes JOIN rep_vendes cp  
ON rep_vendes.cap = cp.num_empl
GROUP BY cp.num_empl, cp.nom, rep_vendes.cap;

 num_empl |     nom     | cap | count 
----------+-------------+-----+-------
      106 | Sam Clark   | 106 |     3
      104 | Bob Smith   | 104 |     3
      108 | Larry Fitch | 108 |     2
      101 | Dan Roberts | 101 |     1
(4 rows)



Exercici 39
Per cada venedor calcular quants clients diferents ha atès ( atès = atendre una comanda)?

SELECT num_comanda, rep, COUNT(DISTINCT clie)
FROM comandes RIGHT JOIN rep_vendes
ON rep = num_empl
GROUP BY num_comanda, rep; 

 num_comanda | rep | count 
-------------+-----+-------
       88888 |     |     1
      110036 | 110 |     1
      112961 | 106 |     1
      112963 | 105 |     1
      112968 | 101 |     1
      112975 | 103 |     1
      112979 | 102 |     1
      112983 | 105 |     1
      112987 | 105 |     1
      112989 | 106 |     1
      112992 | 108 |     1
      112993 | 102 |     1
      112997 | 107 |     1
      113003 | 109 |     1
      113007 | 108 |     1
      113012 | 105 |     1
      113013 | 108 |     1
      113024 | 108 |     1
      113027 | 105 |     1
      113034 | 110 |     1
      113042 | 101 |     1
      113045 | 108 |     1
      113048 | 102 |     1
      113049 | 108 |     1
      113051 | 108 |     1
      113055 | 101 |     1
      113057 | 103 |     1
      113058 | 109 |     1
      113062 | 107 |     1
      113065 | 102 |     1
      113069 | 107 |     1
(31 rows)

Exercici 40
Quina és, per cada oficina, la suma de les quotes dels seus representants? I la mitjana de les quotes per oficina?

SELECT oficina_rep, num_empl, nom, SUM(quota),CAST(AVG(quota) AS NUMERIC (8,2))
FROM rep_vendes 
GROUP BY oficina_rep, num_empl, nom;
 oficina_rep | num_empl |      nom      |    sum    |    avg    
-------------+----------+---------------+-----------+-----------
          12 |      103 | Paul Cruz     | 275000.00 | 275000.00
          12 |      104 | Bob Smith     | 200000.00 | 200000.00
          13 |      105 | Bill Adams    | 350000.00 | 350000.00
          22 |      107 | Nancy Angelli | 300000.00 | 300000.00
          11 |      109 | Mary Jones    | 300000.00 | 300000.00
          11 |      106 | Sam Clark     | 275000.00 | 275000.00
             |      110 | Tom Snyder    |           |          
          12 |      101 | Dan Roberts   | 300000.00 | 300000.00
          21 |      108 | Larry Fitch   | 350000.00 | 350000.00
          21 |      102 | Sue Smith     | 350000.00 | 350000.00
(10 rows)

Exercici 41
Quina és  la quota més alta de cada oficina? Amb la ciutat

SELECT oficina_rep, ciutat, MAX(quota)
FROM rep_vendes JOIN oficines
ON oficina_rep = oficina
GROUP BY oficina_rep, ciutat 
ORDER BY 1;
 oficina_rep |   ciutat    |    max    
-------------+-------------+-----------
          11 | New York    | 300000.00
          12 | Chicago     | 300000.00
          13 | Atlanta     | 350000.00
          21 | Los Angeles | 350000.00
          22 | Denver      | 300000.00
(5 rows)

Exercici 42
Quants clients representa cada venedor rep_vendes? Amb el seu cap i el nom del cap, i la numero de la oficina de la ciutat i el seu direcotor amb el nom:

SELECT rep_clie, rep_vendes.nom, rep_vendes.cap, copia.nom, rep_vendes.oficina_rep, oficines.ciutat, oficines.director, copia2.nom, COUNT (*)
FROM clients JOIN rep_vendes 
ON rep_clie = num_empl 
JOIN rep_vendes AS copia 
ON rep_vendes.cap = copia.num_empl 
JOIN oficines 
ON rep_vendes.oficina_rep = oficines.oficina 
JOIN rep_vendes AS copia2 
ON oficines.director = copia2.num_empl 
GROUP BY rep_clie, rep_vendes.nom, rep_vendes.cap, copia.nom, rep_vendes.oficina_rep, oficines.ciutat, oficines.director, copia2.nom
ORDER BY rep_clie;

 rep_clie |      nom      | cap |     nom     | oficina_rep |   ciutat    | director |     nom     | count 
----------+---------------+-----+-------------+-------------+-------------+----------+-------------+-------
      101 | Dan Roberts   | 104 | Bob Smith   |          12 | Chicago     |      104 | Bob Smith   |     3
      102 | Sue Smith     | 108 | Larry Fitch |          21 | Los Angeles |      108 | Larry Fitch |     4
      103 | Paul Cruz     | 104 | Bob Smith   |          12 | Chicago     |      104 | Bob Smith   |     3
      104 | Bob Smith     | 106 | Sam Clark   |          12 | Chicago     |      104 | Bob Smith   |     1
      105 | Bill Adams    | 104 | Bob Smith   |          13 | Atlanta     |      105 | Bill Adams  |     2
      107 | Nancy Angelli | 108 | Larry Fitch |          22 | Denver      |      108 | Larry Fitch |     1
      108 | Larry Fitch   | 106 | Sam Clark   |          21 | Los Angeles |      108 | Larry Fitch |     2
      109 | Mary Jones    | 106 | Sam Clark   |          11 | New York    |      106 | Sam Clark   |     2
(8 rows)

Exercici 43
Quin és límit de crèdit més alt dels clients de cada venedor-rep_vendes?

SELECT CAST(MAX(limit_credit) AS NUMERIC (8,2)), rep_clie, nom
FROM clients JOIN rep_vendes 
ON rep_clie = num_empl 
GROUP BY rep_clie, nom
ORDER BY rep_clie;
   max    | rep_clie |      nom      
----------+----------+---------------
 65000.00 |      101 | Dan Roberts
 65000.00 |      102 | Sue Smith
 50000.00 |      103 | Paul Cruz
 20000.00 |      104 | Bob Smith
 50000.00 |      105 | Bill Adams
 65000.00 |      106 | Sam Clark
 40000.00 |      107 | Nancy Angelli
 60000.00 |      108 | Larry Fitch
 55000.00 |      109 | Mary Jones
 35000.00 |      110 | Tom Snyder
(10 rows)



Exercici 44
Per cada codi de fàbrica diferents, quants productes hi ha? 

SELECT id_fabricant, COUNT(id_producte)            
FROM productes               
GROUP BY id_fabricant;
 id_fabricant | count 
--------------+-------
 imm          |     6
 aci          |     7
 bic          |     3
 fea          |     2
 qsa          |     3
 rei          |     4
(6 rows)

Exercici 45
Per cada id_producte diferent, a quantes fàbriques es fabrica?

SELECT id_producte, COUNT(DISTINCT id_fabricant) 
FROM productes
GROUP BY id_producte;
 id_producte | count 
-------------+-------
 112         |     1
 114         |     1
 2a44g       |     1
 2a44l       |     1
 2a44r       |     1
 2a45c       |     1
 41001       |     1
 41002       |     1
 41003       |     2
 41004       |     1
 4100x       |     1
 4100y       |     1
 4100z       |     1
 41089       |     1
 41672       |     1
 773c        |     1
 775c        |     1
 779c        |     1
 887h        |     1
 887p        |     1
 887x        |     1
 xk47        |     1
 xk48        |     1
 xk48a       |     1
(24 rows)



Exercici 46
Per cada nom de producte diferent, quants codis (id_fab + id_prod) tenim?
SELECT descripcio, COUNT(*) AS fabr_prod
FROM productes
GROUP BY descripcio
ORDER BY 2 DESC;

   descripcio     | fabr_prod 
--------------------+-----------
 Reductor           |         3
 Article Tipus 3    |         1
 Passador Frontissa |         1
 Riosta 1-Tm        |         1
 Frontissa Esq.     |         1
 Coberta            |         1
 Manovella          |         1
 Frontissa Dta.     |         1
 Plate              |         1
 Article Tipus 4    |         1
 Suport Riosta      |         1
 Retenidor Riosta   |         1
 Article Tipus 2    |         1
 Riosta 1/2-Tm      |         1
 Riosta 2-Tm        |         1
 Article Tipus 1    |         1
 Peu de rei         |         1
 Extractor          |         1
 V Stago Trinquet   |         1
 Retn               |         1
 Bancada Motor      |         1
 Pern Riosta        |         1
 Muntador           |         1
(23 rows)


Exercici 47
Per cada producte (id_fab + id_prod), quantes comandes tenim?

SELECT descripcio, COUNT(*)
FROM productes JOIN comandes
ON id_producte = producte and id_fabricant = fabricant
GROUP BY descripcio 
ORDER BY 1;
     descripcio     | count 
--------------------+-------
 Article Tipus 2    |     2
 Article Tipus 3    |     1
 Article Tipus 4    |     3
 Bancada Motor      |     2
 Coberta            |     1
 Extractor          |     1
 Frontissa Dta.     |     2
 Frontissa Esq.     |     1
 Manovella          |     2
 Muntador           |     2
 Passador Frontissa |     1
 Peu de rei         |     2
 Reductor           |     3
 Riosta 1/2-Tm      |     1
 Riosta 1-Tm        |     1
 Riosta 2-Tm        |     2
 V Stago Trinquet   |     2
(17 rows)

Exercici 48
Per cada client, quantes comandes tenim? Incloure els clients que no han realitzat cap comanda 

SELECT num_clie, COUNT(num_comanda) AS comandes 
FROM clients LEFT JOIN comandes 
ON num_clie = clie 
GROUP BY num_clie
ORDER BY comandes DESC;
 num_clie | comandes 
----------+----------
     2118 |        4
     2103 |        4
     2108 |        3
     2111 |        3
     2106 |        2
     2114 |        2
     2112 |        2
     2107 |        2
     2124 |        2
     2113 |        1
     2117 |        1
     2101 |        1
     2120 |        1
     2109 |        1
     2102 |        1
     2115 |        0
     2122 |        0
     2123 |        0
     2121 |        0
     2119 |        0
     2105 |        0
(21 rows)

Exercici 49
Quantes comandes ha realitzat cada representant de vendes? Incloure els venedors que no han realitzat cap comanda
SELECT num_empl, COUNT(*),nom 
FROM comandes RIGHT JOIN rep_vendes 
ON rep = num_empl 
GROUP BY num_empl;

 num_empl | count |      nom      
----------+-------+---------------
      103 |     2 | Paul Cruz
      104 |     1 | Bob Smith
      105 |     5 | Bill Adams
      107 |     3 | Nancy Angelli
      109 |     2 | Mary Jones
      106 |     2 | Sam Clark
      110 |     2 | Tom Snyder
      101 |     3 | Dan Roberts
      108 |     7 | Larry Fitch
      102 |     4 | Sue Smith
(10 rows)

Exercici 50;
Quantes comandes s'han fet, quina és la suma total dels imports d'aquestes comandes i quina és la mitjana de l'import de la comada : les comandes amb una quantitat d'entre 20 i 30 productes i de les fàbriques que continguin una 'i' o una 'a'.

SELECT COUNT(*), SUM(import), CAST(AVG(import) AS NUMERIC (8,2))
FROM comandes 
WHERE (fabricant LIKE '%i%' OR fabricant LIKE '%a%') AND (quantitat <= 30 AND quantitat >= 20);
 count |   sum    |   avg   
-------+----------+---------
     5 | 44222.00 | 8844.40
(1 row)


Exercici 51
De quina fàbrica shan venut menys unitats de producte (alerta : no menys comandes, menys unitats)
SELECT SUM(quantitat), fabricant 
FROM comandes 
GROUP BY fabricant 
ORDER BY 1
LIMIT 1;

 sum | fabricant 
-----+-----------
   2 | bic
(1 row)

Exercici 52
Quins són els 3 venedors que han fet més comandes?
SELECT COUNT(rep),rep, nom
FROM comandes JOIN rep_vendes
ON rep = num_empl
GROUP BY rep, nom
ORDER BY COUNT(rep) DESC
LIMIT 3;
 count | rep |     nom     
-------+-----+-------------
     7 | 108 | Larry Fitch
     5 | 105 | Bill Adams
     4 | 102 | Sue Smith
(3 rows)

Exercici 53
Mostra quants clients amb un nom que contingui 2 espais en blanc té assignat cada venedor.

SELECT rep_clie, COUNT(num_clie) 
FROM clients 
WHERE empresa LIKE '% % %'
GROUP BY rep_clie 
ORDER BY 1;

 rep_clie | count 
----------+-------
      102 |     2
      104 |     1
      109 |     1
(3 rows)


Exercici 54
Mostra quantes comandes ha fet cada venedor a cada client que tingui un codi múltiple de 5 o múltiple de 3 i, a més a més, que el codi de client no sigui ni el 110 ni el 102.

SELECT clie, rep, COUNT(num_comanda) 
FROM comandes JOIN clients 
ON clie = num_clie
WHERE (num_clie % 5 = 0 OR num_clie % 3 = 0) AND NOT (num_clie= 110 OR num_clie = 102)
GROUP BY clie, rep
ORDER BY 1;

 clie | rep | count 
------+-----+-------
 2103 | 105 |     4
 2106 | 102 |     2
 2109 | 107 |     1
 2112 | 108 |     2
 2118 | 108 |     4
 2120 | 102 |     1
 2124 | 107 |     2
(7 rows)


