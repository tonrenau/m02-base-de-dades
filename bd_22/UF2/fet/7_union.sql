    
    1. Mostar les ventes individuals dels productes dels fabricants 'aci' i 'rei' 
    que comencin per 'Frontissa' o 'Article'. Mostrar també el total venut daquests productes.

SELECT 'Frontissa: ', id_fabricant, descripcio, SUM(quantitat) AS quantitat_venuda
FROM comandes JOIN productes ON producte = id_producte AND fabricant = id_fabricant 
WHERE descripcio LIKE '%Frontissa%' AND (id_fabricant = 'aci' OR id_fabricant = 'rei')
GROUP BY id_fabricant, id_producte
UNION
SELECT 'Article: ', id_fabricant, descripcio, SUM(quantitat) AS quantitat_venuda
FROM comandes JOIN productes ON producte = id_producte AND fabricant = id_fabricant 
WHERE descripcio LIKE '%Article%' AND (id_fabricant = 'aci' OR id_fabricant = 'rei')
GROUP BY id_fabricant, id_producte;

  ?column?   | id_fabricant |     descripcio     | quantitat_venuda 
-------------+--------------+--------------------+------------------
 Article:    | aci          | Article Tipus 2    |               64
 Article:    | aci          | Article Tipus 3    |               35
 Article:    | aci          | Article Tipus 4    |               68
 Frontissa:  | rei          | Frontissa Dta.     |               15
 Frontissa:  | rei          | Frontissa Esq.     |                7
 Frontissa:  | rei          | Passador Frontissa |                6
(6 rows)

    2. Mostrar les ventes individuals fetes pels venedors de New York i de Chicago que superin els 2500€. 
    Mostrar també el total de ventes de cada oficina.

select 'Venedor: ', ciutat, num_comanda, nom, import 
from comandes join rep_vendes on rep = num_empl 
join oficines on  oficina_rep = oficina
where ciutat IN ('New York', 'Chicago') and import > 2500

UNION

select 'Total: ', ciutat, 0,'--------',sum(import)
from comandes join rep_vendes on rep = num_empl 
join oficines on oficina = oficina_rep
where ciutat IN ('New York', 'Chicago') and import > 2500
group by ciutat

order by ciutat, 1 desc;

     entidad     |  ciutat  |     nom     | num_comanda |  import  
-----------------+----------+-------------+-------------+----------
 Venedor:        | Chicago  | Dan Roberts |      113042 | 22500.00
 Venedor:        | Chicago  | Dan Roberts |      112968 |  3978.00
 Total Oficina:  | Chicago  | -----       |           0 | 26478.00
 Venedor:        | New York | Mary Jones  |      113003 |  5625.00
 Venedor:        | New York | Sam Clark   |      112961 | 31500.00
 Total Oficina:  | New York | -----       |           0 | 37125.00
(6 rows)


    3. Mostrar quantes ventes ha fet cada venedor, la mitjana de numero de ventes dels venedors de cada oficina 
    i el numero de ventes total.

SELECT 'venedor:' as Tipo, ciutat, num_empl as id, nom as identitat, COUNT(*) as total_vendes 
FROM rep_vendes LEFT JOIN comandes ON rep = num_empl 
LEFT JOIN oficines ON oficina_rep = oficina 
GROUP BY ciutat, num_empl 

UNION 

SELECT '__Oficina:', ciutat, '0','', COUNT(*)/COUNT(DISTINCT num_empl ) 
FROM oficines RIGHT JOIN rep_vendes ON oficina = oficina_rep 
LEFT JOIN comandes On rep = num_empl 
GROUP BY oficina

UNION 

SELECT '___TOTAL:', ' ', ' 0', ' ', COUNT(*) 
FROM comandes 
ORDER BY 2 DESC, 1 DESC;

    tipo    |   ciutat    | id  |   identitat   | total_vendes 
------------+-------------+-----+---------------+--------------
 venedor:   |             | 110 | Tom Snyder    |            2
 __Oficina: |             |   0 |               |            2
 venedor:   | New York    | 109 | Mary Jones    |            2
 venedor:   | New York    | 106 | Sam Clark     |            2
 __Oficina: | New York    |   0 |               |            2
 venedor:   | Los Angeles | 108 | Larry Fitch   |            7
 venedor:   | Los Angeles | 102 | Sue Smith     |            4
 __Oficina: | Los Angeles |   0 |               |            5
 venedor:   | Denver      | 107 | Nancy Angelli |            3
 __Oficina: | Denver      |   0 |               |            3
 venedor:   | Chicago     | 103 | Paul Cruz     |            2
 venedor:   | Chicago     | 104 | Bob Smith     |            1
 venedor:   | Chicago     | 101 | Dan Roberts   |            3
 __Oficina: | Chicago     |   0 |               |            2
 venedor:   | Atlanta     | 105 | Bill Adams    |            5
 __Oficina: | Atlanta     |   0 |               |            5
 ___TOTAL:  |             |   0 |               |           30
(17 rows)

    4. Mostrar les compres de productes de la fabrica 'aci' que han fet els clients del Bill Adams i el Dan Roberts. 
    Mostrar també limport total per cada client.

    
SELECT 'Compres: ', num_empl, nom, num_clie, empresa, import
FROM comandes
JOIN clients ON clie = num_clie
JOIN rep_vendes ON rep_clie = num_empl
WHERE (nom = 'Bill Adams' OR nom = 'Dan Roberts') AND fabricant = 'aci'

UNION

SELECT 'Total: ', 0, '-----', num_clie, empresa, SUM(import)
FROM comandes
JOIN clients ON clie = num_clie
JOIN rep_vendes ON rep_clie = num_empl
WHERE (nom = 'Bill Adams' OR nom = 'Dan Roberts') AND fabricant = 'aci'
GROUP BY num_clie 
ORDER BY 5, 1 DESC;

 ?column?  | num_empl |     nom     | num_clie |   empresa   |  import  
-----------+----------+-------------+----------+-------------+----------
 Total:    |        0 | -----       |     2103 | Acme Mfg.   | 35582.00
 Compres:  |      105 | Bill Adams  |     2103 | Acme Mfg.   |   702.00
 Compres:  |      105 | Bill Adams  |     2103 | Acme Mfg.   |  3276.00
 Compres:  |      105 | Bill Adams  |     2103 | Acme Mfg.   | 27500.00
 Compres:  |      105 | Bill Adams  |     2103 | Acme Mfg.   |  4104.00
 Total:    |        0 | -----       |     2102 | First Corp. |  3978.00
 Compres:  |      101 | Dan Roberts |     2102 | First Corp. |  3978.00
(7 rows)


    5. Mostrar el total de ventes de cada oficina i el total de ventes de cada regió
    
SELECT 'oficina:', regio, ciutat, sum(import)
FROM oficines JOIN rep_vendes ON oficina = oficina_rep 
LEFT JOIN comandes ON rep = num_empl
GROUP BY regio, oficina 
UNION 
SELECT '_Regio:', regio, ' ', sum(import)
FROM oficines RIGHT JOIN rep_vendes ON oficina= oficina_rep 
LEFT JOIN comandes ON rep = num_empl 
GROUP BY regio 
ORDER BY regio, 1;
    
 ?column? | regio |   ciutat    |    sum    
----------+-------+-------------+-----------
 oficina: | Est   | New York    |  40063.00
 oficina: | Est   | Atlanta     |  39327.00
 oficina: | Est   | Chicago     |  29328.00
 _Regio:  | Est   |             | 108718.00
 oficina: | Oest  | Los Angeles |  81409.00
 oficina: | Oest  | Denver      |  34432.00
 _Regio:  | Oest  |             | 115841.00
 _Regio:  |       |             |  23132.00
(8 rows)

    6. Mostrar els noms dels venedors de cada ciutat i el numero total de venedors per ciutat

select 'Venedor de: ' as result, ciutat, nom,0 as "total_treballadors"
from oficines right join rep_vendes on oficina = oficina_rep 

union 

select 'total de: ', ciutat, '------', COUNT(oficina_rep)
from oficines right join rep_vendes on oficina = oficina_rep 
group by ciutat
order by ciutat , 1 desc ; 

    result    |   ciutat    |      nom      | total_treballadors 
--------------+-------------+---------------+--------------------
 Venedor de:  | Atlanta     | Bill Adams    |                  0
 total de:    | Atlanta     | ------        |                  1
 Venedor de:  | Chicago     | Dan Roberts   |                  0
 Venedor de:  | Chicago     | Bob Smith     |                  0
 Venedor de:  | Chicago     | Paul Cruz     |                  0
 total de:    | Chicago     | ------        |                  3
 Venedor de:  | Denver      | Nancy Angelli |                  0
 total de:    | Denver      | ------        |                  1
 Venedor de:  | Los Angeles | Larry Fitch   |                  0
 Venedor de:  | Los Angeles | Sue Smith     |                  0
 total de:    | Los Angeles | ------        |                  2
 Venedor de:  | New York    | Sam Clark     |                  0
 Venedor de:  | New York    | Mary Jones    |                  0
 total de:    | New York    | ------        |                  2
 Venedor de:  |             | Tom Snyder    |                  0
 total de:    |             | ------        |                  0
(16 rows)


    7. Mostrat els noms dels clients de cada ciutat i el numero total de clients per ciutat.

    8. Mostrat els noms dels treballadors que son -caps- dalgú, els noms dels seus -subordinats- i el numero de treballadors que té assignat cada cap.
    

    
    
