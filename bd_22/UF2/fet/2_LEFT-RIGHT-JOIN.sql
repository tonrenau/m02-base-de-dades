# Consultes multitaula



## Exercici 1

Llista la ciutat de les oficines, i el nom i títol dels directors de cada oficina.

SELECT ciutat, oficina, carrec 
FROM rep_vendes 
LEFT JOIN oficines 
ON rep_vendes.oficina_rep = oficines.oficina;
   ciutat    | oficina |       carrec        
-------------+---------+---------------------
 Atlanta     |      13 | Representant Vendes
 New York    |      11 | Representant Vendes
 Los Angeles |      21 | Representant Vendes
 New York    |      11 | VP Vendes
 Chicago     |      12 | Dir Vendes
 Chicago     |      12 | Representant Vendes
             |         | Representant Vendes
 Los Angeles |      21 | Dir Vendes
 Chicago     |      12 | Representant Vendes
 Denver      |      22 | Representant Vendes
(10 rows)


SELECT ciutat, oficina, carrec 
FROM rep_vendes 
RIGHT JOIN oficines 
ON oficines.oficina = rep_vendes.oficina_rep;
   ciutat    | oficina |       carrec        
-------------+---------+---------------------
 Atlanta     |      13 | Representant Vendes
 New York    |      11 | Representant Vendes
 Los Angeles |      21 | Representant Vendes
 New York    |      11 | VP Vendes
 Chicago     |      12 | Dir Vendes
 Chicago     |      12 | Representant Vendes
 Los Angeles |      21 | Dir Vendes
 Chicago     |      12 | Representant Vendes
 Denver      |      22 | Representant Vendes
(9 rows)

## Exercici 2

Llista totes les comandes mostrant el seu número, import, número de client i límit de crèdit.

SELECT num_comanda, clie, limit_credit, import 
FROM comandes 
LEFT JOIN clients
ON comandes.clie = clients.num_clie;
 num_comanda | clie | limit_credit |  import  
-------------+------+--------------+----------
      112961 | 2117 |     35000.00 | 31500.00
      113012 | 2111 |     50000.00 |  3745.00
      112989 | 2101 |     65000.00 |  1458.00
      113051 | 2118 |     60000.00 |  1420.00
      112968 | 2102 |     65000.00 |  3978.00
      110036 | 2107 |     35000.00 | 22500.00
      113045 | 2112 |     50000.00 | 45000.00
      112963 | 2103 |     50000.00 |  3276.00
      113013 | 2118 |     60000.00 |   652.00
      113058 | 2108 |     55000.00 |  1480.00
      112997 | 2124 |     40000.00 |   652.00
      112983 | 2103 |     50000.00 |   702.00
      113024 | 2114 |     20000.00 |  7100.00
      113062 | 2124 |     40000.00 |  2430.00
      112979 | 2114 |     20000.00 | 15000.00
      113027 | 2103 |     50000.00 |  4104.00
      113007 | 2112 |     50000.00 |  2925.00
      113069 | 2109 |     25000.00 | 31350.00
      113034 | 2107 |     35000.00 |   632.00
      112992 | 2118 |     60000.00 |   760.00
      112975 | 2111 |     50000.00 |  2100.00
      113055 | 2108 |     55000.00 |   150.00
      113048 | 2120 |     50000.00 |  3750.00
      112993 | 2106 |     65000.00 |  1896.00
      113065 | 2106 |     65000.00 |  2130.00
      113003 | 2108 |     55000.00 |  5625.00
      113049 | 2118 |     60000.00 |   776.00
      112987 | 2103 |     50000.00 | 27500.00
      113057 | 2111 |     50000.00 |   600.00
      113042 | 2113 |     20000.00 | 22500.00
(30 rows)

SELECT num_comanda, clie, limit_credit, import 
FROM comandes 
RIGHT JOIN clients
ON comandes.clie = clients.num_clie;
 num_comanda | clie | limit_credit |  import  
-------------+------+--------------+----------
      112961 | 2117 |     35000.00 | 31500.00
      113012 | 2111 |     50000.00 |  3745.00
      112989 | 2101 |     65000.00 |  1458.00
      113051 | 2118 |     60000.00 |  1420.00
      112968 | 2102 |     65000.00 |  3978.00
      110036 | 2107 |     35000.00 | 22500.00
      113045 | 2112 |     50000.00 | 45000.00
      112963 | 2103 |     50000.00 |  3276.00
      113013 | 2118 |     60000.00 |   652.00
      113058 | 2108 |     55000.00 |  1480.00
      112997 | 2124 |     40000.00 |   652.00
      112983 | 2103 |     50000.00 |   702.00
      113024 | 2114 |     20000.00 |  7100.00
      113062 | 2124 |     40000.00 |  2430.00
      112979 | 2114 |     20000.00 | 15000.00
      113027 | 2103 |     50000.00 |  4104.00
      113007 | 2112 |     50000.00 |  2925.00
      113069 | 2109 |     25000.00 | 31350.00
      113034 | 2107 |     35000.00 |   632.00
      112992 | 2118 |     60000.00 |   760.00
      112975 | 2111 |     50000.00 |  2100.00
      113055 | 2108 |     55000.00 |   150.00
      113048 | 2120 |     50000.00 |  3750.00
      112993 | 2106 |     65000.00 |  1896.00
      113065 | 2106 |     65000.00 |  2130.00
      113003 | 2108 |     55000.00 |  5625.00
      113049 | 2118 |     60000.00 |   776.00
      112987 | 2103 |     50000.00 | 27500.00
      113057 | 2111 |     50000.00 |   600.00
      113042 | 2113 |     20000.00 | 22500.00
             |      |     45000.00 |         
             |      |     20000.00 |         
             |      |     25000.00 |         
             |      |     30000.00 |         
             |      |     45000.00 |         
             |      |     40000.00 |         
(36 rows)


## Exercici 3

Llista el número de totes les comandes amb la descripció del producte demanat.

SELECT num_comanda, descripcio 
FROM comandes 
LEFT JOIN productes
ON comandes.producte = productes.id_fabricant
AND comandes.producte = productes.id_producte;
 num_comanda | descripcio 
-------------+------------
      112961 | 
      113012 | 
      112989 | 
      113051 | 
      112968 | 
      110036 | 
      113045 | 
      112963 | 
      113013 | 
      113058 | 
      112997 | 
      112983 | 
      113024 | 
      113062 | 
      112979 | 
      113027 | 
      113007 | 
      113069 | 
      113034 | 
      112992 | 
      112975 | 
      113055 | 
      113048 | 
      112993 | 
      113065 | 
      113003 | 
      113049 | 
      112987 | 
      113057 | 
      113042 | 
(30 rows)

SELECT num_comanda, descripcio 
FROM comandes 
RIGHT JOIN productes
ON comandes.producte = productes.id_fabricant
AND comandes.producte = productes.id_producte;
 num_comanda |     descripcio     
-------------+--------------------
             | Manovella
             | Reductor
             | Plate
             | Extractor
             | Coberta
             | Pern Riosta
             | Frontissa Dta.
             | Article Tipus 1
             | Muntador
             | Article Tipus 2
             | Frontissa Esq.
             | Article Tipus 4
             | Bancada Motor
             | Reductor
             | Reductor
             | Retenidor Riosta
             | Riosta 1/2-Tm
             | Peu de rei
             | Passador Frontissa
             | V Stago Trinquet
             | Article Tipus 3
             | Retn
             | Riosta 1-Tm
             | Riosta 2-Tm
             | Suport Riosta
(25 rows)


## Exercici 4

Llista el nom de tots els clients amb el nom del representant de vendes assignat.

SELECT num_clie, empresa, rep_clie, num_empl, nom 
FROM clients 
LEFT JOIN rep_vendes 
ON clients.rep_clie = rep_vendes.num_empl;
 num_clie |      empresa      | rep_clie | num_empl |      nom      
----------+-------------------+----------+----------+---------------
     2111 | JCP Inc.          |      103 |      103 | Paul Cruz
     2102 | First Corp.       |      101 |      101 | Dan Roberts
     2103 | Acme Mfg.         |      105 |      105 | Bill Adams
     2123 | Carter & Sons     |      102 |      102 | Sue Smith
     2107 | Ace International |      110 |      110 | Tom Snyder
     2115 | Smithson Corp.    |      101 |      101 | Dan Roberts
     2101 | Jones Mfg.        |      106 |      106 | Sam Clark
     2112 | Zetacorp          |      108 |      108 | Larry Fitch
     2121 | QMA Assoc.        |      103 |      103 | Paul Cruz
     2114 | Orion Corp        |      102 |      102 | Sue Smith
     2124 | Peter Brothers    |      107 |      107 | Nancy Angelli
     2108 | Holm & Landis     |      109 |      109 | Mary Jones
     2117 | J.P. Sinclair     |      106 |      106 | Sam Clark
     2122 | Three-Way Lines   |      105 |      105 | Bill Adams
     2120 | Rico Enterprises  |      102 |      102 | Sue Smith
     2106 | Fred Lewis Corp.  |      102 |      102 | Sue Smith
     2119 | Solomon Inc.      |      109 |      109 | Mary Jones
     2118 | Midwest Systems   |      108 |      108 | Larry Fitch
     2113 | Ian & Schmidt     |      104 |      104 | Bob Smith
     2109 | Chen Associates   |      103 |      103 | Paul Cruz
     2105 | AAA Investments   |      101 |      101 | Dan Roberts
(21 rows)

## Exercici 5

Llista la data de totes les comandes amb el numero i nom del client de la comanda.

SELECT num_clie, empresa  
FROM clients 
LEFT JOIN comandes 
ON clients.num_clie = comandes.clie;

 num_clie |      empresa      
----------+-------------------
     2117 | J.P. Sinclair
     2111 | JCP Inc.
     2101 | Jones Mfg.
     2118 | Midwest Systems
     2102 | First Corp.
     2107 | Ace International
     2112 | Zetacorp
     2103 | Acme Mfg.
     2118 | Midwest Systems
     2108 | Holm & Landis
     2124 | Peter Brothers
     2103 | Acme Mfg.
     2114 | Orion Corp
     2124 | Peter Brothers
     2114 | Orion Corp
     2103 | Acme Mfg.
     2112 | Zetacorp
     2109 | Chen Associates
     2107 | Ace International
     2118 | Midwest Systems
     2111 | JCP Inc.
     2108 | Holm & Landis
     2120 | Rico Enterprises
     2106 | Fred Lewis Corp.
     2106 | Fred Lewis Corp.
     2108 | Holm & Landis
     2118 | Midwest Systems
     2103 | Acme Mfg.
     2111 | JCP Inc.
     2113 | Ian & Schmidt
     2121 | QMA Assoc.
     2115 | Smithson Corp.
     2119 | Solomon Inc.
     2122 | Three-Way Lines
     2105 | AAA Investments
     2123 | Carter & Sons
(36 rows)

SELECT num_clie, empresa  
FROM clients 
JOIN comandes 
ON clients.num_clie = comandes.clie;

 num_clie |      empresa      
----------+-------------------
     2117 | J.P. Sinclair
     2111 | JCP Inc.
     2101 | Jones Mfg.
     2118 | Midwest Systems
     2102 | First Corp.
     2107 | Ace International
     2112 | Zetacorp
     2103 | Acme Mfg.
     2118 | Midwest Systems
     2108 | Holm & Landis
     2124 | Peter Brothers
     2103 | Acme Mfg.
     2114 | Orion Corp
     2124 | Peter Brothers
     2114 | Orion Corp
     2103 | Acme Mfg.
     2112 | Zetacorp
     2109 | Chen Associates
     2107 | Ace International
     2118 | Midwest Systems
     2111 | JCP Inc.
     2108 | Holm & Landis
     2120 | Rico Enterprises
     2106 | Fred Lewis Corp.
     2106 | Fred Lewis Corp.
     2108 | Holm & Landis
     2118 | Midwest Systems
     2103 | Acme Mfg.
     2111 | JCP Inc.
     2113 | Ian & Schmidt
(30 rows)


## Exercici 6

Llista les oficines, noms i títols del seus directors amb un objectiu superior a 600.000.

SELECT oficina, nom, carrec 
FROM oficines 
LEFT JOIN rep_vendes 
ON oficines.director = rep_vendes.num_empl 
WHERE oficines.objectiu > 600000;

 oficina |     nom     |   carrec   
---------+-------------+------------
      12 | Bob Smith   | Dir Vendes
      21 | Larry Fitch | Dir Vendes
(2 rows)

## Exercici 7

Llista els venedors de les oficines de la regió est.

SELECT regio, num_empl, nom 
FROM rep_vendes 
LEFT JOIN oficines 
ON oficines.oficina = rep_vendes.oficina_rep 
WHERE oficines.regio = 'Est';
 regio | num_empl |     nom     
-------+----------+-------------
 Est   |      105 | Bill Adams
 Est   |      109 | Mary Jones
 Est   |      106 | Sam Clark
 Est   |      104 | Bob Smith
 Est   |      101 | Dan Roberts
 Est   |      103 | Paul Cruz
(6 rows)

## Exercici 8

Llista les comandes superiors a 25000, incloent el nom del venedor que va servir la comanda i el nom del client que el va sol·licitar.

SELECT fabricant, import, nom, empresa 
FROM comandes 
LEFT JOIN clients 
ON clients.num_clie = comandes.clie 
LEFT JOIN rep_vendes 
ON comandes.rep = rep_vendes.num_empl 
WHERE comandes.import >= 25000;
 fabricant |  import  |      nom      |     empresa     
-----------+----------+---------------+-----------------
 rei       | 31500.00 | Sam Clark     | J.P. Sinclair
 rei       | 45000.00 | Larry Fitch   | Zetacorp
 imm       | 31350.00 | Nancy Angelli | Chen Associates
 aci       | 27500.00 | Bill Adams    | Acme Mfg.
(4 rows)

## Exercici 9

Llista les comandes superiors a 25000, mostrant el client que va servir la comanda i el nom del venedor que té assignat el client.

SELECT num_comanda, empresa, rep_clie, nom, import 
FROM comandes 
LEFT JOIN clients 
ON comandes.clie = clients.num_clie 
LEFT JOIN rep_vendes 
ON clients.rep_clie = rep_vendes.num_empl 
WHERE import > 25000;
 num_comanda |     empresa     | rep_clie |     nom     |  import  
-------------+-----------------+----------+-------------+----------
      112961 | J.P. Sinclair   |      106 | Sam Clark   | 31500.00
      113045 | Zetacorp        |      108 | Larry Fitch | 45000.00
      113069 | Chen Associates |      103 | Paul Cruz   | 31350.00
      112987 | Acme Mfg.       |      105 | Bill Adams  | 27500.00
(4 rows)

## Exercici 10

Llista les comandes superiors a 25000, mostrant el nom del client que el va ordenar, el venedor associat al client, i loficina on el venedor treballa.

SELECT nom, num_clie, num_comanda, empresa, import, ciutat .
FROM comandes 
LEFT JOIN clients 
ON comandes.clie = clients.num_clie 
LEFT JOIN rep_vendes 
ON rep_vendes.num_empl = clients.rep_clie 
LEFT JOIN oficines 
ON oficines.oficina = rep_vendes.oficina_rep 
AND import > 25000;

      nom      | num_clie | num_comanda |      empresa      |  import  |   ciutat    
---------------+----------+-------------+-------------------+----------+-------------
 Sam Clark     |     2117 |      112961 | J.P. Sinclair     | 31500.00 | New York
 Paul Cruz     |     2111 |      113012 | JCP Inc.          |  3745.00 | 
 Sam Clark     |     2101 |      112989 | Jones Mfg.        |  1458.00 | 
 Larry Fitch   |     2118 |      113051 | Midwest Systems   |  1420.00 | 
 Dan Roberts   |     2102 |      112968 | First Corp.       |  3978.00 | 
 Tom Snyder    |     2107 |      110036 | Ace International | 22500.00 | 
 Larry Fitch   |     2112 |      113045 | Zetacorp          | 45000.00 | Los Angeles
 Bill Adams    |     2103 |      112963 | Acme Mfg.         |  3276.00 | 
 Larry Fitch   |     2118 |      113013 | Midwest Systems   |   652.00 | 
 Mary Jones    |     2108 |      113058 | Holm & Landis     |  1480.00 | 
 Nancy Angelli |     2124 |      112997 | Peter Brothers    |   652.00 | 
 Bill Adams    |     2103 |      112983 | Acme Mfg.         |   702.00 | 
 Sue Smith     |     2114 |      113024 | Orion Corp        |  7100.00 | 
 Nancy Angelli |     2124 |      113062 | Peter Brothers    |  2430.00 | 
 Sue Smith     |     2114 |      112979 | Orion Corp        | 15000.00 | 
 Bill Adams    |     2103 |      113027 | Acme Mfg.         |  4104.00 | 
 Larry Fitch   |     2112 |      113007 | Zetacorp          |  2925.00 | 
 Paul Cruz     |     2109 |      113069 | Chen Associates   | 31350.00 | Chicago
 Tom Snyder    |     2107 |      113034 | Ace International |   632.00 | 
 Larry Fitch   |     2118 |      112992 | Midwest Systems   |   760.00 | 
 Paul Cruz     |     2111 |      112975 | JCP Inc.          |  2100.00 | 
 Mary Jones    |     2108 |      113055 | Holm & Landis     |   150.00 | 
 Sue Smith     |     2120 |      113048 | Rico Enterprises  |  3750.00 | 
 Sue Smith     |     2106 |      112993 | Fred Lewis Corp.  |  1896.00 | 
 Sue Smith     |     2106 |      113065 | Fred Lewis Corp.  |  2130.00 | 
 Mary Jones    |     2108 |      113003 | Holm & Landis     |  5625.00 | 
 Larry Fitch   |     2118 |      113049 | Midwest Systems   |   776.00 | 
 Bill Adams    |     2103 |      112987 | Acme Mfg.         | 27500.00 | Atlanta
 Paul Cruz     |     2111 |      113057 | JCP Inc.          |   600.00 | 
 Bob Smith     |     2113 |      113042 | Ian & Schmidt     | 22500.00 | 
(30 rows)

SELECT nom, num_clie, num_comanda, empresa, import, ciutat 
FROM comandes RIGHT JOIN clients 
ON comandes.clie = clients.num_clie 
RIGHT JOIN rep_vendes 
ON rep_vendes.num_empl = clients.rep_clie 
RIGHT JOIN oficines 
ON oficines.oficina = rep_vendes.oficina_rep 
AND import > 25000;
     nom     | num_clie | num_comanda |     empresa     |  import  |   ciutat    
-------------+----------+-------------+-----------------+----------+-------------
 Sam Clark   |     2117 |      112961 | J.P. Sinclair   | 31500.00 | New York
 Larry Fitch |     2112 |      113045 | Zetacorp        | 45000.00 | Los Angeles
 Paul Cruz   |     2109 |      113069 | Chen Associates | 31350.00 | Chicago
 Bill Adams  |     2103 |      112987 | Acme Mfg.       | 27500.00 | Atlanta
             |          |             |                 |          | Denver
(5 rows)

## Exercici 11

Llista totes les combinacions de venedors i oficines on la quota del venedor és superior a lobjectiu de loficina.

SELECT nom, oficina, quota, objectiu
 FROM rep_vendes JOIN oficines 
 ON rep_vendes.oficina_rep = oficines.oficina 
 WHERE rep_vendes.quota > oficines.objectiu;

## Exercici 12

Informa sobre tots els venedors i les oficines en les que treballen.

SELECT num_empl, nom, oficina 
FROM rep_vendes LEFT  JOIN oficines 
ON rep_vendes.oficina_rep = oficines.oficina;
 num_empl |      nom      | oficina 
----------+---------------+---------
      105 | Bill Adams    |      13
      109 | Mary Jones    |      11
      102 | Sue Smith     |      21
      106 | Sam Clark     |      11
      104 | Bob Smith     |      12
      101 | Dan Roberts   |      12
      110 | Tom Snyder    |        
      108 | Larry Fitch   |      21
      103 | Paul Cruz     |      12
      107 | Nancy Angelli |      22
(10 rows)


## Exercici 13

Llista els venedors amb una quota superior a la dels seus directors.

SELECT rep_vendes.nom, rep_vendes.cap
FROM rep_vendes 
LEFT JOIN rep_vendes cap 
ON cap.num_empl = rep_vendes.cap
WHERE rep_vendes.quota > cap.quota;
     nom     | cap 
-------------+-----
 Bill Adams  | 104
 Mary Jones  | 106
 Dan Roberts | 104
 Larry Fitch | 106
 Paul Cruz   | 104
(5 rows)


## Exercici 14

Llistar el nom de lempresa i totes les comandes fetes pel client 2103.

SELECT empresa, num_comanda, clie
FROM clients JOIN comandes 
ON clients.num_clie = comandes.clie 
WHERE clie = 2103;

## Exercici 15

Llista aquelles comandes que el seu import sigui superior a 10000, mostrant el numero de comanda, els imports i les descripcions del producte.

SELECT descripcio, num_comanda, import 
FROM productes 
LEFT JOIN comandes 
ON productes.id_producte = comandes.producte 
AND  productes.id_fabricant = comandes.fabricant
WHERE import > 10000 ;

## Exercici 16 (MAL) 

Llista les comandes superiors a 25000, mostrant el nom del client que la va demanar, el venedor associat al client, i loficina on el venedor treballa. També cal mostar la descripció del producte.

SELECT empresa, nom, oficina_rep, descripcio, import
FROM clients JOIN rep_vendes 
On clients.rep_clie = rep_vendes.num_empl 
JOIN comandes 
ON comandes.rep = rep_vendes.num_empl 
LEFT JOIN oficines 
ON rep_vendes.oficina_REP = rep_vendes.num.empl
LEFT JOIN productes 
ON productes.id_producte = comandes.producte 
AND  productes.id_fabricant = comandes.fabricant
WHERE import > 25000;

## Exercici 17

Trobar totes les comandes rebudes en els dies en que un nou venedor va ser contractat. Per cada comanda mostrar un cop el número, import i data de la comanda.

SELECT num_comanda, data_contracte, import  
FROM rep_vendes LEFT JOIN comandes
ON rep_vendes.data_contracte = comandes.data;
 num_comanda | data_contracte |  import  
-------------+----------------+----------
      112968 | 1989-10-12     |  3978.00
      112968 | 1989-10-12     |  3978.00
      112979 | 1989-10-12     | 15000.00
      112979 | 1989-10-12     | 15000.00
             | 1987-05-19     |         
             | 1986-12-10     |         
             | 1986-10-20     |         
             | 1988-02-12     |         
             | 1988-11-14     |         
             | 1990-01-13     |         
             | 1987-03-01     |         
             | 1988-06-14     |         
(12 rows)


## Exercici 18

Mostra el nom, les vendes dels treballadors que tenen assignada una oficina, amb la ciutat i l'objectiu de l'oficina de cada venedor.

SELECT nom, oficina_rep, ciutat, objectiu
FROM rep_vendes LEFT JOIN oficines 
ON rep_vendes.oficina_rep = oficines.oficina;
      nom      | oficina_rep |   ciutat    | objectiu  
---------------+-------------+-------------+-----------
 Bill Adams    |          13 | Atlanta     | 350000.00
 Mary Jones    |          11 | New York    | 575000.00
 Sue Smith     |          21 | Los Angeles | 725000.00
 Sam Clark     |          11 | New York    | 575000.00
 Bob Smith     |          12 | Chicago     | 800000.00
 Dan Roberts   |          12 | Chicago     | 800000.00
 Tom Snyder    |             |             |          
 Larry Fitch   |          21 | Los Angeles | 725000.00
 Paul Cruz     |          12 | Chicago     | 800000.00
 Nancy Angelli |          22 | Denver      | 300000.00
(10 rows)


## Exercici 20

Llista totes les combinacions possibles de venedors i ciutats.
SELECT num_empl, nom, ciutat
FROM rep_vendes LEFT JOIN oficines
ON rep_vendes.num_empl = oficines.director ;

 num_empl |      nom      |   ciutat    
----------+---------------+-------------
      108 | Larry Fitch   | Denver
      106 | Sam Clark     | New York
      104 | Bob Smith     | Chicago
      105 | Bill Adams    | Atlanta
      108 | Larry Fitch   | Los Angeles
      101 | Dan Roberts   | 
      109 | Mary Jones    | 
      102 | Sue Smith     | 
      107 | Nancy Angelli | 
      110 | Tom Snyder    | 
      103 | Paul Cruz     | 
(11 rows)
   

## Exercici 21

Per a cada venedor, mostrar el nom, les vendes i la ciutat de l'oficina en cas de tenir-ne una d'assignada.

SELECT nom, rep_vendes.vendes, ciutat 
FROM rep_vendes 
LEFT JOIN oficines 
ON oficina_rep = oficina;
      nom      |  vendes   |   ciutat    
---------------+-----------+-------------
 Bill Adams    | 367911.00 | Atlanta
 Mary Jones    | 392725.00 | New York
 Sue Smith     | 474050.00 | Los Angeles
 Sam Clark     | 299912.00 | New York
 Bob Smith     | 142594.00 | Chicago
 Dan Roberts   | 305673.00 | Chicago
 Tom Snyder    |  75985.00 | 
 Larry Fitch   | 361865.00 | Los Angeles
 Paul Cruz     | 286775.00 | Chicago
 Nancy Angelli | 186042.00 | Denver


## Exercici 22

Mostra les comandes de productes que tenen unes existències inferiors a 10. Llistar el numero de comanda, la data de la comanda, el nom del client que ha fet la comanda, identificador del fabricant i lidentificador de producte de la comanda.

SELECT num_comanda, data, empresa, fabricant, producte, quantitat 
FROM comandes 
JOIN clients 
ON comandes.clie = clients.num_clie 
WHERE comandes.quantitat < 10;

## Exercici 23

Llista les 5 comandes amb un import superior. Mostrar lidentificador de la comanda, import de la comanda, preu del producte, nom del client, nom del representant de vendes que va efectuar la comanda i ciutat de loficina, en cas de tenir oficina assignada.

SELECT num_comanda, import, preu, empresa, nom, ciutat 
FROM comandes 
LEFT JOIN productes 
ON fabricant = id_fabricant AND producte = id_producte 
JOIN clients ON num_clie = clie 
JOIN rep_vendes ON rep_clie = num_empl 
LEFT JOIN oficines ON oficina_rep = oficina 
ORDER BY import DESC;

 num_comanda |  import  |  preu   |      empresa      |      nom      |   ciutat    
-------------+----------+---------+-------------------+---------------+-------------
      113045 | 45000.00 | 4500.00 | Zetacorp          | Larry Fitch   | Los Angeles
      112961 | 31500.00 | 4500.00 | J.P. Sinclair     | Sam Clark     | New York
      113069 | 31350.00 | 1425.00 | Chen Associates   | Paul Cruz     | Chicago
      112987 | 27500.00 | 2750.00 | Acme Mfg.         | Bill Adams    | Atlanta
       88888 | 26000.00 |         | JCP Inc.          | Paul Cruz     | Chicago
      113042 | 22500.00 | 4500.00 | Ian & Schmidt     | Bob Smith     | Chicago
      110036 | 22500.00 | 2500.00 | Ace International | Tom Snyder    | 
      112979 | 15000.00 | 2500.00 | Orion Corp        | Sue Smith     | Los Angeles
      113024 |  7100.00 |  355.00 | Orion Corp        | Sue Smith     | Los Angeles
      113003 |  5625.00 | 1875.00 | Holm & Landis     | Mary Jones    | New York
      113027 |  4104.00 |   76.00 | Acme Mfg.         | Bill Adams    | Atlanta
      112968 |  3978.00 |  117.00 | First Corp.       | Dan Roberts   | Chicago
      113048 |  3750.00 | 1875.00 | Rico Enterprises  | Sue Smith     | Los Angeles
      113012 |  3745.00 |  107.00 | JCP Inc.          | Paul Cruz     | Chicago
      112963 |  3276.00 |  117.00 | Acme Mfg.         | Bill Adams    | Atlanta
      113007 |  2925.00 |  975.00 | Zetacorp          | Larry Fitch   | Los Angeles
      113062 |  2430.00 |  243.00 | Peter Brothers    | Nancy Angelli | Denver
      113065 |  2130.00 |  355.00 | Fred Lewis Corp.  | Sue Smith     | Los Angeles
      112975 |  2100.00 |  350.00 | JCP Inc.          | Paul Cruz     | Chicago
      112993 |  1896.00 |   79.00 | Fred Lewis Corp.  | Sue Smith     | Los Angeles
      113058 |  1480.00 |  148.00 | Holm & Landis     | Mary Jones    | New York
      112989 |  1458.00 |  243.00 | Jones Mfg.        | Sam Clark     | New York
      113051 |  1420.00 |         | Midwest Systems   | Larry Fitch   | Los Angeles
      113049 |   776.00 |  355.00 | Midwest Systems   | Larry Fitch   | Los Angeles
      112992 |   760.00 |   76.00 | Midwest Systems   | Larry Fitch   | Los Angeles
      112983 |   702.00 |  117.00 | Acme Mfg.         | Bill Adams    | Atlanta
      112997 |   652.00 |  652.00 | Peter Brothers    | Nancy Angelli | Denver
      113013 |   652.00 |  652.00 | Midwest Systems   | Larry Fitch   | Los Angeles
      113034 |   632.00 |   79.00 | Ace International | Tom Snyder    | 
      113057 |   600.00 |   25.00 | JCP Inc.          | Paul Cruz     | Chicago
      113055 |   150.00 |   25.00 | Holm & Landis     | Mary Jones    | New York
(31 rows)


## Exercici 24

Llista les comandes que han estat preses per un representant de vendes que no és l'actual representant de vendes del client pel que s'ha realitzat la comanda. Mostrar el número de comanda, el nom del client, el nom de lactual representant de vendes del client com a "rep_cliente" i el nom del representant de vendes que va realitzar la comanda com a "rep_pedido".

SELECT num_comanda, empresa, rep_clie.nom AS rep_cliente, rep_vendes.nom AS rep_pedido 
FROM comandes LEFT JOIN clients 
ON clie = num_clie AND rep <> rep_clie 
LEFT JOIN rep_vendes 
ON comandes.rep = rep_vendes.num_empl; 

 num_comanda |     empresa     | rep_cliente |  rep_pedido   
-------------+-----------------+-------------+---------------
      113012 | JCP Inc.        | Paul Cruz   | Bill Adams
      113024 | Orion Corp      | Sue Smith   | Larry Fitch
      113069 | Chen Associates | Paul Cruz   | Nancy Angelli
      113055 | Holm & Landis   | Mary Jones  | Dan Roberts
      113042 | Ian & Schmidt   | Bob Smith   | Dan Roberts
(5 rows)

## Exercici 25

Llista les comandes amb un import superior a 5000 i també aquelles comandes realitzades per un client amb un crèdit inferior a 30000. Mostrar lidentificador de la comanda, el nom del client i el nom del representant de vendes que va prendre la comanda.

SELECT num_comanda, empresa, nom, import, limit_credit 
FROM comandes LEFT JOIN clients 
ON comandes.clie = clients.num_clie 
LEFT JOIN rep_vendes 
ON clients.rep_clie = rep_vendes.num_empl 
WHERE comandes.import> 5000 
AND clients.limit_credit < 30000;

 num_comanda |     empresa     |    nom    |  import  | limit_credit 
-------------+-----------------+-----------+----------+--------------
      113024 | Orion Corp      | Sue Smith |  7100.00 |     20000.00
      112979 | Orion Corp      | Sue Smith | 15000.00 |     20000.00
      113069 | Chen Associates | Paul Cruz | 31350.00 |     25000.00
      113042 | Ian & Schmidt   | Bob Smith | 22500.00 |     20000.00
(4 rows)

