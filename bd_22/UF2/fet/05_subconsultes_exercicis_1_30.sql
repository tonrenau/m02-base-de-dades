# Subconsultes

## Exercici 0

Torna a fer lexercici 4 de les diapositives de teoria de Subconsultes, però
ara fent servir GROUP BY i la funció de group STRING_AGG. Recordem que la
subconsulta era:

_Mostreu els empleats que treballen a la mateixa oficina i que tenen el mateix
càrrec._
  


```  
SELECT STRING_AGG(nom, ',') AS noms, oficina_rep, carrec
  FROM rep_vendes
 GROUP BY oficina_rep, carrec
HAVING COUNT(num_empl) > 1 ;
```

```
         noms          | oficina_rep |       carrec
-----------------------+-------------+---------------------
 Dan Roberts,Paul Cruz |          12 | Representant Vendes
(1 row)
```

## Exercici 1

Llista els venedors que tinguin una quota igual o inferior a l'objectiu de
l'oficina de vendes dAtlanta.

select * from rep_vendes 
where quota <= (select objectiu from oficines where ciutat = 'Atlanta');

      nom      |   quota   | objectiu  
---------------+-----------+-----------
 Bill Adams    | 350000.00 | 350000.00
 Mary Jones    | 300000.00 | 350000.00
 Sue Smith     | 350000.00 | 350000.00
 Sam Clark     | 275000.00 | 350000.00
 Bob Smith     | 200000.00 | 350000.00
 Dan Roberts   | 300000.00 | 350000.00
 Larry Fitch   | 350000.00 | 350000.00
 Paul Cruz     | 275000.00 | 350000.00
 Nancy Angelli | 300000.00 | 350000.00
(9 rows)

## Exercici 2

Tots els clients, identificador i nom de lempresa, que han estat atesos per
(que han fet comanda amb) Bill Adams.

SELECT DISTINCT num_clie, empresa 
FROM clients 
JOIN comandes ON num_clie = clie
WHERE rep = (SELECT num_empl 
		FROM rep_vendes 
		WHERE nom = 'Bill Adams');
		
 num_clie |  empresa  
----------+-----------
     2103 | Acme Mfg.
     2111 | JCP Inc.
(2 rows)

## Exercici 3

Venedors amb quotes que siguin iguals o superiors a lobjectiu de llur oficina
de vendes.

select * from rep_vendes
where quota >= (select objectiu from oficina where oficina = oficina_rep);

      nom      
---------------
 Bill Adams
 Nancy Angelli
(2 rows)

## Exercici 4

Mostrar lidentificador de l'oficina i la ciutat de les oficines on l'objectiu
de vendes de l'oficina excedeix la suma de quotes dels venedors d'aquella
oficina.


SELECT oficina, ciutat 
FROM oficines 
WHERE objectiu > (SELECT SUM(quota) 
		FROM rep_vendes 
		WHERE oficina = oficina_rep);
 oficina |   ciutat    
---------+-------------
      12 | Chicago
      21 | Los Angeles
(2 rows)


## Exercici 5 

Llista dels productes del fabricant amb identificador "aci" l'estoc dels quals
superen l'estoc del producte amb identificador de producte "41004" i
identificador de fabricant "aci".

SELECT id_producte, id_fabricant, descripcio
  FROM productes
 WHERE id_fabricant = 'aci'
   AND estoc >
       (SELECT estoc 
          FROM productes 
         WHERE (id_fabricant, id_producte) = ('aci','41004'));

 id_producte | id_fabricant |   descripcio    
-------------+--------------+-----------------
 41003       | aci          | Article Tipus 3
 41001       | aci          | Article Tipus 1
 41002       | aci          | Article Tipus 2
(3 rows)


## Exercici 6

Llistar els venedors que han acceptat una comanda que representa més del 10% de
la seva quota.

SELECT DISTINCT num_empl, nom, quota, quota*0.1
FROM rep_vendes 
JOIN comandes ON rep = num_empl  
WHERE import > (SELECT quota *0.1 
		FROM rep_vendes 
		WHERE rep = num_empl) ;
		
 num_empl |      nom      |   quota   | ?column?  
----------+---------------+-----------+-----------
      106 | Sam Clark     | 275000.00 | 27500.000
      107 | Nancy Angelli | 300000.00 | 30000.000
      108 | Larry Fitch   | 350000.00 | 35000.000
(3 rows)



SELECT DISTINCT num_empl, nom, quota, quota*0.1
FROM rep_vendes 
JOIN comandes ON rep = num_empl  
WHERE quota*0.1 <= any (SELECT import 
		FROM comandes 
		WHERE rep = num_empl) ;
		
 num_empl |      nom      |   quota   | ?column?  
----------+---------------+-----------+-----------
      106 | Sam Clark     | 275000.00 | 27500.000
      107 | Nancy Angelli | 300000.00 | 30000.000
      108 | Larry Fitch   | 350000.00 | 35000.000
(3 rows)

## Exercici 7

Llistar el nom i l'edat de totes les persones de l'equip de vendes que no dirigeixen una oficina.

SELECT nom, edat 
FROM rep_vendes 
WHERE oficina_rep NOT IN (SELECT oficina FROM oficines WHERE director = num_empl);


SELECT nom, edat 
FROM rep_vendes 
WHERE oficina_rep <> all (SELECT oficina FROM oficines WHERE director = num_empl);


      nom      | edat 
---------------+------
 Mary Jones    |   31
 Sue Smith     |   48
 Dan Roberts   |   45
 Tom Snyder    |   41
 Paul Cruz     |   29
 Nancy Angelli |   49
(6 rows)


SELECT nom, edat 
FROM rep_vendes 
WHERE oficina_rep = any (SELECT oficina FROM oficines WHERE director = num_empl);

SELECT nom, edat 
FROM rep_vendes 
WHERE NOT EXISTS (SELECT director FROM oficines WHERE director = num_empl);

     nom     | edat 
-------------+------
 Bill Adams  |   37
 Sam Clark   |   52
 Bob Smith   |   33
 Larry Fitch |   62
(4 rows)


## Exercici 8

Llistar aquelles oficines, i els seus objectius, els venedors de les quals
tenen unes vendes que superen el 50% de l'objectiu de l'oficina.

SELECT oficina, objectiu 
FROM oficines 
WHERE objectiu * 0.5 < all (SELECT vendes 
			FROM rep_vendes 
			WHERE oficina_rep = oficina); 
 oficina | objectiu  
---------+-----------
      22 | 300000.00
      11 | 575000.00
      13 | 350000.00
(3 rows)


## Exercici 9

Llistar aquells clients els representants de vendes dels quals estàn assignats
a oficines de la regió Est.

SELECT num_clie, nom
FROM clients JOIN rep_vendes ON rep_clie = num_empl 
WHERE oficina_rep IN ( SELECT oficina 
			FROM oficines
			WHERE regio = 'Est');
 num_clie |     nom     
----------+-------------
     2111 | Paul Cruz
     2102 | Dan Roberts
     2103 | Bill Adams
     2115 | Dan Roberts
     2101 | Sam Clark
     2121 | Paul Cruz
     2108 | Mary Jones
     2117 | Sam Clark
     2122 | Bill Adams
     2119 | Mary Jones
     2113 | Bob Smith
     2109 | Paul Cruz
     2105 | Dan Roberts
(13 rows)


## Exercici 10

Llistar els venedors que treballen en oficines que superen el seu objectiu.

SELECT nom       
FROM rep_vendes 
WHERE oficina_rep IS NOT NULL AND 
	vendes > any (SELECT objectiu 
			FROM oficines);
     nom     
-------------
 Bill Adams
 Mary Jones
 Sue Smith
 Dan Roberts
 Larry Fitch
(5 rows)


## Exercici 11

Llistar els venedors que treballen en oficines que superen el seu objectiu.
Mostrar també les següents dades de l'oficina: ciutat i la diferència entre les
vendes i l'objectiu. Ordenar el resultat per aquest últim valor. 
Proposa dues sentències SQL, una amb subconsultes i una sense.

SELECT num_empl, nom, ciutat, oficines.vendes - objectiu AS dif
  FROM rep_vendes 
       JOIN oficines
	   ON oficina_rep = oficina
 WHERE oficina_rep IN
       (SELECT oficina 
       	FROM oficines
	WHERE vendes > objectiu)
 ORDER BY dif;
```
```
 num_empl |     nom     |   ciutat    |    dif
----------+-------------+-------------+-----------
      105 | Bill Adams  | Atlanta     |  17911.00
      102 | Sue Smith   | Los Angeles | 110915.00
      108 | Larry Fitch | Los Angeles | 110915.00
      109 | Mary Jones  | New York    | 117637.00
      106 | Sam Clark   | New York    | 117637.00
(5 rows)


## Exercici 12

Llista els venedors que no treballen en oficines dirigides per Larry Fitch, o
que no treballen a cap oficina. Sense usar consultes multitaula.

SELECT num_empl, nom 
FROM rep_vendes
WHERE oficina_rep IS NULL 
	OR oficina_rep NOT IN (SELECT oficina 
				FROM oficines 
				WHERE director = (SELECT num_empl 
							FROM rep_vendes 
							WHERE nom = 'Larry Fitch'));

 num_empl |     nom
----------+-------------
      105 | Bill Adams
      109 | Mary Jones
      106 | Sam Clark
      104 | Bob Smith
      101 | Dan Roberts
      110 | Tom Snyder
      103 | Paul Cruz
(7 rows)

## Exercici 13

Llista els venedors que no treballen en oficines dirigides per Larry Fitch, o
que no treballen a cap oficina. Mostrant també la ciutat de loficina on
treballa l'empleat i l'identificador del cap de la oficina. Proposa dues
sentències SQL, una amb subconsultes i una sense.

SELECT num_empl, nom, ciutat 
  FROM rep_vendes 
       JOIN oficines
	   ON oficina_rep = oficina
 WHERE oficina_rep IS NULL
    OR oficina_rep NOT IN
	   (SELECT oficina 
          FROM oficines 
         WHERE director =
		       (SELECT num_empl
			      FROM rep_vendes 
                	 WHERE nom = 'Larry Fitch'));

 num_empl |     nom     |  ciutat
----------+-------------+----------
      105 | Bill Adams  | Atlanta
      109 | Mary Jones  | New York
      106 | Sam Clark   | New York
      104 | Bob Smith   | Chicago
      101 | Dan Roberts | Chicago
      103 | Paul Cruz   | Chicago
(6 rows)

## Exercici 14

Llista tots els clients que han realitzat comandes del productes de la família
ACI Widgets entre gener i juny del 1990. Els productes de la famíla ACI Widgets
són aquells que tenen identificador de fabricant "aci" i que lidentificador
del producte comença per "4100".

select * from clients 
where empresa = 'ACI Widgets' and 
(select )



SELECT empresa
  FROM clients
 WHERE num_clie IN
       (SELECT DISTINCT clie
          FROM comandes
         WHERE fabricant = 'aci'
           AND producte LIKE '4100%'
           AND data BETWEEN '1990-01-01' AND '1990-06-30');
```
```
      empresa      
-------------------
 Acme Mfg.
 Ace International
 Holm & Landis
 JCP Inc.
(4 rows)

## Exercici 15

Llista els clients que no tenen cap comanda.

SELECT empresa
  FROM clients
 WHERE num_clie NOT IN
       (SELECT clie
	      FROM comandes);
```

```
     empresa
-----------------
 Carter & Sons
 Smithson Corp.
 QMA Assoc.
 Three-Way Lines
 Solomon Inc.
 AAA Investments
(6 rows)

## Exercici 16

Llista els clients que tenen assignat el venedor que porta més temps contractat.

SELECT empresa
  FROM clients
 WHERE rep_clie =
       (SELECT num_empl
	      FROM rep_vendes 
         WHERE data_contracte =
		       (SELECT MIN(data_contracte)
			      FROM rep_vendes));
```
```
     empresa     
-----------------
 First Corp.
 Smithson Corp.
 AAA Investments
(3 rows)

## Exercici 17

Llista els clients assignats a Sue Smith que no han fet cap comanda amb un
import superior a 30000. Proposa una sentència SQL sense usar multitaula i una
altre en que susi multitaula i subconsultes.

SELECT empresa 
FROM clients 
WHERE rep_clie = ANY (SELECT rep FROM comandes WHERE import < 30000 AND rep = 
						(SELECT num_empl FROM rep_vendes WHERE nom = 'Sue Smith'));
     empresa      
------------------
 Carter & Sons
 Orion Corp
 Rico Enterprises
 Fred Lewis Corp.
(4 rows)

## Exercici 18

Llista l'identificador i el nom dels caps d'empleats que tenen més de 40 anys i
que dirigeixen un venedor que té unes vendes superiors a la seva pròpia quota.

SELECT nom 
  FROM rep_vendes 
 WHERE edat > 40
   AND num_empl IN
	   (SELECT cap 
          FROM rep_vendes 
         WHERE vendes > quota);
```
```
     nom
-------------
 Sam Clark
 Larry Fitch
(2 rows)

## Exercici 19

Llista d'oficines on hi hagi algun venedor tal que la seva quota representi més
del 50% de l'objectiu de loficina

SELECT ciutat 
  FROM oficines
WHERE objectiu * 0.5 < ANY
      (SELECT quota
	     FROM rep_vendes 
        WHERE oficina_rep = oficina);
```
```
  ciutat
----------
 Denver
 New York
 Atlanta
(3 rows)

## Exercici 20

Llista doficines on tots els venedors tinguin la seva quota superior al 55% de
l'objectiu de l'oficina.

SELECT ciutat 
  FROM oficines
 WHERE objectiu * 0.55 < ALL
       (SELECT quota
	      FROM rep_vendes 
         WHERE oficina_rep = oficina);
```
```
 ciutat
---------
 Denver
 Atlanta
(2 rows)

## Exercici 21

Transforma el següent JOIN a una comanda amb subconsultes:

SELECT num_comanda, import, clie, num_clie, limit_credit
  FROM comandes
       JOIN clients
       ON clie = num_clie;
```

```
 num_comanda |  import  | clie | num_clie | limit_credit
-------------+----------+------+----------+--------------
      112961 | 31500.00 | 2117 |     2117 |     35000.00
      113012 |  3745.00 | 2111 |     2111 |     50000.00
...
      112987 | 27500.00 | 2103 |     2103 |     50000.00
      113057 |   600.00 | 2111 |     2111 |     50000.00
      113042 | 22500.00 | 2113 |     2113 |     20000.00
(30 rows)

## Exercici 22

Transforma el següent JOIN a una comanda amb subconsultes:

SELECT empl.nom, empl.quota, cap.nom, cap.quota
  FROM rep_vendes AS empl
       JOIN rep_vendes AS cap
       ON empl.cap = cap.num_empl
 WHERE empl.quota > cap.quota;
```

```

     nom     |   quota   |    nom    |   quota   
-------------+-----------+-----------+-----------
 Bill Adams  | 350000.00 | Bob Smith | 200000.00
 Mary Jones  | 300000.00 | Sam Clark | 275000.00
 Dan Roberts | 300000.00 | Bob Smith | 200000.00
 Larry Fitch | 350000.00 | Sam Clark | 275000.00
 Paul Cruz   | 275000.00 | Bob Smith | 200000.00
(5 rows)


## Exercici 23

Transforma la següent consulta amb un ANY a una consulta amb un EXISTS i també
en una altre consulta amb un ALL:


SELECT oficina
  FROM oficines
 WHERE vendes * 0.8 < ANY 
       (SELECT vendes
          FROM rep_vendes
         WHERE oficina_rep = oficina);

    oficina 
---------
      22
      13
(2 rows)
## Exercici 24

Transforma la següent consulta amb un ALL a una consulta amb un EXISTS i també
en una altre consulta amb un ANY:


SELECT num_clie
  FROM clients
 WHERE limit_credit < ALL
       (SELECT import
          FROM comandes
         WHERE num_clie = clie);

       num_clie 
----------
     2123
     2115
     2121
     2122
     2119
     2113
     2109
     2105
(8 rows)
                           
## Exercici 25

Transforma la següent consulta amb un EXISTS a una consulta amb un ALL i també
a una altre consulta amb un ANY:

SELECT num_clie, empresa
  FROM clients
 WHERE EXISTS
       (SELECT *
          FROM rep_vendes
         WHERE rep_clie = num_empl
           AND edat BETWEEN 40 AND 50);
```

```
 num_clie |      empresa      
----------+-------------------
     2102 | First Corp.
     2123 | Carter & Sons
     2107 | Ace International
     2115 | Smithson Corp.
     2114 | Orion Corp
     2124 | Peter Brothers
     2120 | Rico Enterprises
     2106 | Fred Lewis Corp.
     2105 | AAA Investments
(9 rows)

## Exercici 26

Transforma la següent consulta amb subconsultes a una consulta sense
subconsultes.

SELECT *
  FROM productes
 WHERE EXISTS (SELECT * FROM comandes
			WHERE quantitat > 30
			AND producte=id_producte AND fabricant=id_fabricant);


 id_fabricant | id_producte |   descripcio    |  preu  | estoc 
--------------+-------------+-----------------+--------+-------
 aci          | 41003       | Article Tipus 3 | 107.00 |   207
 aci          | 41004       | Article Tipus 4 | 117.00 |   139
 aci          | 41002       | Article Tipus 2 |  76.00 |   167
(3 rows)

## Exercici 27

Transforma la següent consulta amb subconsultes a una consulta sense
subconsultes.

SELECT num_empl, nom
  FROM rep_vendes
 WHERE num_empl = ANY 
       (SELECT rep_clie
          FROM clients
         WHERE empresa LIKE '%Inc.');
```
```
 num_empl |    nom     
----------+------------
      109 | Mary Jones
      103 | Paul Cruz
(2 rows)

## Exercici 28

Transforma la següent consulta amb un IN a una consulta amb un EXISTS i també a
una altre consulta amb un ALL.

SELECT num_empl, nom
  FROM rep_vendes
 WHERE num_empl IN
       (SELECT cap
          FROM rep_vendes);
```

```
 num_empl |     nom     
----------+-------------
      106 | Sam Clark
      104 | Bob Smith
      101 | Dan Roberts
      108 | Larry Fitch
(4 rows)

## Exercici 29

Modifica la següent consulta perquè mostri la ciutat de loficina, proposa una
consulta simplificada.

SELECT num_comanda
  FROM comandes
 WHERE rep IN (SELECT num_empl FROM rep_vendes
		 WHERE vendes > (SELECT avg(vendes) FROM rep_vendes)
             AND oficina_rep IN (SELECT oficina FROM oficines WHERE regio LIKE 'Est') );


 num_comanda 
-------------
      112961
      113012
      112989
      112968
      112963
      113058
      112983
      113027
      113055
      113003
      112987
      113042
(12 rows)

## Exercici 30

Transforma la següent consulta amb subconsultes a una consulta amb les mínimes
subconsultes possibles.


SELECT num_clie, empresa,
       (SELECT nom
          FROM rep_vendes
         WHERE rep_clie = num_empl) AS rep_nom
  FROM clients
 WHERE rep_clie = ANY
       (SELECT num_empl
          FROM rep_vendes
         WHERE vendes >
               (SELECT MAX(quota)
                  FROM rep_vendes));
                  
 num_clie |     empresa      |     nom     
----------+------------------+-------------
     2103 | Acme Mfg.        | Bill Adams
     2123 | Carter & Sons    | Sue Smith
     2112 | Zetacorp         | Larry Fitch
     2114 | Orion Corp       | Sue Smith
     2108 | Holm & Landis    | Mary Jones
     2122 | Three-Way Lines  | Bill Adams
     2120 | Rico Enterprises | Sue Smith
     2106 | Fred Lewis Corp. | Sue Smith
     2119 | Solomon Inc.     | Mary Jones
     2118 | Midwest Systems  | Larry Fitch
(10 rows)

