1. Visualitza limport total i el número de comandes per producte que shan fet dels productes que tenen un codi 
que comneça per 4 i acaba en 2, en 3 o en 4 i dels quals shan fet 3 o més comandes.

SELECT producte, fabricant,count(*), sum(import)
from comandes 
where producte like '%4' and (producte like '%2' or producte like '%3' or producte like '%4')
group by 1,2
having count(*) >= 3; 

 fabricant | producte | count |   sum   
-----------+----------+-------+---------
 aci       | 41004    |     3 | 7956.00
(1 row)

--> Visualitza limport total i el número de comandes per producte que shan fet dels productes que tenen un codi que conte un 4 i acaba en 2, en 3 o en 7

SELECT fabricant, producte, COUNT(*), sum(import)
FROM comandes LEFT JOIN productes 
ON id_fabricant = fabricant and id_producte = producte 
WHERE producte LIKE '%4%' AND (producte LIKE '%2' OR producte LIKE '%3' OR producte LIKE '%7') 
GROUP BY fabricant, producte; 

 fabricant | producte | count |   sum    
-----------+----------+-------+----------
 aci       | 41003    |     1 |  3745.00
 aci       | 41002    |     2 |  4864.00
 bic       | 41003    |     2 |  1304.00
 qsa       | k47      |     1 |  1420.00
 qsa       | xk47     |     3 | 10006.00
(5 rows)


--> Visualitza limport total i el número de comandes per producte que shan fet dels productes que tenen un codi que conte un 4 i acaba en 2 de la fabrica aci, o en 3 de la fabrica bic o en 7

SELECT fabricant, producte, COUNT(*), sum(import)
FROM comandes LEFT JOIN productes
ON id_fabricant = fabricant and id_producte = producte 
WHERE producte LIKE '%4%' AND (producte LIKE '%2' AND id_fabricant = 'aci' OR producte LIKE '%3' AND id_fabricant = 'bic' OR producte LIKE '%7') 
GROUP BY fabricant, producte; 

 fabricant | producte | count |   sum    
-----------+----------+-------+----------
 bic       | 41003    |     2 |  1304.00
 aci       | 41002    |     2 |  4864.00
 qsa       | k47      |     1 |  1420.00
 qsa       | xk47     |     3 | 10006.00
(4 rows)

2. Volem saber quants clients diferents ens han fet comandes de més de 7 unitats de productes de les fàbriques aci i rei

SELECT COUNT(DISTINCT clie) FROM comandes WHERE (fabricant = 'rei' OR fabricant = 'aci') AND quantitat > 7;
 count 
-------
     7
(1 row)
3. Estem interessats en els clients diferents ens han fet comandes de més de 7 unitats de productes de les fàbriques aci i rei. Volem saber quins codis de clients ens han fet dues o més comandes daquestes característiques.

SELECT COUNT(*), clie
FROM comandes 
WHERE (fabricant = 'aci' OR fabricant = 'rei') AND (quantitat > 7)
GROUP BY clie
HAVING COUNT (*) >= 2;
 count | clie 
-------+------
     3 | 2103
     2 | 2107
     2 | 2111
(3 rows)


4. Volem saber quin fabricant és el que té més unitats dels seus productes als nostres magatzems. Visualitza el codi de la fàbrica i el total dunitats dels seus productes que tenim.

SELECT id_fabricant, SUM(estoc)
FROM productes
GROUP BY id_fabricant
ORDER BY 2 DESC 
LIMIT 1;

id_fabricant | sum 
--------------+-----
 aci          | 880
(1 row)


5. Entre els nostres productes nhi ha que tenen el mateix nom(descripcio). Visualitza la descripcio daquests productes i el preu del més car.

SELECT descripcio,COUNT(*), MAX(preu) 
FROM productes 
GROUP BY descripcio 
HAVING COUNT (*) >= 2;

 descripcio | count |  max   
------------+-------+--------
 Reductor   |     3 | 355.00
(1 row)


6. Volem saber a quines oficines treballen venedors amb un cognom que comenci per A o per S, I un nom que comenci amb T o amb S o amb B o amb N. També volem saber quants trebalaldors daquestes característiqes hi treballen I quina és la suma de les seves quotes per oficina. No volem que es visualitzi el Tom Snyder perquè ell no pertany a cap oficina en particular.

SELECT ciutat, oficina, COUNT(*), SUM(quota)
FROM rep_vendes LEFT JOIN oficines ON oficina_rep = oficina 
WHERE (nom LIKE '% A%' OR nom LIKE '% S%') AND (nom LIKE 'T%' OR nom LIKE 'S%' OR nom LIKE 'B%' OR nom LIKE 'N%') AND oficina_rep IS NOT NULL  
GROUP BY  oficina, ciutat;

   ciutat    | oficina | count |    sum    
-------------+---------+-------+-----------
 Chicago     |      12 |     1 | 200000.00
 Atlanta     |      13 |     1 | 350000.00
 Los Angeles |      21 |     1 | 350000.00
 Denver      |      22 |     1 | 300000.00
(4 rows)

7. Quins són els codis dels representants que tenen assignats més de 2 clients amb un nom dempresa que contingui la lletra 'o' i la lletra 'i'?


SELECT rep_clie, COUNT(*)                  
FROM clients 
WHERE empresa LIKE '%o%' AND empresa LIKE '%i%' 
GROUP BY rep_clie 
HAVING COUNT(*) > 2;

 rep_clie | count 
----------+-------
      102 |     3
(1 row)

8. Quins codis de clients ens han comprat 3 o més vegades?
SELECT clie, COUNT(*) 
FROM comandes 
GROUP BY clie 
HAVING COUNT(*) >=3;
 clie | count 
------+-------
 2108 |     3
 2103 |     4
 2118 |     4
 2111 |     3
(4 rows)


9. Quins codis de clients ens han comprat més duna vegada el mateix producte?

SELECT clie, fabricant, producte, COUNT(*)
FROM comandes
GROUP BY clie, fabricant, producte
HAVING COUNT (*)> 1;

 clie | fabricant | producte | count 
------+-----------+----------+-------
 2103 | aci       | 41004    |     2
(1 row)


10. D'entre els productes que s'han venut més duna vegada, quin és els que ens ha donat un total d'import més baix? Visualitza el codi del producta, la quantitat de cops que s'ha venut I el total d'import que s'ha cobrat per aquest producte.
    
SELECT fabricant,producte,SUM(import),COUNT(*) 
FROM comandes 
GROUP BY fabricant,producte 
HAVING COUNT(*) > 1 
ORDER BY 3 
LIMIT 1;

 fabricant | producte |  sum   | count 
-----------+----------+--------+-------
 aci       | 4100x    | 750.00 |     2
(1 row)
   
11. Per cada venedor (o treballador o reprentant de ventes), mostrar lidentificador del venedor i un camp anomenat "preu0". El camp "preu0" ha de contenir el preu del producte més car que ha venut. 

SELECT rep, MAX(preu) as preu0
FROM productes JOIN comandes ON id_fabricant = fabricant AND id_producte = producte
GROUP BY 1 
ORDER BY 1;

 rep |  preu0  
-----+---------
 101 | 4500.00
 102 | 2500.00
 103 |  350.00
 105 | 2750.00
 106 | 4500.00
 107 | 1425.00
 108 | 4500.00
 109 | 1875.00
 110 | 2500.00
(9 rows)

12. Mostrar lidentificador del fabricant i un camp anomenat "m_preu". El camp "m_preu" ha de mostrar la mitjana del preu dels productes de cada fabricant. 

SELECT id_fabricant, ROUND(AVG(preu),2) AS m_preu 
FROM productes 
GROUP BY 1;
 id_fabricant | m_preu  
--------------+---------
 imm          |  842.33
 aci          |  804.29
 bic          |  352.33
 fea          |  195.50
 qsa          |  202.00
 rei          | 2357.25
(6 rows)

13. Per cada client que ha fet alguna compra, mostrar lidentificador del client i un camp anomenat "comandes_fetes". El camp "comandes_fetes" ha de mostrar quantes comandes ha fet cada client. 

SELECT clie, COUNT(*) AS comandes_fetes 
FROM comandes 
GROUP BY 1 
ORDER BY 1;

 clie | comandes_fetes 
------+----------------
 2101 |              1
 2102 |              1
 2103 |              4
 2106 |              2
 2107 |              2
 2108 |              3
 2109 |              1
 2111 |              3
 2112 |              2
 2113 |              1
 2114 |              2
 2117 |              1
 2118 |              4
 2120 |              1
 2124 |              2
(15 rows)
0

14. Per cada client mostrar lidentificador. Només mostrar aquells clients que la suma dels imports de les seves comandes sigui menor al limit de crèdit. 

SELECT num_clie, limit_credit, SUM(import) 
FROM comandes JOIN clients ON clie = num_clie 
GROUP BY 1 
HAVING SUM(import) < limit_credit 
ORDER BY 1;
 num_clie | limit_credit |   sum    
----------+--------------+----------
     2101 |     65000.00 |  1458.00
     2102 |     65000.00 |  3978.00
     2103 |     50000.00 | 35582.00
     2106 |     65000.00 |  4026.00
     2107 |     35000.00 | 23132.00
     2108 |     55000.00 |  7255.00
     2111 |     50000.00 |  6445.00
     2112 |     50000.00 | 47925.00
     2117 |     35000.00 | 31500.00
     2118 |     60000.00 |  3608.00
     2120 |     50000.00 |  3750.00
     2124 |     40000.00 |  3082.00
(12 rows)


--> Mostrar el nom de l'empresa-client, el nom del representant de vendes que ha fet la comanda i el nom del representant de vendes que l'empresa-client te assignat

 SELECT num_clie,empresa, rep_vendes.nom as "venedor de la comanda", rep_client.nom as "representant del client", limit_credit, SUM(import)                
FROM clients JOIN comandes ON clie = num_clie 
JOIN rep_vendes ON rep = num_empl 
JOIN rep_vendes AS rep_client ON rep_clie = rep_client.num_empl
GROUP BY num_clie, empresa, rep_vendes.nom, rep_client.nom 
HAVING SUM(import) < limit_credit 
ORDER BY  1;

 num_clie |      empresa      | venedor de la comanda | representant del client | limit_credit |   sum    
----------+-------------------+-----------------------+-------------------------+--------------+----------
     2101 | Jones Mfg.        | Sam Clark             | Sam Clark               |     65000.00 |  1458.00
     2102 | First Corp.       | Dan Roberts           | Dan Roberts             |     65000.00 |  3978.00
     2103 | Acme Mfg.         | Bill Adams            | Bill Adams              |     50000.00 | 35582.00
     2106 | Fred Lewis Corp.  | Sue Smith             | Sue Smith               |     65000.00 |  4026.00
     2107 | Ace International | Tom Snyder            | Tom Snyder              |     35000.00 | 23132.00
     2108 | Holm & Landis     | Dan Roberts           | Mary Jones              |     55000.00 |   150.00
     2108 | Holm & Landis     | Mary Jones            | Mary Jones              |     55000.00 |  7105.00
     2111 | JCP Inc.          | Bill Adams            | Paul Cruz               |     50000.00 |  3745.00
     2111 | JCP Inc.          | Paul Cruz             | Paul Cruz               |     50000.00 |  2700.00
     2112 | Zetacorp          | Larry Fitch           | Larry Fitch             |     50000.00 | 47925.00
     2114 | Orion Corp        | Larry Fitch           | Sue Smith               |     20000.00 |  7100.00
     2114 | Orion Corp        | Sue Smith             | Sue Smith               |     20000.00 | 15000.00
     2117 | J.P. Sinclair     | Sam Clark             | Sam Clark               |     35000.00 | 31500.00
     2118 | Midwest Systems   | Larry Fitch           | Larry Fitch             |     60000.00 |  3608.00
     2120 | Rico Enterprises  | Sue Smith             | Sue Smith               |     50000.00 |  3750.00
     2124 | Peter Brothers    | Nancy Angelli         | Nancy Angelli           |     40000.00 |  3082.00
(16 rows)



15. Mostrar l'identificador i la ciutat de les oficines i dos camps més, un anomenat "credit1" i l'altre "credit2". Per a cada oficina, el camp "credit1" ha de mostrar el límit de crèdit més petit d'entre tots els clients que el seu representant de vendes treballa a l'oficina. El camp "credit2" ha de ser el mateix però pel límit de crèdit més gran. 

SELECT oficina,ciutat,MIN(limit_credit) AS credit1, MAX(limit_credit) AS credit2 
FROM rep_vendes LEFT JOIN oficines ON oficina_rep = oficina 
LEFT JOIN clients ON num_empl = rep_clie 
GROUP BY 1,2;

 oficina |   ciutat    | credit1  | credit2  
---------+-------------+----------+----------
         |             | 35000.00 | 35000.00
      22 | Denver      | 40000.00 | 40000.00
      13 | Atlanta     | 30000.00 | 50000.00
      11 | New York    | 25000.00 | 65000.00
      21 | Los Angeles | 20000.00 | 65000.00
      12 | Chicago     | 20000.00 | 65000.00
(6 rows)


16. Per cada venedor i cadascun dels seus clients, mostrar lidentificador del venedor, lidentificador del client i un camp anomenat "import_m". El camp "impore_m" ha de mostrar limport mig de les comandes que ha realitzat cada venedor a cada client diferent. 

SELECT num_empl, nom, num_clie,empresa, ROUND(AVG(import),2) AS "import m" 
FROM rep_vendes JOIN comandes ON num_empl = rep 
JOIN clients ON clie = num_clie
GROUP BY 1,2,3,4
ORDER BY 1,2,3,4;

 num_empl |      nom      | num_clie |      empresa      | import m 
----------+---------------+----------+-------------------+----------
      101 | Dan Roberts   |     2102 | First Corp.       |  3978.00
      101 | Dan Roberts   |     2108 | Holm & Landis     |   150.00
      101 | Dan Roberts   |     2113 | Ian & Schmidt     | 22500.00
      102 | Sue Smith     |     2106 | Fred Lewis Corp.  |  2013.00
      102 | Sue Smith     |     2114 | Orion Corp        | 15000.00
      102 | Sue Smith     |     2120 | Rico Enterprises  |  3750.00
      103 | Paul Cruz     |     2111 | JCP Inc.          |  1350.00
      105 | Bill Adams    |     2103 | Acme Mfg.         |  8895.50
      105 | Bill Adams    |     2111 | JCP Inc.          |  3745.00
      106 | Sam Clark     |     2101 | Jones Mfg.        |  1458.00
      106 | Sam Clark     |     2117 | J.P. Sinclair     | 31500.00
      107 | Nancy Angelli |     2109 | Chen Associates   | 31350.00
      107 | Nancy Angelli |     2124 | Peter Brothers    |  1541.00
      108 | Larry Fitch   |     2112 | Zetacorp          | 23962.50
      108 | Larry Fitch   |     2114 | Orion Corp        |  7100.00
      108 | Larry Fitch   |     2118 | Midwest Systems   |   902.00
      109 | Mary Jones    |     2108 | Holm & Landis     |  3552.50
      110 | Tom Snyder    |     2107 | Ace International | 11566.00
(18 rows)
    
17. Per cada comanda que tenim, visualitzar la regió de loficina del representant de ventes que lha fet,  el nom del representant que l'ha fet, l'import i el nom de lempresa que ha comprat el producte. Mostrar-ho ordenat pels 2 primers camps demanats.

SELECT oficina,regio,nom,import,empresa 
FROM oficines
JOIN rep_vendes 
ON oficina_rep = oficina
JOIN comandes
ON rep = num_empl
JOIN clients
ON num_clie = clie 
ORDER BY 2,3;

 oficina | regio |      nom      |  import  |     empresa      
---------+-------+---------------+----------+------------------
      13 | Est   | Bill Adams    |   702.00 | Acme Mfg.
      13 | Est   | Bill Adams    |  3276.00 | Acme Mfg.
      13 | Est   | Bill Adams    | 27500.00 | Acme Mfg.
      13 | Est   | Bill Adams    |  4104.00 | Acme Mfg.
      13 | Est   | Bill Adams    |  3745.00 | JCP Inc.
      12 | Est   | Dan Roberts   | 22500.00 | Ian & Schmidt
      12 | Est   | Dan Roberts   |  3978.00 | First Corp.
      12 | Est   | Dan Roberts   |   150.00 | Holm & Landis
      11 | Est   | Mary Jones    |  1480.00 | Holm & Landis
      11 | Est   | Mary Jones    |  5625.00 | Holm & Landis
      12 | Est   | Paul Cruz     |  2100.00 | JCP Inc.
      12 | Est   | Paul Cruz     |   600.00 | JCP Inc.
      11 | Est   | Sam Clark     | 31500.00 | J.P. Sinclair
      11 | Est   | Sam Clark     |  1458.00 | Jones Mfg.
      21 | Oest  | Larry Fitch   |  1420.00 | Midwest Systems
      21 | Oest  | Larry Fitch   |  2925.00 | Zetacorp
      21 | Oest  | Larry Fitch   |  7100.00 | Orion Corp
      21 | Oest  | Larry Fitch   |   760.00 | Midwest Systems
      21 | Oest  | Larry Fitch   | 45000.00 | Zetacorp
      21 | Oest  | Larry Fitch   |   652.00 | Midwest Systems
      21 | Oest  | Larry Fitch   |   776.00 | Midwest Systems
      22 | Oest  | Nancy Angelli |  2430.00 | Peter Brothers
      22 | Oest  | Nancy Angelli | 31350.00 | Chen Associates
      22 | Oest  | Nancy Angelli |   652.00 | Peter Brothers
      21 | Oest  | Sue Smith     |  2130.00 | Fred Lewis Corp.
      21 | Oest  | Sue Smith     |  1896.00 | Fred Lewis Corp.
      21 | Oest  | Sue Smith     |  3750.00 | Rico Enterprises
      21 | Oest  | Sue Smith     | 15000.00 | Orion Corp
(28 rows)

18. Mostrar la quantitat de comandes que cada representant ha fet per client i limport total de comandes que cada venedor ha venut a cada client.  Mostrar la ciutat de loficina del representant de ventes, el nom del representant,  el nom del client, la quantitat de comandes representant-client i  el total de limport de les seves comandes per client.  Ordenar resultats per ciutat, representant i client.

SELECT rep,clie,COUNT(num_comanda),SUM(import),ciutat,nom,empresa 
FROM comandes 
JOIN rep_vendes  
ON rep = num_empl
JOIN clients 
ON clie = num_clie
JOIN oficines 
ON oficina_rep = oficina
GROUP BY rep,clie,ciutat,nom,empresa 
ORDER BY ciutat,rep,clie;

 rep | clie | count |   sum    |   ciutat    |      nom      |     empresa      
-----+------+-------+----------+-------------+---------------+------------------
 105 | 2103 |     4 | 35582.00 | Atlanta     | Bill Adams    | Acme Mfg.
 105 | 2111 |     1 |  3745.00 | Atlanta     | Bill Adams    | JCP Inc.
 101 | 2102 |     1 |  3978.00 | Chicago     | Dan Roberts   | First Corp.
 101 | 2108 |     1 |   150.00 | Chicago     | Dan Roberts   | Holm & Landis
 101 | 2113 |     1 | 22500.00 | Chicago     | Dan Roberts   | Ian & Schmidt
 103 | 2111 |     2 |  2700.00 | Chicago     | Paul Cruz     | JCP Inc.
 107 | 2109 |     1 | 31350.00 | Denver      | Nancy Angelli | Chen Associates
 107 | 2124 |     2 |  3082.00 | Denver      | Nancy Angelli | Peter Brothers
 102 | 2106 |     2 |  4026.00 | Los Angeles | Sue Smith     | Fred Lewis Corp.
 102 | 2114 |     1 | 15000.00 | Los Angeles | Sue Smith     | Orion Corp
 102 | 2120 |     1 |  3750.00 | Los Angeles | Sue Smith     | Rico Enterprises
 108 | 2112 |     2 | 47925.00 | Los Angeles | Larry Fitch   | Zetacorp
 108 | 2114 |     1 |  7100.00 | Los Angeles | Larry Fitch   | Orion Corp
 108 | 2118 |     4 |  3608.00 | Los Angeles | Larry Fitch   | Midwest Systems
 106 | 2101 |     1 |  1458.00 | New York    | Sam Clark     | Jones Mfg.
 106 | 2117 |     1 | 31500.00 | New York    | Sam Clark     | J.P. Sinclair
 109 | 2108 |     2 |  7105.00 | New York    | Mary Jones    | Holm & Landis
(17 rows)

19. Mostrar la quantitat de representants de ventes que hi ha a la regió Est, la quantitat de  representants de ventes que hi ha a la regió Oest i la quantitat de representants que no estan assignats a cap regió.  Només es vol comptar als representants que no tenen ciutat assignada o que tenen un 'N' minúscula o majúscula al nom de la seva ciutat  i tenen una 'm' o una 'n'  minúscula o majúscula al seu nom.

SELECT COUNT(num_empl),regio 
FROM rep_vendes 
LEFT JOIN oficines 
ON oficina_rep = oficina
WHERE ciutat ILIKE '%n%' AND
 (nom ILIKE '%n%' OR nom ILIKE '%m%') OR 
 regio IS NULL 
 GROUP BY regio;
 
 count | regio 
-------+-------
     1 | 
     3 | Est
     2 | Oest
(3 rows)
20. Mostrar la quantitat de representants de ventes que hi ha a les diferents ciutats-oficines i la quantitat de clients que tenen associats als representants daquestes ciutats-oficines. Mostrar la regió, la ciutat, el número de representants i el número de clients.
    
SELECT regio, ciutat, rep_clie, COUNT(num_clie)
FROM clients 
JOIN rep_vendes
ON rep_clie = num_empl
JOIN oficines 
ON oficina_rep = oficina
GROUP BY regio, ciutat, rep_clie;

 regio |   ciutat    | rep_clie | count 
-------+-------------+----------+-------
 Est   | Chicago     |      101 |     3
 Est   | New York    |      109 |     2
 Oest  | Los Angeles |      102 |     4
 Est   | Atlanta     |      105 |     2
 Est   | New York    |      106 |     2
 Est   | Chicago     |      103 |     3
 Oest  | Los Angeles |      108 |     2
 Est   | Chicago     |      104 |     1
 Oest  | Denver      |      107 |     1
(9 rows)


21.  Mostrar els noms de les empreses que han comprat productes de les fàbriques imm i rei amb un preu de catàleg (no de compra) inferior a 80 o superior a 1000. Mostrar l'empresa' el fabricant, el codi de producte, el preu, el nom del representant de ventes que ha fet la comanda i el nom del seu director.

SELECT empresa, id_fabricant, id_producte, preu, venedor.nom, director.cap, director.nom 
FROM productes JOIN comandes ON id_producte = producte AND id_fabricant = fabricant
JOIN clients ON clie = num_clie 
JOIN rep_vendes AS venedor ON rep_clie = venedor.num_empl 
JOIN rep_vendes AS director ON director.cap = venedor.num_empl
WHERE (id_fabricant = 'imm' OR id_fabricant = 'rei') AND (preu < 80 OR preu > 1000);


  empresa    | id_fabricant | id_producte |  preu   |  venedors   | cap |      nom      
---------------+--------------+-------------+---------+-------------+-----+---------------
 Ian & Schmidt | rei          | 2a44r       | 4500.00 | Bob Smith   | 104 | Bill Adams
 J.P. Sinclair | rei          | 2a44l       | 4500.00 | Sam Clark   | 106 | Mary Jones
 Zetacorp      | rei          | 2a44r       | 4500.00 | Larry Fitch | 108 | Sue Smith
 J.P. Sinclair | rei          | 2a44l       | 4500.00 | Sam Clark   | 106 | Bob Smith
 Ian & Schmidt | rei          | 2a44r       | 4500.00 | Bob Smith   | 104 | Dan Roberts
 J.P. Sinclair | rei          | 2a44l       | 4500.00 | Sam Clark   | 106 | Larry Fitch
 Ian & Schmidt | rei          | 2a44r       | 4500.00 | Bob Smith   | 104 | Paul Cruz
 Zetacorp      | rei          | 2a44r       | 4500.00 | Larry Fitch | 108 | Nancy Angelli
(8 rows)

    
22. Mostrar els venedors amb un nom que no comenci ni per I ni per J que entre totes les seves ventes ens han comprat per un total superior a 25.000.

SELECT nom, SUM(import) 
FROM rep_vendes JOIN comandes ON num_empl = rep 
WHERE NOT (nom LIKE 'I%' OR nom LIKE 'J%')  
GROUP BY nom
HAVING SUM(import) > 25000;

      nom      |   sum    
---------------+----------
 Larry Fitch   | 58633.00
 Nancy Angelli | 34432.00
 Bill Adams    | 39327.00
 Dan Roberts   | 26628.00
 Sam Clark     | 32958.00
(5 rows)


23. Mostrar les comandes que els clients han fet a representants de ventes que no són el que tenen assignat. Mostrar limport de la comanda, el nom del client, el nom del representant de ventes que ha fet la comanda, la seva oficina si en té, el nom del representant de ventes que el client té assignat i la seva oficina si en té.

SELECT import, empresa, rep_comanda.nom as "pilla comanda", rep_comanda.oficina_rep, rep_vendes.nom as "dirigente", rep_vendes.oficina_rep   
FROM comandes JOIN clients ON clie = num_clie 
JOIN rep_vendes as rep_comanda ON rep_comanda.num_empl = rep 
JOIN rep_vendes ON rep_vendes.num_empl = rep_clie
WHERE rep <> rep_clie
ORDER BY 1,2,3;
  import  |     empresa     | pilla comanda | oficina_rep | dirigente  | oficina_rep 
----------+-----------------+---------------+-------------+------------+-------------
   150.00 | Holm & Landis   | Dan Roberts   |          12 | Mary Jones |          11
  3745.00 | JCP Inc.        | Bill Adams    |          13 | Paul Cruz  |          12
  7100.00 | Orion Corp      | Larry Fitch   |          21 | Sue Smith  |          21
 22500.00 | Ian & Schmidt   | Dan Roberts   |          12 | Bob Smith  |          12
 31350.00 | Chen Associates | Nancy Angelli |          22 | Paul Cruz  |          12
(5 rows)

24. Mostrar tots els productes del catàleg de productes de lempresa, amb tots els seus camps, ordenats de menor a major preu, el total d'unitats que se n'han venut i quants representants diferents els han venut.

SELECT productes.*, SUM(quantitat), COUNT(DISTINCT rep)  
FROM productes LEFT JOIN comandes ON id_fabricant=fabricant AND id_producte = producte 
GROUP BY id_fabricant, id_producte
ORDER BY preu;

 id_fabricant | id_producte |     descripcio     |  preu   | estoc | sum | count 
--------------+-------------+--------------------+---------+-------+-----+-------
 aci          | 41001       | Article Tipus 1    |   55.00 |   277 |     |     0
 aci          | 41002       | Article Tipus 2    |   76.00 |   167 |  64 |     2
 aci          | 41003       | Article Tipus 3    |  107.00 |   207 |  35 |     1
 aci          | 41004       | Article Tipus 4    |  117.00 |   139 |  68 |     2
 aci          | 4100x       | Peu de rei         |   25.00 |    37 |  30 |     2
 aci          | 4100y       | Extractor          | 2750.00 |    25 |  11 |     1
 aci          | 4100z       | Muntador           | 2500.00 |    28 |  15 |     2
 bic          | 41003       | Manovella          |  652.00 |     3 |   2 |     2
 bic          | 41089       | Retn               |  225.00 |    78 |     |     0
 bic          | 41672       | Plate              |  180.00 |     0 |     |     0
 fea          | 112         | Coberta            |  148.00 |   115 |  10 |     1
 fea          | 114         | Bancada Motor      |  243.00 |    15 |  16 |     2
 imm          | 773c        | Riosta 1/2-Tm      |  975.00 |    28 |   3 |     1
 imm          | 775c        | Riosta 1-Tm        | 1425.00 |     5 |  22 |     1
 imm          | 779c        | Riosta 2-Tm        | 1875.00 |     9 |   5 |     2
 imm          | 887h        | Suport Riosta      |   54.00 |   223 |     |     0
 imm          | 887p        | Pern Riosta        |  250.00 |    24 |     |     0
 imm          | 887x        | Retenidor Riosta   |  475.00 |    32 |     |     0
 qsa          | xk47        | Reductor           |  355.00 |    38 |  28 |     2
 qsa          | xk48        | Reductor           |  134.00 |   203 |     |     0
 qsa          | xk48a       | Reductor           |  117.00 |    37 |     |     0
 rei          | 2a44g       | Passador Frontissa |  350.00 |    14 |   6 |     1
 rei          | 2a44l       | Frontissa Esq.     | 4500.00 |    12 |   7 |     1
 rei          | 2a44r       | Frontissa Dta.     | 4500.00 |    12 |  15 |     2
 rei          | 2a45c       | V Stago Trinquet   |   79.00 |   210 |  32 |     2
(25 rows)


25. Per cada client mostrar el nom del representant de ventes que té assignat, el nom del seu director si en té, el nom del cap de l'oficina on treballa si té oficina i el nom del cap de l'oficina on treballa el seu director si té director i els eu director té oficina.

SELECT num_clie, empresa, rep_client.nom, rep_client_director.nom, oficina_cap.nom, oficina_director_cap.nom
FROM clients LEFT JOIN rep_vendes as rep_client ON rep_client.num_empl = rep_clie
LEFT JOIN rep_vendes as rep_client_director ON rep_client_director.num_empl = rep_client.cap
LEFT JOIN oficines as oficina1 ON rep_client.oficina_rep = oficina1.oficina 
LEFT JOIN rep_vendes as oficina_cap ON oficina1.director = oficina_cap.num_empl
LEFT JOIN oficines as oficina2 ON rep_client_director.oficina_rep = oficina2.oficina 
LEFT JOIN rep_vendes as oficina_director_cap ON oficina2.director = oficina_director_cap.num_empl;

 num_clie |      empresa      |      nom      |     nom     |     nom     |     nom     
----------+-------------------+---------------+-------------+-------------+-------------
     2111 | JCP Inc.          | Paul Cruz     | Bob Smith   | Bob Smith   | Bob Smith
     2102 | First Corp.       | Dan Roberts   | Bob Smith   | Bob Smith   | Bob Smith
     2103 | Acme Mfg.         | Bill Adams    | Bob Smith   | Bill Adams  | Bob Smith
     2123 | Carter & Sons     | Sue Smith     | Larry Fitch | Larry Fitch | Larry Fitch
     2107 | Ace International | Tom Snyder    | Dan Roberts |             | Bob Smith
     2115 | Smithson Corp.    | Dan Roberts   | Bob Smith   | Bob Smith   | Bob Smith
     2101 | Jones Mfg.        | Sam Clark     |             | Sam Clark   | 
     2112 | Zetacorp          | Larry Fitch   | Sam Clark   | Larry Fitch | Sam Clark
     2121 | QMA Assoc.        | Paul Cruz     | Bob Smith   | Bob Smith   | Bob Smith
     2114 | Orion Corp        | Sue Smith     | Larry Fitch | Larry Fitch | Larry Fitch
     2124 | Peter Brothers    | Nancy Angelli | Larry Fitch | Larry Fitch | Larry Fitch
     2108 | Holm & Landis     | Mary Jones    | Sam Clark   | Sam Clark   | Sam Clark
     2117 | J.P. Sinclair     | Sam Clark     |             | Sam Clark   | 
     2122 | Three-Way Lines   | Bill Adams    | Bob Smith   | Bill Adams  | Bob Smith
     2120 | Rico Enterprises  | Sue Smith     | Larry Fitch | Larry Fitch | Larry Fitch
     2106 | Fred Lewis Corp.  | Sue Smith     | Larry Fitch | Larry Fitch | Larry Fitch
     2119 | Solomon Inc.      | Mary Jones    | Sam Clark   | Sam Clark   | Sam Clark
     2118 | Midwest Systems   | Larry Fitch   | Sam Clark   | Larry Fitch | Sam Clark
     2113 | Ian & Schmidt     | Bob Smith     | Sam Clark   | Bob Smith   | Sam Clark
     2109 | Chen Associates   | Paul Cruz     | Bob Smith   | Bob Smith   | Bob Smith
     2105 | AAA Investments   | Dan Roberts   | Bob Smith   | Bob Smith   | Bob Smith
(21 rows)



## EXERCICI Practica ---------------------------------------------------------------
5 TAULES 


SELECT num_comanda, clie, empresa, venedor_comanda.num_empl, rep_assignat.num_empl, descripcio, Oficina1.ciutat, oficines2.ciutat 
FROM comandes JOIN clients ON clie = num_clie 
JOIN rep_vendes AS venedor_comanda ON rep = venedor_comanda.num_empl 
JOIN productes ON fabricant = id_fabricant AND producte = id_producte 
JOIN oficines AS Oficina1 ON venedor_comanda.oficina_rep = oficina
JOIN rep_vendes AS rep_assignat ON rep_clie = rep_assignat.num_empl
JOIN oficines AS oficines2 ON rep_assignat.oficina_rep = oficines2.oficina
WHERE (oficina1.ciutat = 'New York' AND venedor_comanda.edat > 40) OR
 (oficina1.ciutat = 'Chicago' AND venedor_comanda.edat < 60) OR 
 (venedor_comanda.oficina_rep IS NULL AND venedor_comanda.edat > 30) AND
  fabricant = 'aci' ;

 num_comanda | clie |    empresa    | num_empl | num_empl |     descripcio     |  ciutat  |  ciutat  
-------------+------+---------------+----------+----------+--------------------+----------+----------
      112961 | 2117 | J.P. Sinclair |      106 |      106 | Frontissa Esq.     | New York | New York
      112989 | 2101 | Jones Mfg.    |      106 |      106 | Bancada Motor      | New York | New York
      112968 | 2102 | First Corp.   |      101 |      101 | Article Tipus 4    | Chicago  | Chicago
      112975 | 2111 | JCP Inc.      |      103 |      103 | Passador Frontissa | Chicago  | Chicago
      113055 | 2108 | Holm & Landis |      101 |      109 | Peu de rei         | Chicago  | New York
      113057 | 2111 | JCP Inc.      |      103 |      103 | Peu de rei         | Chicago  | Chicago
      113042 | 2113 | Ian & Schmidt |      101 |      104 | Frontissa Dta.     | Chicago  | Chicago
(7 rows)
















