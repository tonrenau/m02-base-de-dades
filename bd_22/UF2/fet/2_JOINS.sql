# Consultes multitaula



## Exercici 1

Llista la ciutat de les oficines, i el nom i títol dels directors de cada oficina.

SELECT ciutat, oficina, oficina_rep, nom, cap FROM rep_vendes JOIN oficines ON oficina = oficina_rep; 

   ciutat    | oficina | oficina_rep |      nom      | cap 
-------------+---------+-------------+---------------+-----
 Atlanta     |      13 |          13 | Bill Adams    | 104
 New York    |      11 |          11 | Mary Jones    | 106
 Los Angeles |      21 |          21 | Sue Smith     | 108
 New York    |      11 |          11 | Sam Clark     |    
 Chicago     |      12 |          12 | Bob Smith     | 106
 Chicago     |      12 |          12 | Dan Roberts   | 104
 Los Angeles |      21 |          21 | Larry Fitch   | 106
 Chicago     |      12 |          12 | Paul Cruz     | 104
 Denver      |      22 |          22 | Nancy Angelli | 108
(9 rows)


## Exercici 2

Llista totes les comandes mostrant el seu número, import, número de client i límit de crèdit.

SELECT num_comanda, import, num_clie, limit_credit FROM clients JOIN comandes ON comandes.clie = clients.num_clie;


 num_comanda |  import  | num_clie | limit_credit 
-------------+----------+----------+--------------
      112961 | 31500.00 |     2117 |     35000.00
      113012 |  3745.00 |     2111 |     50000.00
      112989 |  1458.00 |     2101 |     65000.00
      113051 |  1420.00 |     2118 |     60000.00
      112968 |  3978.00 |     2102 |     65000.00
      110036 | 22500.00 |     2107 |     35000.00
      113045 | 45000.00 |     2112 |     50000.00
      112963 |  3276.00 |     2103 |     50000.00
      113013 |   652.00 |     2118 |     60000.00
      113058 |  1480.00 |     2108 |     55000.00
      112997 |   652.00 |     2124 |     40000.00
      112983 |   702.00 |     2103 |     50000.00
      113024 |  7100.00 |     2114 |     20000.00
      113062 |  2430.00 |     2124 |     40000.00
      112979 | 15000.00 |     2114 |     20000.00
      113027 |  4104.00 |     2103 |     50000.00
      113007 |  2925.00 |     2112 |     50000.00
      113069 | 31350.00 |     2109 |     25000.00
      113034 |   632.00 |     2107 |     35000.00
      112992 |   760.00 |     2118 |     60000.00
      112975 |  2100.00 |     2111 |     50000.00
      113055 |   150.00 |     2108 |     55000.00
      113048 |  3750.00 |     2120 |     50000.00
      112993 |  1896.00 |     2106 |     65000.00
      113065 |  2130.00 |     2106 |     65000.00
      113003 |  5625.00 |     2108 |     55000.00
      113049 |   776.00 |     2118 |     60000.00
      112987 | 27500.00 |     2103 |     50000.00
      113057 |   600.00 |     2111 |     50000.00
      113042 | 22500.00 |     2113 |     20000.00
(30 rows)



## Exercici 3

Llista el número de totes les comandes amb la descripció del producte demanat.

SELECT num_comanda, descripcio, producte, fabricant FROM comandes JOIN productes on id_producte = producte AND fabricant = id_fabricant;
 
 num_comanda |     descripcio     | producte 
-------------+--------------------+----------
      112961 | Frontissa Esq.     | 2a44l
      113012 | Manovella          | 41003
      113012 | Article Tipus 3    | 41003
      112989 | Bancada Motor      | 114  
      112968 | Article Tipus 4    | 41004
      110036 | Muntador           | 4100z
      113045 | Frontissa Dta.     | 2a44r
      112963 | Article Tipus 4    | 41004
      113013 | Manovella          | 41003
      113013 | Article Tipus 3    | 41003
      113058 | Coberta            | 112  
      112997 | Manovella          | 41003
      112997 | Article Tipus 3    | 41003
      112983 | Article Tipus 4    | 41004
      113024 | Reductor           | xk47 
      113062 | Bancada Motor      | 114  
      112979 | Muntador           | 4100z
      113027 | Article Tipus 2    | 41002
      113007 | Riosta 1/2-Tm      | 773c 
      113069 | Riosta 1-Tm        | 775c 
      113034 | V Stago Trinquet   | 2a45c
      112992 | Article Tipus 2    | 41002
      112975 | Passador Frontissa | 2a44g
      113055 | Peu de rei         | 4100x
      113048 | Riosta 2-Tm        | 779c 
      112993 | V Stago Trinquet   | 2a45c
      113065 | Reductor           | xk47 
      113003 | Riosta 2-Tm        | 779c 
      113049 | Reductor           | xk47 
      112987 | Extractor          | 4100y
      113057 | Peu de rei         | 4100x
      113042 | Frontissa Dta.     | 2a44r
(32 rows)


## Exercici 4

Llista el nom de tots els clients amb el nom del representant de vendes assignat.

SELECT num_clie, empresa, num_empl, nom FROM clients JOIN rep_vendes On rep_vendes.num_empl = clients.rep_clie ;
 
 num_clie |      empresa      | num_empl |      nom      
----------+-------------------+----------+---------------
     2111 | JCP Inc.          |      103 | Paul Cruz
     2102 | First Corp.       |      101 | Dan Roberts
     2103 | Acme Mfg.         |      105 | Bill Adams
     2123 | Carter & Sons     |      102 | Sue Smith
     2107 | Ace International |      110 | Tom Snyder
     2115 | Smithson Corp.    |      101 | Dan Roberts
     2101 | Jones Mfg.        |      106 | Sam Clark
     2112 | Zetacorp          |      108 | Larry Fitch
     2121 | QMA Assoc.        |      103 | Paul Cruz
     2114 | Orion Corp        |      102 | Sue Smith
     2124 | Peter Brothers    |      107 | Nancy Angelli
     2108 | Holm & Landis     |      109 | Mary Jones
     2117 | J.P. Sinclair     |      106 | Sam Clark
     2122 | Three-Way Lines   |      105 | Bill Adams
     2120 | Rico Enterprises  |      102 | Sue Smith
     2106 | Fred Lewis Corp.  |      102 | Sue Smith
     2119 | Solomon Inc.      |      109 | Mary Jones
     2118 | Midwest Systems   |      108 | Larry Fitch
     2113 | Ian & Schmidt     |      104 | Bob Smith
     2109 | Chen Associates   |      103 | Paul Cruz
     2105 | AAA Investments   |      101 | Dan Roberts
(21 rows)



## Exercici 5

Llista la data de totes les comandes amb el numero i nom del client de la comanda.

SELECT num_clie, empresa  FROM clients JOIN comandes ON clients.num_clie = comandes.clie;
 
 
 num_clie |      empresa      
----------+-------------------
     2117 | J.P. Sinclair
     2111 | JCP Inc.
     2101 | Jones Mfg.
     2118 | Midwest Systems
     2102 | First Corp.
     2107 | Ace International
     2112 | Zetacorp
     2103 | Acme Mfg.
     2118 | Midwest Systems
     2108 | Holm & Landis
     2124 | Peter Brothers
     2103 | Acme Mfg.
     2114 | Orion Corp
     2124 | Peter Brothers
     2114 | Orion Corp
     2103 | Acme Mfg.
     2112 | Zetacorp
     2109 | Chen Associates
     2107 | Ace International
     2118 | Midwest Systems
     2111 | JCP Inc.
     2108 | Holm & Landis
     2120 | Rico Enterprises
     2106 | Fred Lewis Corp.
     2106 | Fred Lewis Corp.
     2108 | Holm & Landis
     2118 | Midwest Systems
     2103 | Acme Mfg.
     2111 | JCP Inc.
     2113 | Ian & Schmidt
(30 rows)


## Exercici 6

Llista les oficines, noms i títols del seus directors amb un objectiu superior a 600.000.

SELECT oficina, nom, carrec FROM oficines JOIN rep_vendes ON oficines.director = rep_vendes.num_empl WHERE oficines.objectiu > 600000;
 
 oficina |     nom     |   carrec   
---------+-------------+------------
      12 | Bob Smith   | Dir Vendes
      21 | Larry Fitch | Dir Vendes
(2 rows)


## Exercici 7

Llista els venedors de les oficines de la regió est.

SELECT oficina, regio, num_empl, nom 
FROM rep_vendes 
JOIN oficines 
ON rep_vendes.oficina_rep = oficines.oficina 
WHERE oficines.regio = 'Est';
 oficina | regio | num_empl |     nom     
---------+-------+----------+-------------
      13 | Est   |      105 | Bill Adams
      11 | Est   |      109 | Mary Jones
      11 | Est   |      106 | Sam Clark
      12 | Est   |      104 | Bob Smith
      12 | Est   |      101 | Dan Roberts
      12 | Est   |      103 | Paul Cruz
(6 rows)


## Exercici 8

Llista les comandes superiors a 25000, incloent el nom del venedor que va servir la comanda i el nom del client que el va sol·licitar.

SELECT fabricant, import, nom, empresa 
FROM comandes 
JOIN clients 
ON clients.num_clie = comandes.clie  
JOIN rep_vendes 
ON comandes.rep = rep_vendes.num_empl 
WHERE comandes.import >= 25000;


 fabricant |  import  |      nom      |     empresa     
-----------+----------+---------------+-----------------
 rei       | 31500.00 | Sam Clark     | J.P. Sinclair
 rei       | 45000.00 | Larry Fitch   | Zetacorp
 imm       | 31350.00 | Nancy Angelli | Chen Associates
 aci       | 27500.00 | Bill Adams    | Acme Mfg.
(4 rows)


## Exercici 9

Llista les comandes superiors a 25000, mostrant el client que va servir la comanda i el nom del venedor que té assignat el client.

SELECT num_comanda, import, num_clie, rep_clie, empresa 
FROM comandes 
JOIN clients 
ON comandes.clie = clients.num_clie 
JOIN rep_vendes 
ON clients.rep_clie = rep_vendes.num_empl 
WHERE import > 25000;
 
 SELECT num_comanda, import, num_clie, rep_clie, nom FROM comandes, clients, rep_vendes WHERE comandes.clie = clients.num_clie AND clients.rep_clie = rep_vendes.num_empl AND import > 25000;
 
 num_comanda |  import  | num_clie | rep_clie |     nom     
-------------+----------+----------+----------+-------------
      112961 | 31500.00 |     2117 |      106 | Sam Clark
      113045 | 45000.00 |     2112 |      108 | Larry Fitch
      113069 | 31350.00 |     2109 |      103 | Paul Cruz
      112987 | 27500.00 |     2103 |      105 | Bill Adams
(4 rows)


## Exercici 10

Llista les comandes superiors a 25000, mostrant el nom del client que el va ordenar, el venedor associat al client, i loficina on el venedor treballa.

SELECT num_comanda, import, num_clie, empresa, rep_clie, oficina_rep  
FROM comandes JOIN clients 
ON comandes.clie = clients.num_clie 
JOIN rep_vendes 
ON clients.rep_clie = rep_vendes.num_empl 
JOIN oficines 
ON oficines.oficina = rep_vendes.oficina_rep 
WHERE import > 25000;

num_comanda |  import  | num_clie |     empresa     | rep_clie | oficina_rep 
-------------+----------+----------+-----------------+----------+-------------
      112961 | 31500.00 |     2117 | J.P. Sinclair   |      106 |          11
      113045 | 45000.00 |     2112 | Zetacorp        |      108 |          21
      113069 | 31350.00 |     2109 | Chen Associates |      103 |          12
      112987 | 27500.00 |     2103 | Acme Mfg.       |      105 |          13
(4 rows)


## Exercici 11

Llista totes les combinacions de venedors i oficines on la quota del venedor és superior a l'objectiu de l'oficina.

SELECT nom, oficina, quota, objectiu
 FROM rep_vendes JOIN oficines 
 ON rep_vendes.oficina_rep = oficines.oficina 
 WHERE rep_vendes.quota > oficines.objectiu;

 nom | oficina | quota | objectiu 
-----+---------+-------+----------
(0 rows)



## Exercici 12

Informa sobre tots els venedors i les oficines en les que treballen.

SELECT num_empl, nom, oficina 
FROM rep_vendes JOIN oficines 
ON rep_vendes.oficina_rep = oficines.oficina;

 num_empl |      nom      | oficina
----------+---------------+----------
      105 | Bill Adams    |      13
      109 | Mary Jones    |      11
      102 | Sue Smith     |      21
      106 | Sam Clark     |      11
      104 | Bob Smith     |      12
      101 | Dan Roberts   |      12
      108 | Larry Fitch   |      21
      103 | Paul Cruz     |      12
      107 | Nancy Angelli |      22
(9 rows)

## Exercici 13 !!!

Llista els venedors amb una quota superior a la dels seus directors.

SELECT rep_vendes.nom, rep_vendes.cap
FROM rep_vendes 
JOIN rep_vendes cap 
ON cap.num_empl = rep_vendes.cap
WHERE rep_vendes.quota > cap.quota;
     nom     | cap 
-------------+-----
 Bill Adams  | 104
 Mary Jones  | 106
 Dan Roberts | 104
 Larry Fitch | 106
 Paul Cruz   | 104
(5 rows)



## Exercici 14

Llistar el nom de lempresa i totes les comandes fetes pel client 2103.

SELECT empresa, num_comanda, clie
FROM clients JOIN comandes 
ON clients.num_clie = comandes.clie 
WHERE clie = 2103;
  empresa  | num_comanda | clie 
-----------+-------------+------
 Acme Mfg. |      112963 | 2103
 Acme Mfg. |      112983 | 2103
 Acme Mfg. |      113027 | 2103
 Acme Mfg. |      112987 | 2103
(4 rows)


## Exercici 15

Llista aquelles comandes que el seu import sigui superior a 10000, mostrant el numero de comanda, els imports i les descripcions del producte.

SELECT descripcio, num_comanda, import 
FROM productes 
JOIN comandes 
ON productes.id_producte = comandes.producte 
AND  productes.id_fabricant = comandes.fabricant
WHERE import > 10000 ;
   descripcio   | num_comanda |  import  
----------------+-------------+----------
 Frontissa Esq. |      112961 | 31500.00
 Muntador       |      110036 | 22500.00
 Frontissa Dta. |      113045 | 45000.00
 Muntador       |      112979 | 15000.00
 Riosta 1-Tm    |      113069 | 31350.00
 Extractor      |      112987 | 27500.00
 Frontissa Dta. |      113042 | 22500.00
(7 rows)


## Exercici 16 (MAL)

Llista les comandes superiors a 25000, mostrant el nom del client que la va demanar, el venedor associat al client, i loficina on el venedor treballa. També cal mostar la descripció del producte

SELECT empresa, nom, oficina_rep, descripcio, import
FROM clients JOIN rep_vendes 
On clients.rep_clie = rep_vendes.num_empl 
JOIN comandes 
ON comandes.rep = rep_vendes.num_empl 
JOIN oficines 
ON rep_vendes.oficina_REP = rep_vendes.num.empl
JOIN productes 
ON productes.id_producte = comandes.producte 
AND  productes.id_fabricant = comandes.fabricant
WHERE import > 25000;
     empresa     |      nom      | oficina_rep |   descripcio   |  import  
-----------------+---------------+-------------+----------------+----------
 Acme Mfg.       | Bill Adams    |          13 | Extractor      | 27500.00
 Jones Mfg.      | Sam Clark     |          11 | Frontissa Esq. | 31500.00
 Zetacorp        | Larry Fitch   |          21 | Frontissa Dta. | 45000.00
 Peter Brothers  | Nancy Angelli |          22 | Riosta 1-Tm    | 31350.00
 J.P. Sinclair   | Sam Clark     |          11 | Frontissa Esq. | 31500.00
 Three-Way Lines | Bill Adams    |          13 | Extractor      | 27500.00
 Midwest Systems | Larry Fitch   |          21 | Frontissa Dta. | 45000.00
(7 rows)

	

## Exercici 17

Trobar totes les comandes rebudes en els dies en que un nou venedor va ser contractat. Per cada comanda mostrar un cop el número, import i data de la comanda.

SELECT num_comanda, data_contracte, import  
FROM rep_vendes JOIN comandes
ON rep_vendes.data_contracte = comandes.data;

 num_comanda | data_contracte |  import  
-------------+----------------+----------
      112968 | 1989-10-12     |  3978.00
      112968 | 1989-10-12     |  3978.00
      112979 | 1989-10-12     | 15000.00
      112979 | 1989-10-12     | 15000.00
(4 rows)


## Exercici 18

Mostra el nom, les vendes dels treballadors que tenen assignada una oficina, amb la ciutat i l'objectiu de l'oficina de cada venedor.

SELECT nom, oficina_rep, ciutat, objectiu
FROM rep_vendes JOIN oficines 
ON rep_vendes.oficina_rep = oficines.oficina;
      nom      | oficina_rep |   ciutat    | objectiu  
---------------+-------------+-------------+-----------
 Bill Adams    |          13 | Atlanta     | 350000.00
 Mary Jones    |          11 | New York    | 575000.00
 Sue Smith     |          21 | Los Angeles | 725000.00
 Sam Clark     |          11 | New York    | 575000.00
 Bob Smith     |          12 | Chicago     | 800000.00
 Dan Roberts   |          12 | Chicago     | 800000.00
 Larry Fitch   |          21 | Los Angeles | 725000.00
 Paul Cruz     |          12 | Chicago     | 800000.00
 Nancy Angelli |          22 | Denver      | 300000.00
(9 rows)


## Exercici 19

Llista el nom de tots els venedors i el del seu director en cas de tenir-ne. El camp que conté el nom del treballador s'ha d'identificar amb "empleado" i el camp que conté el nom del director amb "director".

SELECT nom AS "empleat" , cap AS "director"  
FROM rep_vendes;
    empleat    | director 
---------------+----------
 Bill Adams    |      104
 Mary Jones    |      106
 Sue Smith     |      108
 Sam Clark     |         
 Bob Smith     |      106
 Dan Roberts   |      104
 Tom Snyder    |      101
 Larry Fitch   |      106
 Paul Cruz     |      104
 Nancy Angelli |      108
(10 rows)

## Exercici 20

Llista totes les combinacions possibles de venedors i ciutats.

SELECT num_empl, nom, ciutat
FROM rep_vendes JOIN oficines
ON rep_vendes.num_empl = oficines.director ;
 num_empl |     nom     |   ciutat    
----------+-------------+-------------
      108 | Larry Fitch | Denver
      106 | Sam Clark   | New York
      104 | Bob Smith   | Chicago
      105 | Bill Adams  | Atlanta
      108 | Larry Fitch | Los Angeles
(5 rows)


## Exercici 21 !!!

Per a cada venedor, mostrar el nom, les vendes i la ciutat de l'oficina en cas de tenir-ne una d'assignada.

SELECT nom, rep_vendes.vendes, ciutat 
FROM rep_vendes 
JOIN oficines 
ON oficina_rep = oficina;
      nom      |  vendes   |   ciutat    
---------------+-----------+-------------
 Bill Adams    | 367911.00 | Atlanta
 Mary Jones    | 392725.00 | New York
 Sue Smith     | 474050.00 | Los Angeles
 Sam Clark     | 299912.00 | New York
 Bob Smith     | 142594.00 | Chicago
 Dan Roberts   | 305673.00 | Chicago
 Larry Fitch   | 361865.00 | Los Angeles
 Paul Cruz     | 286775.00 | Chicago
 Nancy Angelli | 186042.00 | Denver
(9 rows)

## Exercici 22

Mostra les comandes de productes que tenen unes existències inferiors a 10. Llistar el numero de comanda, la data de la comanda, el nom del client que ha fet la comanda, identificador del fabricant i lidentificador de producte de la comanda.

SELECT num_comanda, data, empresa, fabricant, producte, quantitat 
FROM comandes JOIN clients 
ON comandes.clie = clients.num_clie 
WHERE comandes.quantitat < 10;
 num_comanda |    data    |      empresa      | fabricant | producte | quantitat 
-------------+------------+-------------------+-----------+----------+-----------
      112961 | 1989-12-17 | J.P. Sinclair     | rei       | 2a44l    |         7
      112989 | 1990-01-03 | Jones Mfg.        | fea       | 114      |         6
      113051 | 1990-02-10 | Midwest Systems   | qsa       | k47      |         4
      110036 | 1990-01-30 | Ace International | aci       | 4100z    |         9
      113013 | 1990-01-14 | Midwest Systems   | bic       | 41003    |         1
      112997 | 1990-01-08 | Peter Brothers    | bic       | 41003    |         1
      112983 | 1989-12-27 | Acme Mfg.         | aci       | 41004    |         6
      112979 | 1989-10-12 | Orion Corp        | aci       | 4100z    |         6
      113007 | 1990-01-08 | Zetacorp          | imm       | 773c     |         3
      113034 | 1990-01-29 | Ace International | rei       | 2a45c    |         8
      112975 | 1989-12-12 | JCP Inc.          | rei       | 2a44g    |         6
      113055 | 1990-02-15 | Holm & Landis     | aci       | 4100x    |         6
      113048 | 1990-02-10 | Rico Enterprises  | imm       | 779c     |         2
      113065 | 1990-02-27 | Fred Lewis Corp.  | qsa       | xk47     |         6
      113003 | 1990-01-25 | Holm & Landis     | imm       | 779c     |         3
      113049 | 1990-02-10 | Midwest Systems   | qsa       | xk47     |         2
      113042 | 1990-02-02 | Ian & Schmidt     | rei       | 2a44r    |         5
(17 rows)


## Exercici 23 !!!

Llista les 5 comandes amb un import superior. Mostrar l''identificador de la comanda, import de la comanda, preu del producte, 
nom del client, nom del representant de vendes que va efectuar la comanda i ciutat de l''oficina, en cas de tenir oficina assignada.

select num_comanda, import, preu, empresa, nom, ciutat 
from comandes
join productes on fabricant = id_fabricant and producte = id_producte
join clients on clie = num_clie
join rep_vendes on rep_clie = num_empl 
join oficines on oficina_rep = oficina 
order by import DESC ; 
limit 5; 

 num_comanda |  import  |  preu   |     empresa     |     nom     |   ciutat    
-------------+----------+---------+-----------------+-------------+-------------
      113045 | 45000.00 | 4500.00 | Zetacorp        | Larry Fitch | Los Angeles
      112961 | 31500.00 | 4500.00 | J.P. Sinclair   | Sam Clark   | New York
      113069 | 31350.00 | 1425.00 | Chen Associates | Paul Cruz   | Chicago
      112987 | 27500.00 | 2750.00 | Acme Mfg.       | Bill Adams  | Atlanta
      113042 | 22500.00 | 4500.00 | Ian & Schmidt   | Bob Smith   | Chicago
(5 rows)



## Exercici 24 !!!

Llista les comandes que han estat preses per un representant de vendes que no és l''actual representant de vendes del client pel que s''ha realitzat la comanda. 
Mostrar el número de comanda, el nom del client, el nom de lactual representant de vendes del client com a "rep_cliente" i el nom del representant de vendes 
que va realitzar la comanda com a "rep_pedido".


SELECT num_comanda, empresa, rep_clie.nom AS rep_cliente, rep_vendes.nom AS rep_pedido
FROM comandes JOIN clients 
ON clie = num_clie AND rep <> rep_clie
JOIN rep_vendes ON rep = num_empl 
JOIN rep_vendes rep_clie ON rep_clie = rep_clie.num_empl
WHERE clients.rep_clie != comandes.rep;

 num_comanda |     empresa     | rep_cliente |  rep_pedido   
-------------+-----------------+-------------+---------------
      113012 | JCP Inc.        | Paul Cruz   | Bill Adams
      113024 | Orion Corp      | Sue Smith   | Larry Fitch
      113069 | Chen Associates | Paul Cruz   | Nancy Angelli
      113055 | Holm & Landis   | Mary Jones  | Dan Roberts
      113042 | Ian & Schmidt   | Bob Smith   | Dan Roberts


## Exercici 25

Llista les comandes amb un import superior a 5000 i també aquelles comandes realitzades per un client amb un crèdit inferior a 30000. Mostrar lidentificador de la comanda, el nom del client i el nom del representant de vendes que va prendre la comanda.

SELECT num_comanda, empresa, nom, import, limit_credit
FROM comandes JOIN clients 
ON comandes.clie = clients.num_clie
JOIN rep_vendes 
ON clients.rep_clie = rep_vendes.num_empl 
WHERE comandes.import > 5000 AND clients.limit_credit < 30000;
 num_comanda |     empresa     |    nom    |  import  | limit_credit 
-------------+-----------------+-----------+----------+--------------
      113024 | Orion Corp      | Sue Smith |  7100.00 |     20000.00
      112979 | Orion Corp      | Sue Smith | 15000.00 |     20000.00
      113069 | Chen Associates | Paul Cruz | 31350.00 |     25000.00
      113042 | Ian & Schmidt   | Bob Smith | 22500.00 |     20000.00
(4 rows)

