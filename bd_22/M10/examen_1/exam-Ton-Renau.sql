
#4 








#3
create or replace function info_client(emp text) returns text as $$
	declare 
		resp text := '';
		id_clie int := 0;
		res_select record;
	begin
		select * from clients into res_select where empresa = emp;
		id_clie := res_select.num_clie;
		if id_clie > 0 then 
			resp := 'El client ' || emp || ' te com a identificador. ' || id_clie || E'\n' ;

			select count(*) as num_comd, sum(import) as import, 
			count(distinct producte || fabricant) as num_prod 
			from comandes into res_select where clie = id_clie;

			if res_select.num_comd > 0 then 
				resp := resp || 'Ha fet ' || res_select.num_comd || ' comandes, per un valor de ' || res_select.import 
				|| ' euros, i ens ha comprat ' || res_select.num_prod || ' productes diferents. ' || E'\n';
			else 
				resp := resp || ' No ha fet cap comanda. ' || E'\n';
			end if;
		else
			resp := 'No existeix cpa empresa amb aquest nom.';
		end if;
		return resp;
	end;
$$ language plpgsql;


#2 
create or replace function info_any_mes(anyo int) returns table 
	(mes numeric,clie int, reps int, coamdes int, import numeric) as $$
	select date_part('month', data),count(distinct clie),
			count(distinct rep), count(num_comanda), 
			coalesce(sum(import),0)
	from comandes
	where anyo = date_part('year', data )
	group by date_part('month', data);
$$ language sql;



# 1 
create function info_any(anyo int) returns table 
	(clie int, reps int, coamdes int, import numeric) as $$
	select count(distinct clie), count(distinct rep), 
			count(num_comanda), coalesce(sum(import),0)
	from comandes
	where anyo = date_part('year', data )
$$ language sql;