1)

CREATE or REPLACE FUNCTION info_any(int) RETURNS table 
	(num_clients integer, num_reps integer, 
	num_productes integer, total_import numeric) AS $$
	 SELECT COUNT(DISTINCT clie), COUNT(DISTINCT rep), 
	 	COUNT(*), COALESCE(SUM(import),0)
	   FROM comandes 
	  WHERE DATE_PART('year', data) = $1;
$$ LANGUAGE SQL;

select info_any(1989);
select info_any(1990);
select info_any(1991);


2) 
CREATE or REPLACE FUNCTION info_any_mes(int) RETURNS table 
(mes smallint, num_clients integer, num_reps integer, num_productes integer, total_import numeric) AS $$
	 SELECT  DATE_PART('month', data), COUNT(DISTINCT clie), COUNT(DISTINCT rep), COUNT(*), 
	 COALESCE(SUM(import),0)
	   FROM comandes 
	  WHERE DATE_PART('year', data) = $1
	  GROUP BY DATE_PART('month', data);
$$ LANGUAGE SQL;

select info_any_mes(1989);
select info_any_mes(1990);
select info_any_mes(1991);





3) CREATE OR REPLACE FUNCTION info_client(nom_empresa text) RETURNS text AS $$
DECLARE
result text := '';
id_client integer :=0;
res_select record;
BEGIN
	/* Busquem les dades del client */
    SELECT * FROM clients into res_select WHERE empresa=$1;
    id_client := res_select.num_clie;
    IF id_client > 0 THEN /* millor fer servir IF id_client IS NULL */
	   result := 'El client ' || nom_empresa || ' te com a identificador el num. : ' || id_client || '.' || E'\n';
	   
	   /* Busquem les dades de les comandes del client*/
	   SELECT COUNT(*) as num_comandes_client, SUM(import) as total_import, count(DISTINCT fabricant || producte) num_productes_diferents
		FROM comandes into res_select WHERE clie=id_client;
       IF res_select.num_comandes_client > 0 then
	      result := result || 'Ha fet ' || res_select.num_comandes_client || ' comandes, per un valor de ' || res_select.total_import
	       || '€, i ens ha comprat ' || res_select.num_productes_diferents || ' productes diferents.' || E'\n';
	       
	       /* Busquem quins productes ha comprat */
	       /* necessitem un FOR per processar tots els resultats del SELECT
	       SELECT * FROM productes INTO res_select WHERE id_fabricant || id_producte IN (SELECT fabricant || producte FROM comandes WHERE clie=id_client);
	       */
	   ELSE
	      result := result || 'No ha fet cap comanda' || E'\n';
	   END IF;	
    ELSE
       result := 'No existeix cap empresa amb aquest nom.'|| E'\n';
    END IF;
    
    
    RETURN result;
END;
$$ LANGUAGE 'plpgsql';

select info_client('Corp');
               info_client               
-----------------------------------------
 No existeix cap empresa amb aquest nom.+

select info_client('JCP Inc.');
                                     info_client                                      
--------------------------------------------------------------------------------------
 El client JCP Inc. te com a identificador el num. : 2111.                           +
 Ha fet 3 comandes, per un valor de 6445.00€, i ens ha comprat 3 productes diferents.+


4. Fer el mateix però amb LIKE (abans comptar quantes empreses acompleixen el LIKE, només continuar quan el LIKE ens torna una empresa)
CREATE OR REPLACE FUNCTION info_client_cadena(nom_empresa text) RETURNS text AS $$
DECLARE
result text := '';
id_client integer :=0;
res_select record;
sql text;
BEGIN
	/* Busquem les dades del client */
    SELECT COUNT(*) as num FROM clients into res_select WHERE empresa LIKE '%' || $1 || '%';
    IF res_select.num > 1 THEN
	   result := 'Hi ha més d''un client amb aquesta cadena.';
	ELSE
		IF res_select.num = 0 	THEN
		    result := 'No hi ha cap client amb aquesta cadena.';
		ELSE 
		    /* Busquem les dades del client */
		    SELECT * FROM clients into res_select WHERE empresa=(SELECT empresa FROM clients WHERE empresa LIKE '%' || $1 || '%');
			id_client := res_select.num_clie;
			IF id_client > 0 THEN /* millor fer servir IF id_client IS NULL */
			   result := 'El client ' || res_select.empresa || ' té com a identificador el num. : ' || id_client || '.' || E'\n';
			   
			   /* Busquem les dades de les comandes del client*/
			   SELECT COUNT(*) as num_comandes_client, SUM(import) as total_import, count(DISTINCT fabricant || producte) num_productes_diferents
	 			FROM comandes into res_select WHERE clie=id_client;
 			   IF res_select.num_comandes_client > 0 then
				  result := result || 'Ha fet ' || res_select.num_comandes_client || ' comandes, per un valor de ' || res_select.total_import
				   || '€, i ens ha comprat ' || res_select.num_productes_diferents || ' productes diferents.' || E'\n';
				   
				   /* Busquem quins productes ha comprat */
				   /* necessitem un FOR per processar tots els resultats del SELECT
				   SELECT * FROM productes INTO res_select WHERE id_fabricant || id_producte IN (SELECT fabricant || producte FROM comandes WHERE clie=id_client);
				   */
			   ELSE
				  result := result || 'No ha fet cap comanda' || E'\n';
			   END IF;	
			ELSE
			   result := 'No existeix cap empresa amb aquest nom.'|| E'\n';
			END IF;
		END IF;
    END IF;
    
    RETURN result;
END;
$$ LANGUAGE 'plpgsql';

select info_client_cadena('rr');
           info_client_cadena            
-----------------------------------------
 No hi ha cap client amb aquesta cadena.
 
select info_client_cadena('Corp');
            info_client_cadena             
-------------------------------------------
 Hi ha més d'un client amb aquesta cadena.
(1 row)

' 
select info_client_cadena('AAA');
                        info_client_cadena                        
------------------------------------------------------------------
 El client AAA Investments te com a identificador el num. : 2105.+
 No ha fet cap comanda                                           +


select info_client_cadena('Mid');
        info_client_cadena                                  
--------------------------------------------------------------------------------------
 El client Midwest Systems té com a identificador el num. : 2118.                    +
 Ha fet 4 comandes, per un valor de 3608.00€, i ens ha comprat 4 productes diferents.+

