## Exercici 1

Mostreu la longitud de la cadena "hola que tal"
SELECT length("hola que tal");

 length 
--------
     12
(1 row)

## Exercici 2

Mostreu la longitud dels valors del camp "id_producte".
SELECT length(id_producte) 
FROM productes;

 length 
--------
      5
      5
      4
      5
      4
      5
      5
      5
      4
      4
      5
      3
      4
      5
      5
      4
      5
      5
      5
      5
      4
      5
      3
      4
      5
(25 rows)

## Exercici 3

Mostrar la longitud dels valors del camp "descripcio".
SELECT length(descripcio)
FROM productes;

 length 
--------
     16
      9
      8
      5
     11
     15
     15
      9
     11
      8
     14
      7
     13
      4
     15
     11
      8
      8
     15
     14
     13
     10
     13
     16
     18
(25 rows)

## Exercici 4

Mostreu els noms dels venedors en majúscules.

SELECT upper(nom) 
FROM rep_vendes;

     upper     
---------------
 BILL ADAMS
 MARY JONES
 SUE SMITH
 SAM CLARK
 BOB SMITH
 DAN ROBERTS
 TOM SNYDER
 LARRY FITCH
 PAUL CRUZ
 NANCY ANGELLI
(10 rows)
## Exercici 5

Mostreu els noms dels venedors en minúscules.

SELECT lower(nom) 
FROM rep_vendes;

     lower     
---------------
 bill adams
 mary jones
 sue smith
 sam clark
 bob smith
 dan roberts
 tom snyder
 larry fitch
 paul cruz
 nancy angelli
(10 rows)

## Exercici 6

Trobeu on és la posició de l''espai en blanc de la cadena 'potser 7'.

SELECT position(' ' in 'potser 7');

 position 
----------
        7
(1 row)

## Exercici 7

Volem mostrar el nom, només el nom dels venedors sense el cognom, en
majúscules.

SELECT upper(split_part(nom, ' ', 1)) as nom
FROM rep_vendes;

  nom 
--------
 BILL
 MARY
 SUE
 SAM
 BOB
 DAN
 TOM
 LARRY
 PAUL
 NANCY
 PANCHO
(11 rows)


## Exercici 8

Crear una vista que mostri l''identificador dels representants de vendes i en
columnes separades el nom i el cognom.

CREATE VIEW id_repr
    AS SELECT num_empl, split_part(nom, ' ', 1) AS nom ,split_part(nom, ' ',2) AS cognom
    FROM rep_vendes;

SELECT * FROM id_repr;

 num_empl |  nom  | cognom  
----------+-------+---------
      105 | Bill  | Adams
      109 | Mary  | Jones
      102 | Sue   | Smith
      106 | Sam   | Clark
      104 | Bob   | Smith
      101 | Dan   | Roberts
      110 | Tom   | Snyder
      108 | Larry | Fitch
      103 | Paul  | Cruz
      107 | Nancy | Angelli
(10 rows)

## Exercici 9 !!!

Mostreu els valors del camp nombre de manera que 'Bill Adams' sorti com 'B.
Adams'.
SELECT concat(LEFT(nom,1),'.',SUBSTRING(nom from ' .*$')) FROM rep_vendes;

## Exercici 10!!!

Mostreu els valors del camp nombre de manera que 'Bill Adams' sorti com 'Adams,
Bill'.

SELECT concat(SUBSTRING(nom from ' .*$'),' ', SUBSTRING(nom from '^.* ')) FROM rep_vendes;

## Exercici 11

Volem mostrar el camp descripcion de la taula productos però que en comptes de
sortir espais en blanc, volem subratllats ('_').

SELECT replace(descripcio, ' ', '_')
FROM productes;

      replace       
--------------------
 V_Stago_Trinquet
 Extractor
 Reductor
 Plate
 Riosta_2-Tm
 Article_Tipus_3
 Article_Tipus_4
 Manovella
 Pern_Riosta
 Reductor
 Frontissa_Esq.
 Coberta
 Suport_Riosta
 Retn
 Article_Tipus_1
 Riosta_1-Tm
 Muntador
 Reductor
 Article_Tipus_2
 Frontissa_Dta.
 Riosta_1/2-Tm
 Peu_de_rei
 Bancada_Motor
 Retenidor_Riosta
 Passador_Frontissa
(25 rows)

## Exercici 12

Volem treure per pantalla una columna, que conté el nom i les vendes, amb els
següent estil:

```
   vendes dels empleats
 ---------------------------
  Bill Adams..... 367911,00
  Mary Jones..... 392725,00
  Sue Smith...... 474050,00
  Sam Clark...... 299912,00
  Bob Smith...... 142594,00
  Dan Roberts.... 305673,00
  Tom Snyder.....  75985,00
  Larry Fitch.... 361865,00
  Paul Cruz...... 286775,00
  Nancy Angelli.. 186042,00
 (10 rows)
```
SELECT nom || substring ("..................", 
 

## Exercici 13

Treieu per pantalla el temps total que fa que estan contractats els
treballadors, ordenat pels cognoms dels treballadors amb un estil semblant al
següent:

```
        nom    |     temps_treballant
    -----------+-------------------------
    Mary Jones | 13 years 4 months 6 days
```

SELECT nom, age(data_contracte, ::timestamp)
FROM rep_vendes;

      nom      |     temps_treballat      
---------------+--------------------------
 Bill Adams    | 35 years 1 mon 21 days
 Mary Jones    | 33 years 5 mons 23 days
 Sue Smith     | 36 years 3 mons 25 days
 Sam Clark     | 34 years 9 mons 20 days
 Bob Smith     | 35 years 10 mons 16 days
 Dan Roberts   | 36 years 5 mons 15 days
 Tom Snyder    | 33 years 2 mons 22 days
 Larry Fitch   | 33 years 5 mons 23 days
 Paul Cruz     | 36 years 1 mon 3 days
 Nancy Angelli | 34 years 4 mons 20 days
(10 rows)


## Exercici 14

Cal fer un llistat dels productes dels quals l'estoc és inferior al
total d'unitats venudes d'aquell producte els darrers 60 dies, a comptar des de
la data actual. Cal mostrar els codis de fabricant i de producte, l'estoc i les
unitats totals venudes dels darrers 60 dies.



## Exercici 15

Com l'exercici anterior però en comptes de 60 dies ara es volen aquells
productes venuts durant el mes actual o durant l'anterior.

## Exercici 16

Per fer un estudi previ de la campanya de Nadal es vol un llistat on, per cada
any del qual hi hagi comandes a la base de dades, es mostri el nombre de
clients diferents que hagin fet comandes en el mes de desembre d'aquell any.
Cal mostrar l'any i el número de clients, ordenat ascendent per anys.


## Exercici 17

Llisteu codi(s) i descripció dels productes. La descripció ha d'aparèixer en
majúscules. Ha d'estar ordenat per la longitud de les descripcions (les més
curtes primer).

## Exercici 18

LListar el nom dels treballadors i la data del seu contracte mostrant-la amb el següent format:

*Dia_de_la_setmana dia_mes_numeric, mes_text del any_4digits*

Per exemple:

```
...
Bill Adams    | Friday    12, February del 1988
...
```

## Exercici 19

Modificar l''import de les comandes que s''han realitzat durant la tardor
augmentant-lo un 20% i arrodonint a l''alça el resultat.

## Exercici 20

Mostar les dades de les oficines llistant en primera instància aquelles
oficines que tenen una desviació entre vendes i objectius més gran.

## Exercici 21

Llistar les dades d''aquells representants de vendes que tenen un identificador
senar i són directors d''algun representant de vendes.
