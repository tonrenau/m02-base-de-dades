/*
Funció descompte()

Passat un import (euros), retorna l'import amb el descompte aplicat.

segons import:
1-30  euros -> 10%
>30 fins 60 -> 20%
>60 fins 100 -> 30%
> 100 euros -> 40%
*/

CREATE OR REPLACE FUNCTION descompte(euros integer) RETURNS integer AS $$
DECLARE
    euros integer := $1;
    valor integer;
BEGIN
    IF euros >= 1 AND euros <= 30 THEN 
        valor := euros - (euros*0.1);
    ELSIF euros > 30 AND euros <= 60 THEN
        valor := euros - (euros*0.2);
    ELSIF euros > 60 AND euros <= 100 THEN
        valor := euros - (euros*0.2);
    ELSIF euros > 100 THEN
        valor := euros - (euros*0.3);
    END IF;

    RETURN valor;
END;
$$ LANGUAGE 'plpgsql';


/*
Funció aplica_descompte()
Paràmetres d'entrada: fabcod, prodcod
Retorna void.

Fent servir la funció descompte(), aplica el descompte al preu d'un producte
*/

CREATE OR REPLACE FUNCTION aplica_descompte(p_fabcod text, p_prodcod text) RETURNS void AS $$
DECLARE
    preu_producte integer;
BEGIN
    SELECT precio INTO preu_producte FROM producto WHERE fabcod = p_fabcod and prodcod = p_prodcod;
    UPDATE producto SET precio = descompte(precio) WHERE fabcod = p_fabcod AND prodcod = p_prodcod;

END;
$$ LANGUAGE 'plpgsql';



/*
Crear la funció inserta_repventa( nom, edat, ofi, lloc)
retorna: Booleà
Tasca: funció per crear representants de vendes amb els paràmetres rebuts
Si el nom o l'edat són null mostra un missatge i retorna FALSE
li assignarem la mateixa quota que tingui el repventa més jove
haurem de calcular el codi del representant, sumant 1 a l'últim de la taula
si l'oficina és null l'assignem a l'oficina amb menys gent

*/
CREATE OR REPLACE FUNCTION inserta_repventa(nom text, edat integer, ofi integer, lloc text) RETURNS boolean AS $$
DECLARE
    cuota_nou numeric;
    repcod_nou int;
    ofinum_nou int;
BEGIN
    IF nom is null or edat is null THEN
        RAISE NOTICE 'Cal introduïr un nom i una edat';
        RETURN FALSE;
    END IF;
    SELECT cuota INTO cuota_nou FROM repventa ORDER BY edat ASC LIMIT 1;

    SELECT max(repcod)+1 INTO repcod_nou FROM repventa;

    IF ofi IS NULL THEN
        SELECT ofinum INTO ofinum_nou FROM repventa where ofinum IS NOT NULL GROUP BY ofinum ORDER BY count(*) ASC LIMIT 1; 
    END IF;
    
    INSERT INTO repventa values (repcod_nou, nom, edat, ofi, lloc, '1988-02-12', null, cuota_nou, 1);
    RETURN TRUE;
END;
$$ LANGUAGE 'plpgsql';


SELECT inserta_repventa('Anna',34,11,'Jefa');



-- 4.- Crear una funció factura(cliecod) que donat un identificador de client mostri per pantalla la seva factura. 
-- La factura ha de contenir tots els productes que ha demanat el client amb la data en que els ha demanat. 
-- En cas de tenir descompte calcular el descompte aplicat a cada comanda. (veure exercici següent descuento_aplicable())
-- Calcular el l'IVA i el total. Crear les funcions necessàries per crear un codi modular. 
-- La factura ha de tenir el següent aspecte:


CREATE OR REPLACE FUNCTION linea(n int, c char) RETURNS text AS $$
DECLARE 
s text := '';
BEGIN 
    FOR i IN 1..n LOOP
        s := s || c;
    END LOOP;
    s := s || E'\n';
    RETURNS s;
END;
$$ LANGUAGE 'plpgsql';


CREATE OR REPLACE FUNCTION factura(fac_cliecod numeric) RETURNS text AS $$
DECLARE
factura text := ''; 
nombre_client text;
calc text := '';
base_iva numeric;
iva_text text;
total numeric;
BEGIN
    -- Obtener el nombre del cliente
    SELECT nombre into nombre_client FROM cliente WHERE cliecod = fac_cliecod;

    factura := ('Client (id):' || nombre_client || '(' || fac_cliecod || E')\n');
    
    -- Llamamos a la función para que nos cree las líneas con el caracter =
    factura := factura || linea(length(factura)-1, '=');

    -- Añadimos la fecha de hoy
    factura := factura || 'Data : ' || to_char(current_date, 'DD/MM/YYYY') || E'\n\n';
    
    -- Cabecera de la factura 
    factura := factura || E 'Descripció del producte (id)  Data de la comanda  Quantitat  Preu Unitari  Descompte         Import\n';
    calc := 'Descripció del producte (id)  Data de la comanda  Quantitat  Preu Unitari  Descompte         Import';

    -- Llamamos a la función para que nos cree las líneas con el catacter -
    factura := factura || linea(length(calc)-1, '-');

    -- Recorremos los datos necesarios para nuestra factura

    FOR comanda IN SELECT descrip, pr.fabcod, pr.prodcod, fecha, cant, precio, importe
                FROM pedido pe 
                JOIN producto pr ON pe.prodcod = pr.prodcod and pe.prodcod = pe.prodcod
                WHERE cliecod = fac_cliecod
    LOOP
        -- Posem la descripció del producte en una variable per després 
        -- poder fer bé la alineació i que aquesta variable ocupi 30 caràcters
        desc_producte := comanda.descrip || '(' || comanda.fabcod || 
                         ', ' || comanda.prodcod || ')';
        -- No més posem el descompte si es fa descompte
        -- Si és 0 es deixa en blanc
        IF comanda.descuento <> 0 THEN
           descompte := comanda.descuento || '%';
        END IF;
        factura := factura || format(E'%-30s%18s%11s%14s%11s%15s\n',
                desc_producte, to_char(comanda.fecha, 'DD/MM/YYYY'), 
                comanda.cant, comanda.precio, descompte, comanda.importe);
        base := base + comanda.importe;
    END LOOP;
    factura := factura || linea(length(calc)-1, '=');
    -- Totals
    factura := factura || format(E'%80s:%18s\n', 'Base', base);
    base_iva := base * iva / 100;
    iva_text := 'IVA(' || iva || '%)';
    factura := factura || format(E'%80s:%18s\n', iva_text, round(base_iva,2));
    total := base + base_iva;
    factura := factura || format(E'%80s:%18s\n','Total', round(total,2));
    factura := factura || linea(length(calc)-1, '=');
    RETURN factura;
   END;
$$ LANGUAGE 'plpgsql';
/*

training=> select factura(2118::smallint);
                                               factura                                               
-----------------------------------------------------------------------------------------------------
 Client (id): Midwest Systems(2118)                                                                 +
 ==================================                                                                 +
 Data: 26/04/2021                                                                                   +
                                                                                                    +
 Descripció del producte (id)  Data de la comanda  Quantitat  Preu Unitari  Descompte         Import+
 ---------------------------------------------------------------------------------------------------+
 Reductor(qsa, xk47)                   10/02/1990          2        355.00                    776.00+
 Reductor(qsa, xk47)                   10/02/1990          4        355.00        10%        1420.00+
 Manivela(bic, 41003)                  14/01/1990          1        652.00        10%         652.00+
 Articulo Tipo 2(aci, 41002)           04/11/1989         10         76.00        10%         760.00+
 ===================================================================================================+
                                                                             Base:           3608.00+
                                                                         IVA(18%):            649.44+
                                                                            Total:           4257.44+
 ===================================================================================================+


*/

