# Funcions d''usuari

## Exercici 1

Donat l''identificador d''un client retorneu la importància del client, és a
dir, el percentatge dels imports de les comandes del client respecte al total
dels imports de les comandes.

CREATE or replace function importancia(clie integer) returns numeric as $$
	SELECT ROUND(sum(import) / (SELECT sum(import) from comandes)*100, 2)  
	from comandes 
	where clie = $1
	group by clie order by 1; 
$$ LANGUAGE SQL ;

select * from  importancia (2112);
 importancia 
-------------
       19.35


## Exercici 2

Calculeu el que s''ha deixat de cobrar per a un producte determinat.
És a dir, la diferència que hi ha entre el que val i el que li hem cobrat al total de clients.

create or replace function deixat_cobrat(fabri varchar(3), prod varchar(5)) returns numeric as $$
	select sum(preu*quantitat - import)
	from comandes join productes on (fabricant, producte) = (id_fabricant, id_producte)
	where fabri = $1 and prod = $2;
$$ language SQL;

select * from deixat_cobrat('rei', '41003');
 deixat_cobrat 
---------------
       2684.00
(1 row)


## Exercici 3

Creeu una funció que si li passem les columnes vendes i quota ens retorni una
columna amb el valor de vendes - quota.

create or replace function vendes_quota(ven numeric, quota numeric) returns numeric as $$
	select ven - quota 
	from rep_vendes
	where ven = $1 and quota = $2 ; 
$$ language SQL;


## Exercici 4

Feu una funció que donat un identificador de representant de vendes retorni
l''identificador dels clients que té assignats amb el seu límit de crèdit.

create or replace function clie_rep(id int) returns table(clie int, credit numeric(8,2)) as $$
	select num_clie, limit_credit
	from clients
	where rep_clie = $1;
$$ language sql;

 select * from clie_rep(103);
 clie |  credit  
------+----------
 2111 | 50000.00
 2121 | 45000.00
 2109 | 25000.00
(3 rows)

## Exercici 5

Crear una funció promig_anual(venedor, any) que retorni el promig d''imports de
comandes del venedor durant aquell any.

create or replace function promig_anual(ven int, anyo int) returns numeric as $$
	select round(avg(import)) 
	from comandes 
	where rep = ven and date_part('year', data) = anyo;
$$ language sql ;

select * from promig_anual(105, 1989);
 promig_anual 
--------------
        10493
(1 row)

## Exercici 6

Creeu una funció max_promig_anual(anyo) que retorni el màxim dels promitjos
anuals de tots els venedors. Useu la funció de l''exercici anterior.

create or replace function max_promig_anual(ven int, anyo int) returns numeric as $$
	select round(max(import)) 
	from comandes 
	where rep = ven and date_part('year', data) = anyo;
$$ language sql ;

select * from max_promig_anual(105, 1989);
 promig_anual 
--------------
        27500
        
## Exercici 7

Creeu una funció promig_anual_tots(anyo) que retorni el promig anual de cada
venedor durant l''any indicat. Useu funcions creades en els exercicis anteriors.

CREATE TYPE rep_promig AS (rep SMALLINT, promig NUMERIC);

CREATE FUNCTION promig_anual_tots(anyo int) RETURNS SETOF rep_promig AS $$ 
     SELECT DISTINCT rep, promig_anual(rep, $1) 
       FROM comandes; 
$$ LANGUAGE SQL;

## Exercici 8

Feu una funció que retorni tots els codis dels clients que no hagin comprat res durant el mes introduït.

create function clie_nocompra(mes int)returns setof smallint as $$
	SElect distinct num_clie 
	from clients 
	where num_clie IN (SELECT clie FROM comandes WHERE DATE_part('month',data) = $1);
$$ language sql;


--> una altre manera ara fent referencia a l'any:
create or replace function clie_compra(mes int)returns setof smallint as $$
	SElect distinct num_clie 
	from clients 
	where num_clie IN (SELECT clie FROM comandes WHERE DATE_part('year',data) = $1);
$$ language sql;


## Exercici 9

Funció anomenada maxim_mes a la que se li passa un any i retorna el mes en el
que hi ha hagut les màximes vendes (imports totals del mes).

create or replace function max_mes (anyo int) returns smallint as $$
	SELECT DATE_PART('month',data)
	from comandes  
	where date_part ('year', data) = anyo
	group by DATE_PART ('month',data)
	order by SUM(import) desc
	limit 1;
$$ language sql ;
	

## Exercici 10

1. Creeu una funció baixa_rep que doni de baixa el venedor que se li passa per
paràmetre i reassigni tots els seus clients al venedor que tingui menys clients
assignats (si hi ha empat, a qualsevol d''ells).

create or replace function baixa_rep(venedor int) returns int as $$
	select rep_clie 
	from clients 
	group by rep_clie 
	order by COUNT(num_clie)
	fetch first 1 rows only;
$$ language sql ;


CREATE FUNCTION baixa_rep(INTEGER) RETURNS void AS $$
     DELETE FROM rep_vendes
      WHERE num_empl = $1;

     UPDATE clients
        SET rep_clie = 
            (SELECT rep_clie
               FROM clients
              GROUP BY rep_clie
              ORDER BY COUNT(num_clie)
              FETCH FIRST 1 ROWS ONLY)
      WHERE rep_clie = $1;
$$ LANGUAGE SQL;

2. Com usarieu la funció per donar de baixa els 3 venedors que fa més temps que
no han fet cap venda?

SELECT num_empl
  FROM rep_vendes
       LEFT JOIN comandes
       ON num_empl = rep
 GROUP BY num_empl
 ORDER BY MAX(data)
 FETCH FIRST 3 ROWS ONLY;


## Exercici 11

Creeu una funció anomenada *n_clients* que donat un identificador d''un
representant de vendes ens retorni el nombre de clients que te assignats. Si
l'entrada és nul·la s'ha de retornar un valor nul. 

create or replace function n_clients (int) returns table(rep int, clie int) as $$
	select rep_clie,count(*)
	from clients 
	where rep_clie = $1
	group by 1;
$$ language sql returns null on null input;


select * from n_clients (106);
 rep | clie 
-----+------
 106 |    2

## Exercici 12

Creeu una funció anomenada *n_atesos* que donat un identificador d''un
representant de vendes ens retorni el nombre de clients diferents que ha atès.
Si l'entrada és nul·la s'ha de retornar un valor nul.

create or replace function n_atesos(int) returns table(rep int, clie int) as $$
	select rep, count(*)
	from comandes 
	where $1 = rep 
	group by 1;
$$ language sql returns null on null input; 

select * from n_atesos(103);
 rep | clie 
-----+------
 103 |    2
(1 row)

## Exercici 13

Creeu una funció anomenada *total_imports* que donat un identificador d''un
representant de vendes ens retorni la suma dels imports de les seves comandes.
Si l'entrada és nul·la s'ha de retornar un valor nul.

create or replace function total_imports(int) returns numeric as $$
	select SUM(import)
	from comandes 
	where rep = $1;
$$ language sql returns null on null input;

select * from total_imports(103);
 total_imports 
---------------
       2700.00
(1 row)

## Exercici 14

Creeu una funció anomenada "informe_rep" que ens retorni una taula amb
l''identificador del representant de vendes, el resultat de *n_clients*,
*n_atesos* i *total_imports*. Si a la funció se li passa un identificador de
representant de vendes només ha de retornar la informació relativa a aquest
representant de vendes. En cas de passar un valor nul a la funció, aquesta ha
de donar la informació de tots els representants de vendes.

create or replace function informe_rep(int) returns table (rep int, n_clie int, clie_atesos int, import int) as $$
	select num_empl, n_clients(num_empl), n_atesos(num_empl),total_imports(num_empl)
	from rep_vendes
	where case
		when $1 is not null then num_empl = $1
		else true end ;
$$ language sql;

SELECT informe_rep(101);
    informe_rep     
--------------------
 (101,3,3,26628.00)
(1 row)


## Exercici 15

Creeu una funció, i les funcions auxiliars convenients, que rebi l'identificador
d'un producte i retorni una taula amb les següents dades:

+ Identificador de producte i fabricant.
+ Nombre de representants de vendes que han venut aquest producte.
+ Nombre de clients que han comprat el producte.
+ Mitjana de l'import de les comandes d'aquest producte.
+ Quantitat mínima i quantitat màxima que s''ha demanat del producte en una sola comanda.

create function n_rep_han_venut (id_prod char(5),id_fab char(3)) returns int as $$
	select count(distinct rep)
	from comandes 
	where fabricant = id_fab and producte = id_prod;
$$ language sql; 

CREATE FUNCTION n_clie_han_comprat(char(3), char(5)) RETURNS int AS $$ 
     SELECT COUNT(DISTINCT clie) 
       FROM comandes 
      WHERE fabricant = $1 AND producte = $2; 
$$ LANGUAGE SQL;

CREATE FUNCTION mitjana_import(char(3), char(5)) RETURNS numeric AS $$ 
     SELECT AVG(import) 
       FROM comandes 
      WHERE fabricant = $1 AND producte = $2; 
$$ LANGUAGE SQL;

CREATE FUNCTION quant_min_max(char(3), char(5), OUT int, OUT int) AS $$ 
     SELECT MIN(quantitat), MAX(quantitat)    
       FROM comandes
      WHERE fabricant = $1 AND producte = $2;
$$ LANGUAGE SQL;

create or replace function info_producte(char(3), char(5)) returns table 
(id_prod char(5), id_fab char(3),rep int, clie int, mitjana numeric, min int, max int) as $$
	select $1, $2, n_rep_han_venut($1, $2), n_clie_han_comprat($1, $2), mitjana_import($1, $2), 
	;
$$ language sql; 
	

## Exercici 16

Creeu una funció que donada una oficina ens retorni una taula identificant els
productes i mostrant la quantitat d'aquest producte que s'ha venut a l''oficina.

create or replace function prod_ofi(int) returns table (id_prod char(5), quantitat int) as $$
	select producte, sum(quantitat)
	from comandes join oficines on director = rep 
	where $1 = oficina
	group by 1 ;
$$ language sql;
	
select * from prod_ofi (22);
 id_prod | quantitat 
---------+-----------
 2a44r   |        10
 41002   |        10
 41003   |         1
 773c    |         3
 k47     |         4
 xk47    |        22
(6 rows)

## Exercici 17

Creeu les funcions necessàries per aconseguir el següent resultat:

+ Cridant *resum_clie()* ha de retornar una taula amb els identificadors dels
clients, la suma de les seves compres, el nombre de comandes realitzades i el
nombre de representants de vendes que l''han atès.

create function resum_clie() returns table (id_clie int, suma_compres numeric, n_comandes int, n_rep_vendes int) as $$
	SELECT num_clie, sum(import), count(num_comanda), count(distinct rep) 
       FROM clients 
            LEFT JOIN comandes
            ON num_clie = clie 
      GROUP BY num_clie; 
$$ language sql; 

SELECT (resum_client()).*;

 id_clie | suma_compres | num_comandes | num_rep 
---------+--------------+--------------+---------
    2101 |      1458.00 |            1 |       1
    2102 |      3978.00 |            1 |       1
    2103 |     35582.00 |            4 |       1
    2105 |         NULL |            0 |       0
    2106 |      4026.00 |            2 |       1
    2107 |     23132.00 |            2 |       1
    2108 |      7255.00 |            3 |       2
    2109 |     31350.00 |            1 |       1
    2111 |      6445.00 |            3 |       2
    2112 |     47925.00 |            2 |       1
    2113 |     22500.00 |            1 |       1
    2114 |     22100.00 |            2 |       2
    2115 |         NULL |            0 |       0
    2117 |     31500.00 |            1 |       1
    2118 |      3608.00 |            4 |       1
    2119 |         NULL |            0 |       0
    2120 |      3750.00 |            1 |       1
    2121 |         NULL |            0 |       0
    2122 |         NULL |            0 |       0
    2123 |         NULL |            0 |       0
    2124 |      3082.00 |            2 |       1
(21 rows)

+ Cridant *resum_clie(num_clie)* ha de retornar una taula amb els
identificador dels representants de vendes i el nombre de comandes que ha
realitzat el client amb aquest representant de vendes

create or replace function resum_clie(num_clie int) returns table (rep int, num_comandes int)as $$
	select num_empl, (select count(num_comanda) from comandes where rep = num_empl and clie = num_clie )
	from rep_vendes;
$$ language sql;

select * from resum_clie(2111);
 rep | num_comandes 
-----+--------------
 105 |            1
 109 |            0
 102 |            0
 106 |            0
 104 |            0
 101 |            0
 110 |            0
 108 |            0
 103 |            2
 107 |            0
(10 rows)


	
+ Cridant *resum_client(num_clie, num_empl)* ha de retornar una taula amb el
nombre de productes diferents que ha demanat el client especificat al
representant de vendes especificat i la mitja de l''import de les comandes que
ha realitzat el client especificat al representant de vendes especificat.

create or replace function resum_clie(num_clie int, num_empl int) returns table (n_prod int, mitjana_import numeric)as $$
	select count(distinct (fabricant, producte)), round(avg(import),2)
	from comandes
	where clie = $1 and rep = $2;
$$ language sql ;

select * from resum_clie(2111, 103);
 n_prod |    mitjana_import     
--------+-------------------
      2 |          1350.00
(1 row)

## Exercici 18

Creeu la funció *millor_venedor()* que retorni totes les dades del venedor que ha
venut més (major total d''imports) durant l''any en curs.

create or replace function millor_venedor(int) returns rep_vendes as $$
	select *
	from rep_vendes 
	where num_empl = (select rep from comandes 
		where date_part('year', data) = $1 
		group by rep 
		order by sum(import) desc 
		fetch first 1 rows only);
$$ language sql;

select * from millor_venedor (1989);
 num_empl |    nom    | edat | oficina_rep |  carrec   | data_contracte | cap |   quota   |  vendes   
----------+-----------+------+-------------+-----------+----------------+-----+-----------+-----------
      106 | Sam Clark |   52 |          11 | VP Vendes | 1988-06-14     |     | 275000.00 | 299912.00
(1 row)

## Exercici 19

Calcular el descompte fet a un client concret, respecte totes les comandes del client.
Cal crear dos funcions auxiliars:

+ La primera obtindrà el total dels imports de les comandes d''un client determinat.

create or replace function total_imort_clie(clie int) returns numeric as $$
	select sum(import)
	from comandes
	where clie = $1
$$ language sql; 

+ La segona serà una funció preu de "comanda abstracta" que necessitarà el
producte en qüestió i la quantitat de productes demanats.

create or replace function import_comanda(fab char(3), prod char(5), quant int) returns numeric as $$
	select $3 * preu
	from productes 
	where id_fabricant = $1 and id_producte= $2 
$$ language sql; 
 
 select * from import_comanda ('rei', '2a44l', 7) ;
 import_comanda 
----------------
       31500.00
(1 row)


finalment :

create or replace function descomte_clie(num_clie int) returns numeric as $$
	select SUM(import_comanda(fabricant, producte, quantitat)) - total_imort_clie($1)
	from comandes
	where clie = $1
$$ language sql returns null on null input;


select * from descomte_clie (2103);
 descomte_clie 
---------------
       2750.00

select num_clie, descomte_clie(num_clie) from clients;
 num_clie | descomte_clie 
----------+---------------
     2111 |          0.00
     2102 |          0.00
     2103 |       2750.00
     2123 |              
     2107 |          0.00
     2115 |              
     2101 |          0.00
     2112 |          0.00
     2121 |              
     2114 |          0.00
     2124 |          0.00
     2108 |          0.00
     2117 |          0.00
     2122 |              
     2120 |          0.00
     2106 |          0.00
     2119 |              
     2118 |      -1486.00
     2113 |          0.00
     2109 |          0.00
     2105 |              
(21 rows)





