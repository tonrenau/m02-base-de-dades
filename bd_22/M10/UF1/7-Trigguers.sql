

-- Trigger i taula per auditar taula productes de TRAINING

CREATE TABLE PRODUCTES_AUDIT AS TABLE PRODUCTES;

ALTER TABLE productes_audit ADD accio char(1), ADD data_hora TIMESTAMP DEFAULT CURRENT_TIMESTAMP;

CREATE OR REPLACE FUNCTION productes_auditar() RETURNS trigger AS
$$
BEGIN
IF (TG_OP = 'DELETE') THEN
	INSERT INTO productes_audit SELECT OLD.*, 'D';
	RETURN OLD;
ELSIF (TG_OP = 'UPDATE') THEN
	INSERT INTO productes_audit SELECT OLD.*, 'U';
	RETURN NEW;
ELSIF (TG_OP = 'INSERT') THEN
	INSERT INTO productes_audit SELECT NEW.*, 'I';
	RETURN NEW;
END IF;
RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER productes_auditar BEFORE INSERT OR UPDATE OR DELETE ON productes
FOR EACH ROW EXECUTE PROCEDURE productes_auditar();

-- prova del trigger
insert into productes values ('bbb', 'aaaaa', 'descaaa', 140,3,null,457);
INSERT 0 1

select * from productes_audit ;
 id_fabricant | id_producte | descripcio |  preu  | estoc | data_vigencia | id_producte_historic | accio |         data_hora          
--------------+-------------+------------+--------+-------+---------------+----------------------+-------+----------------------------
              |             |            |        |       |               |                      | I     | 2023-05-04 15:51:40.41194
 bbb          | aaaaa       | descaaa    | 140.00 |     3 |               |                  457 | I     | 2023-05-04 15:52:36.792956
(2 rows)

UPDATE productes set id_producte='aaabb' where id_producte_historic=457;
UPDATE 1

select * from productes_audit ;
 id_fabricant | id_producte | descripcio |  preu  | estoc | data_vigencia | id_producte_historic | accio |         data_hora          
--------------+-------------+------------+--------+-------+---------------+----------------------+-------+----------------------------
              |             |            |        |       |               |                      | I     | 2023-05-04 15:51:40.41194
 bbb          | aaaaa       | descaaa    | 140.00 |     3 |               |                  457 | I     | 2023-05-04 15:52:36.792956
 bbb          | aaaaa       | descaaa    | 140.00 |     3 |               |                  457 | U     | 2023-05-04 15:53:28.313979
(3 rows)

delete from productes where id_producte_historic=457;
DELETE 1

select * from productes_audit ;
 id_fabricant | id_producte | descripcio |  preu  | estoc | data_vigencia | id_producte_historic | accio |         data_hora          
--------------+-------------+------------+--------+-------+---------------+----------------------+-------+----------------------------
              |             |            |        |       |               |                      | I     | 2023-05-04 15:51:40.41194
 bbb          | aaaaa       | descaaa    | 140.00 |     3 |               |                  457 | I     | 2023-05-04 15:52:36.792956
 bbb          | aaaaa       | descaaa    | 140.00 |     3 |               |                  457 | U     | 2023-05-04 15:53:28.313979
 bbb          | aaabb       | descaaa    | 140.00 |     3 |               |                  457 | D     | 2023-05-04 15:53:59.708566
(4 rows)

-----------------------------------------------------------------------------

--Exercici 1
--Fer un log/auditoria de totes les taules de training, guardant totes les operacions de totes les taules en una mateixa taula, i guardant tot el registre de cada taula coma a text.

select comandes.*::text from comandes;

CREATE TABLE rastre_training(
operacio char(1) NOT NULL,
taula text,
registre text,
data_hora timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
usuari text NOT NULL); 
