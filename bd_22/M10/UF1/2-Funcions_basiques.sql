Exercici 1 (reverse)
Donada una cadena, crea una altra cadena amb els caràcters de la cadena donada invertits.

CREATE or replace FUNCTION invertir_cadena(cadena text) returns text as $$
begin 
return reverse(cadena);
end;
$$ language plpgsql;

SELECT invertir_cadena('hola'); 
 invertir_cadena 
-----------------
 aloh
(1 row)

Exercici 2. (removeSpaces)
Donada una cadena, crea una altra cadena igual a la primera sense espais en blanc.

CREATE or replace FUNCTION invertir_cadena(cadena text) returns text as $$
begin 
return replace(cadena,' ','');
end;
$$ language plpgsql;

SELECT invertir_cadena('ei qe tal');
 invertir_cadena 
-----------------
 eiqetal
 
Exercici 3. (withoutString)
Donades dues cadenes, crea una nova cadena igual a la primera sense l''aparició de la segona. 
Si apareix més d''un cop, només es suprimirà la primera aparició.


Exercici 4. (toUpper)
Donada una cadena, crea una segona cadena amb els caràcters que estaven en minúscula passats a majúscula.

Exercici 5. (dni2nif)
Donada una cadena amb el número d''un DNI, genera una cadena amb el NIF.

El NIF s''obté a partir del DNI afegint-li la lletra que s''obté calculant el residu de la divisió entera del DNI entre 23.

Les lletres són: TRWAGMYFPDXBNJZSQVHLCKE i la transformació de codi és: 0 ->T, 1->R, 2->W, etc.

Exemple: Al DNI 37721039 li correspon el NIF 37721039G

CREATE FUNCTION dni_lletra(dni_rebut text) RETURNS text AS $$
SELECT dni_rebut || SUBSTR ('TRWAGMYFPDXBNJZSQVHLCKE', MOD (dni_rebut::INTEGER,23) +1,1 );
$$ LANGUAGE SQL;

select dni_lletra('47751439');




CREATE OR REPLACE FUNCTION dni_lletra1(dni_rebut text) RETURNS text AS $$
SELECT dni_rebut || SUBSTRING ('TRWAGMYFPDXBNJZSQVHLCKE' FROM (MOD (dni_rebut::INTEGER,23) +1) FOR 1);
$$ LANGUAGE SQL;

SELECT dni_lletra1('47751439');




CREATE OR REPLACE FUNCTION dni_lletra2(dni_rebut text) RETURNS text AS $$
DECLARE
lletra CHAR := '';
resultat TEXT := '';
modul INTEGER;
cadena_lletres_dni TEXT := 'TRWAGMYFPDXBNJZSQVHLCKE';

BEGIN
modul := MOD(dni_rebut :: INTEGER,23);
modul := modul +1; 
lletra := SUBSTRING(cadena_lletres_dni FROM modul FOR 1);
resultat := CONCAT (dni_rebut, lletra);
RETURN resultat;

END;
$$ LANGUAGE 'plpgsql';





CREATE OR REPLACE FUNCTION dni_lletra3(dni_rebut text) RETURNS text AS $$
DECLARE
lletra CHAR := '';
resultat TEXT := '';
modul INTEGER;
cadena_lletres_dni TEXT := 'TRWAGMYFPDXBNJZSQVHLCKE';

BEGIN
modul := MOD(dni_rebut :: INTEGER,23);
modul := modul +1; 
lletra := SUBSTR(cadena_lletres_dni, modul, 1);
resultat := dni_rebut || lletra;
RETURN resultat;

END;
$$ LANGUAGE 'plpgsql';


