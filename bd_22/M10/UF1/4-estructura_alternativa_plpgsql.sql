-- ~ Exercici 1
(sign) Determina si un nombre real és positiu, negatiu o zero. 
La funció retornarà les cadenes "positiu", "negatiu" o "zero", respectivament.

create or replace function signe_num (num int) returns varchar as $$
begin 
	if num > 0 then return 'positiu';
	elsif num < 0 then return 'negative';
	else return 'zero';
	end if;
	end;
$$ language plpgsql;

-- ~ Exercici 2
(triangle) A partir de les llargades de tres segments, esbrina si formen o no un triangle 
i si formen triangle de quin tipus.

Per tal de que tres segments formin un triangle, un segment qualsevol ha de ser 
més petit que la suma de la llargada dels altres dos.

Si els tres segments formen un triangle, aquest pot ser:

equilàter si els tres costats són iguals (del llatí: costats iguals)
isòsceles si només dos costats són iguals (del grec: dues cames iguals)
escalè si tots 3 costat són de diferent mida (del llatí que el va agafar del grec: desigual)
Heu de dissenyar una funció que retorni

"No és triangle"
"Triangle equilàter"
"Triangle isòsceles"
"Triangle escalè"

create function triangle (c1 int, c2 int, c3 int) returns varchar as $$
	begin 
		if c1 = c2 and c1 = c3 then return 'Triangle equilater';
		elsif c1 != c2 and c1 != c3 and c3 != c2 then return 'triangle escale';
		elsif (c1 = c2 and c1 != c3) or (c1 = c3 and c1 != c2) or (c2 = c3 and c2 != c1) then return 'triangle isoceles';
		else return 'no es triangle';
		end if;
		end; 
$$ language plpgsql; 

-- ~ Exercici 3
Donada una cadena amb un NIF, determina si és un NIF correcte.
S''ha de validar que la llargada de la cadena d''entrada sigui correcta (9 caràcters) 
i que la lletra sigui correcta.
El NIF s''obté a partir del DNI afegint-li la lletra que s''obté calculant el residu 
de la divisió entera del DNI entre 23.
Les lletres són: TRWAGMYFPDXBNJZSQVHLCKE i la transformació de codi és: 
0 -> T, 1-> R, 2 -> W, etc.
Exemple: Al DNI 37721039 li correspon el NIF 37721039G. 
Si la llargada és incorrecta retornarem un 1, si el problema és la lletra retornarem un 2 
i si tot és correcte retornarem un 0.

create or replace function dni_lletra(dni text) returns text as $$ 
	declare 
	modul int;
	lletres text := 'TRWAGMYFPDXBNJZSQVHLCKE';
	lletra char := '';
	resultat text := '';
	begin 
	modul := (mod(dni::int,23)+1);
	lletra := substring(lletres,modul,1);
	resultat := concat (dni,lletra);
	
	return resultat;
	end;
$$ language plpgsql;

create or replace function obtenir_num_dni (nif text) returns int as $$
	declare 
	numero int;
	begin 
	select left($1,8)into numero;
	return numero ;
	end;
$$ language plpgsql;

create or replace function lletra_dni(num_dni int) returns text as $$ 
	declare 
	dni text;
	begin
	select $1 || substring('TRWAGMYFPDXBNJZSQVHLCKE' from mod($1,23) + 1 for 1) into dni;
	return dni ;
	end ;
$$ language plpgsql;

create or replace function validar_dni (dni text) returns text as $$
	declare 
	num_dni int := obtenir_num_dni ($1);
	dni_nif text := lletra_dni($1);
	valor int := 0 ;
	begin 
	if length(dni_nif) != 9 then valor := 1;
	elsif $1 != dni_nif then valor := 2;
	end if ;
	return valor;
	end ;
$$language plpgsql;




-- ~ Exercici 4
Donada una comanda, esbrina si es va fer en un any de traspàs.
Si era any de traspàs retorna TRUE, sinó FALSE.
Són de traspàs els anys que són múltiples de 400 i els anys que són múltiples 
de 4 però no de 100.

create function any_comanda(int) returns int as $$
	select date_part('year', data) 
    from comandes 
    where $1 = num_comanda;
$$ language sql;

create function any_trespas(integer) returns text as $$
	declare 
    anyo int := any_comanda($1);
	trespas text := false;
	begin 
	if (anyo % 400 = 0) or (anyo % 4 = 0) and (anyo != 100) then trespas := true;
	end if;
	return trespas;
	end;
$$ language plpgsql;




-- ~ Exercici 5
Crea una funció que insereixi una oficina a la taula oficines. 
Per paràmetre arribaran totes les dades d''una oficina (oficina, ciutat, regio, director, objectiu, vendes). 
S''hauran de fer les següents comprovacions:
El codi d''oficina no existeix a la taula oficines.
La regió es 'Est' o 'Oest'.
El codi de director és valid.
L''objectiu és superior a 0.
Si totes les dades són vàlides, l''oficina s''inserirà i retornarem TRUE. Altrament no s''inserirà i 
retornarem FALSE.

create or replace function new_ofi
(ofi int, ciutat text, regio text, dir int, obj numeric, vendes numeric) returns boolean as $$
	declare 
		inserit boolean := False; 
	begin
		if ofi not in (select oficina from oficines) then 
			if regio = 'Est' or regio = 'Oest' then
				if dir IN (select num_empl from rep_vendes) then
					if obj > 0 then						
						insert into oficines values (ofi,ciutat,regio,dir,obj,vendes);
						inserit := True;
	end if;
	end if;
	end if; 
	end if;
	return inserit;
	end;
$$ language plpgsql;

IMPORTANT: feu totes les comprovacions en funcions separades per poder-les reutilitzar.
SELECT inserir_oficina(30, 'Barcelona', 'Est', 109, 200000, 300000);
 inserir_oficina 
-----------------
 t
(1 row)

SELECT inserir_oficina(30, 'Barcelona', 'North', 109, 200000, 300000);
 inserir_oficina 
-----------------
 f
(1 row)

-- ~ Exercici 6
Crea una funció que insereixi o modifiqui una oficina a la taula oficines. 
Per paràmetre arribaran totes les dades d''una oficina (oficina, ciutat, regio, director, objectiu, vendes). 
S''hauran de fer les següents comprovacions:

La region es 'Est' o 'Oest'.

El codi de director és vàlid.

L''objectiu és superior a 0.

Si totes les dades són vàlides i el codi d''oficina no existeix, l''oficina s''inserirà i retornarem un 1.

Si totes les dades són vàlides i el codi d''oficina existeix, l''oficina amb aquest codi es modificarà i 
retornarem un 2.

Altrament no es farà res i retornarem un -1.


create or replace function mod_ofi(ofi int, ciu text, reg text, dir int, obj numeric, ven numeric) returns boolean as $$
	declare
		validar boolean := False;
	begin 
		if reg = 'Est' or reg = 'Oest' then 
			if dir IN (select num_empl from rep_vendes) then
				if obj > 0 then 
					if ofi not in (select oficina from oficines) then 
						insert into oficines values(ofi,ciu,reg,dir,obj,ven);
						validar := True;
					if ofi in (select oficina from oficines) then 
						update oficines set ciutat=ciu, regio=reg, director=dir, objectiu=obj, vendes=ven where oficina = ofi;
						validar := True;
	end if; 		
	end if;	
	end if;
	end if;
	end if;
	return validar;
	end;
$$ language plpgsql; 

