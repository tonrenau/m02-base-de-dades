
1. Crea una funció que canvïi les dates de les comandes, canviant el 1989 per 2022 i el 1990 per 2023

CREATE OR REPLACE FUNCTION canvi_data_comandes()
RETURNS int AS
$$
DECLARE
 	result text := '';
 	searchsql text := '';
 	searchsql1 text := '';
 	res_select record;
 	num_canvis int :=0;
 	dia text;
 	mes text;
 	nova_data text;
BEGIN
 	searchsql := 'SELECT *, DATE_PART(''year'',data) as any_comanda FROM comandes WHERE data < ''2022-01-01'';';
    
	RAISE NOTICE '%',searchsql;
	
	FOR res_select IN EXECUTE(searchsql) LOOP
		 IF res_select.any_comanda = 1989 THEN
			
			dia = substr(res_select.data::text, 9,2);
			mes = DATE_PART('MONTH',res_select.data);
			nova_data := '2022-' || mes || '-' || dia;
 			searchsql := 'UPDATE comandes set data=''' || nova_data ||'''::date WHERE num_comanda=' || res_select.num_comanda ||';';
 			execute(searchsql);
 			num_canvis:=num_canvis +1;
		 ELSE
			IF res_select.any_comanda = 1990 THEN
			
				dia = substr(res_select.data::text, 9,2);
				mes = DATE_PART('MONTH',res_select.data);
				nova_data := '2023-' || mes || '-' || dia;
				searchsql := 'UPDATE comandes set data=''' || nova_data ||'''::date WHERE num_comanda=' || res_select.num_comanda ||';';
				execute(searchsql);	
				num_canvis:=num_canvis +1;
            END IF;
         END IF;
 	END LOOP;

   	RETURN num_canvis;
EXCEPTION 
	WHEN others THEN return 5;
END;
$$
LANGUAGE 'plpgsql';

2. Crea una taula buida amb la mateixa estructura que la taula comades que es digui historic_comandes. 
crea una funció que busqui els productes (fabricant + producte) que no existeixen a la taula productes, 
que els copïi a la taula nova historic_comandes i els esborri de comandes. 
Després crea la clau forana correcte de comandes a productes.






3. Afegeix un camp -data_vigencia- timestamp amb default current_timestamp, i un camp 
id_producte_historic integer  a la taula de productes. Posa el current_timestamp de valor al 
camp data_vigencia de tots els registres.  Crea una funció que ompli el camp id_producte_historic 
amb números seqüencials. 
Posa coma a clau primària l''id_producte_històric i com a clau única la parella de camps 
(id_fabricant, id_producte).
D''aquesta manera, quan es faci una modificació, com el preu, a la taula productes, es crearà un registre nou, 
amb una nova data de vigència, permetent així guardar la informació de producte corresponent 
a factures on aquella informació era vigent












4. El proper pas serà modificar la taula comandes per tal que es relacioni amb productes a 
través del nou id, per tant caldrà afegir aquest nou camp a comandes, escriure una funció per posar 
el nou id corresponent a la parella (fabricant, producte), esborrar la clau forana actual i 
crear la nova clau forana

