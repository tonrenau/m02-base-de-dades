/*  **************************BASE DE DADES EXAMEN BASKET */

DROP DATABASE if exists basket;
CREATE DATABASE basket;
\c basket

/* Crear les taules on s'insertaran les següents dades de la B.D. de basket */

CREATE TABLE paisos(
	id_pais serial PRIMARY KEY,
	nom_pais varchar(30) NOT NULL
);

CREATE TABLE ciutats(
	id_ciutat bigserial PRIMARY KEY,
	nom_ciutat varchar(30) NOT NULL,
	id_pais int DEFAULT NULL REFERENCES paisos(id_pais) ON DELETE SET NULL ON UPDATE SET NULL
);

CREATE TABLE clubs(
	id_club bigserial PRIMARY KEY,
	nom varchar(50) NOT NULL,
	adreca varchar(50) NOT NULL,
	id_ciutat bigint DEFAULT NULL REFERENCES ciutats(id_ciutat) ON DELETE SET NULL ON UPDATE SET NULL
	);

CREATE TABLE equips(
	id_equip bigserial PRIMARY KEY,
	nom varchar(50) NOT NULL,
	id_club bigint NOT NULL REFERENCES clubs(id_club) ON DELETE RESTRICT ON UPDATE RESTRICT
	);
	
CREATE TABLE competicions(
	id_competi serial PRIMARY KEY,
	nom varchar(30) NOT NULL,
	descripcio varchar(200) DEFAULT NULL
	);

CREATE TABLE grupscompeticio(
	numgrup bigserial PRIMARY KEY,
	competicio int NOT NULL REFERENCES competicions(id_competi) ON DELETE RESTRICT ON UPDATE RESTRICT,
	descripcio varchar(50) DEFAULT NULL
	);

CREATE TABLE temporades(
	id_temporada bigserial PRIMARY KEY,
	anyinici int NOT NULL,
	datainici date NOT NULL,
	datafinal date NOT NULL
);

CREATE TABLE partits(
	id_partit bigserial PRIMARY KEY,
	id_temporada bigint NOT NULL REFERENCES temporades(id_temporada) ON DELETE RESTRICT ON UPDATE RESTRICT,
	datapartit date NOT NULL,
	horapartit time NOT NULL,
	equiplocal bigint NOT NULL REFERENCES equips(id_equip) ON DELETE RESTRICT ON UPDATE RESTRICT,
	equipvisitant bigint NOT NULL REFERENCES equips(id_equip) ON DELETE RESTRICT ON UPDATE RESTRICT CHECK (equipvisitant != equiplocal),
	puntsequiplocal int NOT NULL,
	puntsequipvisitant int NOT NULL,
	UNIQUE (id_temporada,equiplocal,equipvisitant)	
	);
	

CREATE TABLE equiptemporadagrup(
	id_equiptempgrup bigserial PRIMARY KEY,
	id_equip bigint NOT NULL REFERENCES equips(id_equip) ON DELETE RESTRICT ON UPDATE RESTRICT,
	id_temporada bigint NOT NULL REFERENCES temporades(id_temporada) ON DELETE RESTRICT ON UPDATE RESTRICT,
	numgrup bigint NOT NULL REFERENCES grupscompeticio(numgrup) ON DELETE RESTRICT ON UPDATE RESTRICT,
	puntuacio int NOT NULL,
	data_temp date NOT NULL,
	UNIQUE (id_equip,id_temporada,numgrup)	
);


/* ****************** C A R R E G A   D E   D A D E S ***************** */

-- 2. A

INSERT INTO paisos values (DEFAULT,'HOLANDA');
INSERT INTO paisos values (DEFAULT,'ITALIA');

INSERT INTO ciutats values (DEFAULT,'AMSTERDAM',1);
INSERT INTO ciutats values (DEFAULT,'GERGEN',1);
INSERT INTO ciutats values (DEFAULT,'ROMA',2);

INSERT INTO clubs values (DEFAULT,'Los Miscos','c/Gran Via 22',2);
INSERT INTO clubs values (DEFAULT,'Els Brunos','c/Mallorca 15',1);
INSERT INTO clubs values (DEFAULT,'Ilos Pastas','c/Falsa 123',3);

INSERT INTO equips values (DEFAULT, 'MISCO CLUB', 1);
INSERT INTO equips values (DEFAULT, 'BRUNETTES', 2);
INSERT INTO equips values (DEFAULT, 'PASTAFIORI CLUB', 3);

INSERT INTO temporades values (DEFAULT,2022,'2022-01-01-','2022-10-01');
INSERT INTO temporades values (DEFAULT,2023,'2023-01-01-','2023-10-01');

INSERT INTO competicions values (DEFAULT,'Copa Rey');

INSERT INTO grupscompeticio values (DEFAULT,1);


INSERT INTO equiptemporadagrup values (DEFAULT,1,1,1,0,'2022-10-01');
INSERT INTO equiptemporadagrup values (DEFAULT,2,1,1,0,'2022-10-01');
INSERT INTO equiptemporadagrup values (DEFAULT,3,1,1,0,'2022-10-01');


INSERT INTO partits values (DEFAULT,1,'2022-02-02','18:00:00',1,2,50,75);
INSERT INTO partits values (DEFAULT,1,'2022-02-02','18:00:00',1,3,90,75);
INSERT INTO partits values (DEFAULT,1,'2022-02-05','18:00:00',2,1,50,75);
INSERT INTO partits values (DEFAULT,1,'2022-04-18','18:00:00',2,3,80,75);
INSERT INTO partits values (DEFAULT,1,'2022-04-18','18:00:00',3,2,80,75);



INSERT INTO partits values (DEFAULT,1,'2022-02-05','18:00:00',3,1,80,75);


INSERT INTO partits values (DEFAULT,1,'2022-02-02','15:00:00',2,1,50,75);
--basket=# INSERT INTO partits values (DEFAULT,1,'2022-02-02','15:00:00',2,1,50,75);
--ERROR:  duplicate key value violates unique constraint "partits_id_temporada_equiplocal_equipvisitant_key"
--DETAIL:  Key (id_temporada, equiplocal, equipvisitant)=(1, 2, 1) already exists.


INSERT INTO partits values (DEFAULT,1,'2022-02-02','15:00:00',3,3,50,75);
--basket=# INSERT INTO partits values (DEFAULT,1,'2022-02-02','15:00:00',3,3,50,75);
--ERROR:  new row for relation "partits" violates check constraint "partits_check"
--DETAIL:  Failing row contains (4, 1, 2022-02-02, 15:00:00, 3, 3, 50, 75).


INSERT INTO partits values (DEFAULT,1,'2022-02-02','15:00:00',3,3987,50,75);
--basket=# INSERT INTO partits values (DEFAULT,1,'2022-02-02','15:00:00',3890,3,50,75);
--ERROR:  CLAU FORANA equipvisitant inexistent





