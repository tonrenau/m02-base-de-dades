-- --------------------------------------------------------

--
-- Estructura de la taula comentaris
--

DROP TABLE IF EXISTS comentaris;
CREATE TABLE comentaris (
  id SERIAL NOT NULL PRIMARY KEY,
  data_comentari timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  text_comentari text  NOT NULL,
  id_usuari_comentari INT NOT NULL,
  id_publicacio INT NOT NULL
);

-- --------------------------------------------------------

--
-- Estructura de la taula likes
--

DROP TABLE IF EXISTS likes;
CREATE TABLE likes (
  id SERIAL NOT NULL PRIMARY KEY,
  data_like timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  id_usuari_like INT NOT NULL,
  id_publicacio INT NOT NULL,
  esborrat bool NOT NULL
);

-- --------------------------------------------------------

--
-- Estructura de la taula publicacions
--

DROP TABLE IF EXISTS publicacions;
CREATE TABLE publicacions (
  id SERIAL NOT NULL PRIMARY KEY,
  data_publicacio timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  imatge varchar(100)  NOT NULL,
  text_imatge text  NOT NULL,
  id_usuari INT NOT NULL,
  esborrat bool NOT NULL
);

-- --------------------------------------------------------

--
-- Estructura de la taula seguidors
--

DROP TABLE IF EXISTS seguidors;
CREATE TABLE seguidors (
  id SERIAL NOT NULL PRIMARY KEY,
  data_seguidor timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  estat Bool NOT NULL,
  id_usuari_seguit INT NOT NULL,
  id_usuari_segueix INT NOT NULL,
  UNIQUE (id_usuari_seguit, id_usuari_segueix)
);

-- --------------------------------------------------------

--
-- Estructura de la taula usuaris
--

DROP TABLE IF EXISTS usuaris;
CREATE TABLE usuaris (
  id_usuari SERIAL NOT NULL PRIMARY KEY,
  password varchar(100)  DEFAULT NULL,
  username varchar(20)  DEFAULT NULL UNIQUE,
  nom varchar(40)  DEFAULT NULL,
  cognoms varchar(40)  DEFAULT NULL,
  email varchar(100)  DEFAULT NULL UNIQUE,
  data_ultim_acces timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  data_alta timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
);

-- ------------------------------------------------------------
-- CLAUS FORANES
---------------------------------------------------------------

ALTER TABLE comentaris
ADD FOREIGN KEY (id) REFERENCES usuaris (id_usuari),
ADD FOREIGN KEY (id_publicacio) REFERENCES publicacions (id);

ALTER TABLE likes
ADD FOREIGN KEY (id_usuari_like) REFERENCES usuaris (id_usuari),
ADD FOREIGN KEY (id_publicacio) REFERENCES publicacions (id);


ALTER TABLE publicacions
ADD FOREIGN KEY (id_usuari) REFERENCES usuaris (id_usuari);

ALTER TABLE seguidors
ADD FOREIGN KEY (id_usuari_seguit) REFERENCES usuaris (id_usuari),
ADD FOREIGN KEY (id_usuari_segueix) REFERENCES usuaris (id_usuari);

	
	
insert into usuaris values (default,'xxxxxxx','paco gerte','paco','gerte','pacogerte@gmail.com',default,default );
insert into usuaris values (default,'xxxxxxx','Eva Factura','Eva','factura','Evita@gmail.com',default,default );
insert into usuaris values (default,'xxxxxxx','Lucia Peruana','Lucia','Peruana','peruanos@gmail.com',default,default );
insert into usuaris values (default,'xxxxxxx','Eva milf','eva','milf','Milfs@gmail.com',default,default );

insert into publicacions values (default, '19:00:00', 'foto mirall', 'molt feo', 1, true)
insert into publicacions values (default, '06:00:00', 'foto paisatge', 'montanya', 1, true)
insert into publicacions values (default, '19:00:00', 'foto mirall', 'k dura', 4, true)
insert into publicacions values (default, '19:00:00', 'foto friends', 'Facturando amigas', 2, true)

insert into seguidors values (default, '13:00:00', true, 1, 2 )
insert into seguidors values (default, '13:00:00', true, 1, 4 )
insert into seguidors values (default, '13:00:00', true, 1, 3 )
insert into seguidors values (default, '13:00:00', true, 2, 4 )
insert into seguidors values (default, '13:00:00', true, 3, 1 )


insert into likes values (default, '19:00:00' , 1,1, true)
insert into likes values (default, '19:00:00' , 1,2, true)

CREATE MATERIALIZED VIEW num_like AS
SELECT idlike, idpost, COUNT(*)
FROM likes JOIN posts ON idlike = idpost
GROUP BY 1,2;
SELECT 0
insta=# CREATE MATERIALIZED VIEW num_like AS
SELECT idlike, idpost, COUNT(*)
FROM likes JOIN posts ON idlike = idpost
GROUP BY 1,2;
SELECT 0
