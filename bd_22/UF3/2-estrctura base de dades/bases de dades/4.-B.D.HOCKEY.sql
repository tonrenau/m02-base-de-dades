"""
BASE DE DADES HOCKEY

	clubs: IDclub, nom, localitat, equipacio_1, equipacio_2, camp
	equips: IDequip, nom, IDcompeticio, IDclubequip
	competicio: IDcompeticio, nom, descripcio,temporada
	partits: IDpartit, IDequiplocal, IDequipvisitant, data_partit, hora_partit, gols_local,gols_visitant,IDcompeticio
	TAULA clasificacio 
"""


CREATE DATABASE Hockey 

DROP TABLE IF EXISTS clubs;
DROP TABLE IF EXISTS equips;
DROP TABLE IF EXISTS competicio;
DROP TABLE IF EXISTS partits;
DROP TABLE IF EXISTS classificacio;

CREATE TABLE clubs (
	IDclub 		serial 			PRIMARY KEY,
	nom 		varchar (30) 	NOT NULL,
	localitat 	varchar (50) 	NOT NULL,
	camp 		varchar (30) 	NOT NULL);
	

CREATE TABLE equips (
	IDequip 	serial 			PRIMARY KEY,
	nom 		varchar (30) 	NOT NULL,
	IDcompeti 	int				NOT NULL,
	IDclubequip int 			NOT NULL);
 
CREATE TABLE competicio (
	IDcompeticio 	serial 		PRIMARY KEY,
	nom 		varchar (30) 	NOT NULL,
	descripcio 	text,
	temporada 	varchar (20) 	NOT NULL);
	
CREATE TABLE partits (
	IDpartits 		serial 		PRIMARY KEY,
	IDequiplocal 	int 		NOT NULL,
	IDequipvisitant int 		NOT NULL,
	data_partit 	date 		NOT NULL,
	gols_local 		int 		NOT NULL,
	gols_visitant 	int 		NOT NULL,
	IDcomp 			int 		NOT NULL);
	
	
CREATE TABLE classificacio (
IDClassificacio 	serial 		PRIMARY KEY,
ClassCompeticio 	int,
club 				int		 	NOT NULL,
Punts 				int 		NOT NULL,
PartitsJugats 		int,
PartitsGuanyats 	int,
PartitsPerduts 		int	);


ALTER TABLE equips
ADD CONSTRAINT idclbeq_equips_fk FOREIGN KEY (idclubequip) REFERENCES clubs(idclub) 
															ON DELETE RESTRICT ON UPDATE RESTRICT,
ADD CONSTRAINT idcom_equips_fk FOREIGN KEY (IDcompeti) REFERENCES competicio(IDcompeticio) 
															ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE partits 
ADD CONSTRAINT ideqloc_partits_fk FOREIGN KEY (idequiplocal) REFERENCES clubs (idclub) 
															ON DELETE RESTRICT ON UPDATE RESTRICT,
ADD CONSTRAINT ideqvis_partits_fk FOREIGN KEY (idequipvisitant) REFERENCES clubs (idclub) 
															ON DELETE RESTRICT ON UPDATE RESTRICT,
ADD CONSTRAINT idcom_partits_fk FOREIGN KEY (idcomp) REFERENCES competicio (idcompeticio) 
													ON DELETE SET NULL ON UPDATE SET NULL;

ALTER TABLE classificacio
ADD CONSTRAINT club_fk FOREIGN KEY (club) REFERENCES clubs(idclub);

/* ****************** C À R R E G A   D E   D A D E S ***************** */

INSERT INTO clubs values (DEFAULT, 'Noia', 'Sant Sadurni danoia', 'Ataneu');
INSERT INTO clubs values (DEFAULT, 'Barça', 'Barcelona', 'Palau Barça');
INSERT INTO clubs values (DEFAULT, 'Voltrega', 'voltrega', 'Sant Hipolit');
INSERT INTO clubs values (DEFAULT, 'Caldes', 'Caldes de montbui', 'Torre roja');

INSERT INTO equips values (DEFAULT, 'Noia_OKlliga', 1, 1);
INSERT INTO equips values (DEFAULT, 'Barça_Oklliga', 1, 2);
INSERT INTO equips values (DEFAULT, 'Noia_NacCAT', 1, 1);

INSERT INTO competicio values (DEFAULT, 'OKlliga', NULL, '2022-23');
INSERT INTO competicio values (DEFAULT, 'Nacional Catalana', NULL, '2022-23');

INSERT INTO partits values (DEFAULT, 1, 2, '2023-02-18',4,3,1);
INSERT INTO partits values (DEFAULT, 3, 4, '2023-02-18',3,1,1);
INSERT INTO partits values (DEFAULT, 1, 3, '2023-02-18',5,2,1);
INSERT INTO partits values (DEFAULT, 2, 4, '2023-02-18',8,2,1);
INSERT INTO partits values (DEFAULT, 1, 3, '2023-02-18',6,3,1);

INSERT INTO classificacio values (DEFAULT, 1, 1, 9, 3,3,0);
INSERT INTO classificacio values (DEFAULT, 1, 2, 3, 2,1,1);
INSERT INTO classificacio values (DEFAULT, 1, 4, 0, 2,0,2);
INSERT INTO classificacio values (DEFAULT, 1, 3, 3, 3,1,2);

	
SELECT *, (SELECT COUNT(*) FROM partits WHERE idequiplocal = club
AND PuntsLocal > PuntsVisitant) 
+ (SELECT COUNT(*) FROM partits WHERE idequipvisitant = club 
AND PuntsVisitant > PuntsLocal) AS guanyats FROM equips;	
	
"Partits Jugats"

UPDATE classificacio SET SELECT *, (SELECT COUNT(*) FROM partits 
WHERE idequiplocal = club OR club = idequipvisitant) FROM equips;

"Partits Guanyats"

UPDATE classificacio SET partitsguanyats = (SELECT COUNT(*) FROM partits 
WHERE equiplocal = equip AND puntsLocal > puntsvisitant) 
+ (SELECT COUNT(*) FROM partits WHERE equipvisitant = equip 
AND puntsvisitant > puntslocal) FROM equips;

"Partits Perduts"

UPDATE classificacio SET partitsperduts = (SELECT COUNT(*) FROM partits 
WHERE equipLocal = equip AND puntslocal < puntsvisitant) 
+ (SELECT COUNT(*) FROM partits WHERE equipvisitant = equip 
AND puntsvisitant < puntslocal) FROM equips;	
	
	
