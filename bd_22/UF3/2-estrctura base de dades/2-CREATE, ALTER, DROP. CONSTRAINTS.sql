# Exercicis DDL (Estructura) 
CREATE DATABASE biblio

## Exercici 1

Crear una base de dades anomenada "biblioteca". Dins aquesta base de dades:
+ crear una taula anomenada llibres amb els camps següents:
	+ "ref": clau primaria numèrica que identifica els llibres i s'ha d'assignar automàticament.
	+ "titol": títol del llibre.
	+ "editorial": nom de l'editorial.
	+ "autor": identificador de l'autor, clau forana, per defecte ha de
	referenciar al primer valor de la taula autors que simbolitza l'autor "Anònim".
+ Crear una altre taula anomenada "autors" amb els següents camps:
	+ "autor": identificador de l'autor.
	+ "nom": Nom i cognoms de l'autor.
	+ "nacionalitat": Nacionalitat de l'autor.
+ Ambdues taules han de mantenir integritat referencial. Cal que si es trenca
  la integritat per delete d'autor, la clau forana del llibre apunti a
"Anònim".
+ Si es trenca la integritat per insert/update s'ha d'impedir l'alta/modificació.
+ Cal inserir l''autor "Anònim" a la taula autors.

CREATE TABLE autor (
	autor 			smallint,
	nom 			character varying (20) ,
	nacionalitat 	character varying (20),
	PRIMARY KEY (autor));

INSERT INTO autor values (1,'anonim','plana de vic');

CREATE TABLE llibres (
	ref 		serial,
	titol 		varchar(100) NOT NULL,
	editorial 	varchar (50),
	autor 		smallint not null DEFAULT 1,
	PRIMARY KEY (ref),
	FOREIGN KEY (autor) REFERENCES autor 
							ON DELETE SET DEFAULT 
							ON UPDATE RESTRICT);

## Exercici 2

A l''anterior BD biblioteca:
+ Afegirem una taula anomenada _socis_ amb els següents camps:
	+ num_soci: clau primària
	+ nom: nom i cognoms del soci.
	+ dni: DNI del soci.
+ Afegirem també una taula anomenada _prestecs_ amb els següents camps:
	+ ref: clau forana, que fa referència al llibre prestat.
	+ soci: clau forana, que fa referència al soci.
	+ data_prestec: data en que s''ha realitzat el préstec.
+ No cal que prestecs tingui clau principal ja que només és una taula de relació.
+ En eliminar un llibre cal que s'eliminin els seus préstecs automàticament.
+ No s'ha de poder eliminar un soci amb préstecs pendents.


CREATE TABLE socis (
	num_soci 	serial,
	nom			varchar(50)	NOT NULL, 
	dni 		varchar (9)	NOT NULL,
	PRIMARY KEY (num_soci));
	
	
	
CREATE TABLE prestecs (
	ref				int NOT NULL,
	num_soci		serial NOT NULL,
	data_prestec	date	NOT NULL,
	FOREIGN KEY (ref) REFERENCES llibres ON DELETE CASCADE ON UPDATE RESTRICT,
	FOREIGN KEY (num_soci) REFERENCES socis ON DELETE RESTRICT ON UPDATE RESTRICT); 
	

## Exercici 3

A la base de dades training crear una taula anomenada "rep_vendes_baixa" que
tingui la mateixa estructura que la taula rep_vendes i a més a més un camp
anomenat "baixa" que pugui contenir la data en que un representant de vendes
s''ha donat de baixa.

CREATE TABLE rep_vendes_baixa (
	baixa date) 
	INHERITS (rep_vendes);


## Exercici 4

A la base de dades training crear una taula anomenada
"productes_sense_comandes" omplerta amb aquells productes que no han tingut mai
cap comanda. A continuació esborrar de la taula "productes" aquells productes
que estan en aquesta nova taula.

Create TABLE productes_sense_comandes 
AS (SELECT * FROM productes WHERE NOT EXISTS 
	(SELECT * FROM comandes WHERE id_fabricant = fabricant AND id_producte = producte));
											 
DELETE FROM productes WHERE EXISTS (SELECT * FROM productes_sense_comandes 
WHERE productes.id_fabricant = producte_sense_comandes.id_fabricant 
AND productes.id_producte = producte_sense_comandes.id_producte);


## Exercici 5

A la base de dades training crear una taula temporal que substitueixi la taula
"clients" però només ha de contenir aquells clients que no han fet comandes i
tenen assignat un representant de vendes amb unes vendes inferiors al 110% de
la seva quota.

CREATE LOCAL TEMPORARY TABLE tebale2...;



## Exercici 6   

Escriu les sentències necessàries per a crear l'estructura de l'esquema
proporcionat de la base de dades training. Justifica les accions a realitzar en
modificar/actualitzar les claus primàries.

CREATE TABLE rep_vendes (
	num_empl 	smallint,
	nom 		varchar (30)	NOT NULL,
	edat		smallint,
	oficina_rep smallint,
	carrec 		varchar (20),
	data_contracte 	date 		NOT NULL,
	quota 		numeric (8,2),
	vendes 		numeric (8,2),
	PRIMARY KEY (num_empl),
	FOREIGN KEY (cap) REFERENCES rep_vendes (num_empl),
	FOREIGN KEY (oficina_rep) REFERENCES oficines (oficina), 
	CONSTRAINT CHECK (quota > 0) ,
	CONSTRAINT CHECK (vendes > 0) ,
	CONSTRAINT CHECK (edat >18 and edat < 65)); 

CREATE TABLE oficines ()

CREATE TABLE clients ()

CREATE TABLE comandes ()

CREATE TABLE productes ()


## Exercici 7

Escriu una sentència que permeti modificar la base de dades training
proporcionada. Cal que afegeixi un camp anomenat "nif" a la taula "clients" que
permeti emmagatzemar el NIF de cada client. També s''ha de procurar que el NIF
de cada client sigui únic.

ALTER TABLE clients
ADD nif smallint unique;

## Exercici 8

Escriu una sentència que permeti modificar la base de dades training
proporcionada. Cal que afegeixi un camp anomenat "tel" a la taula "clients" que
permeti emmagatzemar el número de telèfon de cada client. També s''ha de
procurar que aquest contingui 9 xifres.


ALTER TABLE clients 
add telf int check (telf LIKE '_________');


## Exercici 9

Escriu les sentències necessàries per modificar la base de dades training
proporcionada. Cal que s''impedeixi que els noms dels representants de vendes i
els noms dels clients estiguin buits, és a dir que ni siguin nuls ni continguin
la cadena buida.

alter Table rep_vendes 
alter nom set not null 
add check (nom <> '');

ALTER TABLE clients 
ADD CONSTRAINT CHECK empresa IS NOT NULL

## Exercici 10

Escriu una sentència que permeti modificar la base de dades training
proporcionada. Cal que procuri que l''edat dels representants de vendes no sigui
inferior a 18 anys ni superior a 65.

ALTER TABLE rep_vendes 
ADD CONSTRAINT edat_ck CHECK (edat >= 18 and edat <= 65); 

## Exercici 11

Escriu una sentència que permeti modificar la base de dades training
proporcionada. Cal que esborri el camp "carrec" de la taula "rep_vendes"
esborrant també les possibles restriccions i referències que tingui.

ALTER TABLE rep_vendes
DROP carrec CASCADE;

## Exercici 12

Escriu les sentències necessàries per modificar la base de dades training
proporcionada per tal que aquesta tingui integritat referencial. Justifica les
accions a realitzar per modificar les dades.


DELETE FROM comandes WHERE fabricant = 'qsa' AND producte = 'k47';

ALTER TABLE comandes
ADD CONSTRAINT producte_de_comanda
FOREIGN KEY (fabricant, producte) REFERENCES productes(id_fabricant, id_producte)
ON DELETE CASCADE
ON UPDATE CASCADE;


