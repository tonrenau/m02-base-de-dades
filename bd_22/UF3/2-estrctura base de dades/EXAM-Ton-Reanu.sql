'
examen SQL 
Ton Reanu 

'

CREATE DATABASE arts;

DROP TABLE IF EXISTS museus;
DROP TABLE IF EXISTS sales;
DROP TABLE IF EXISTS artistes;
DROP TABLE IF EXISTS obres;
DROP TABLE IF EXISTS ciutat;
DROP TABLE IF EXISTS paisos;

CREATE TABLE museus (
	idmuseu 	serial 		PRIMARY KEY ,
	nom 		varchar(20) NOT NULL ,
	adreça 		varchar(40) NOT NULL ,
	ciutat 		int			NOT NULL,
	anyInaugurcio int 		NOT NULL);
	
CREATE TABLE sales (
	idsala 		serial 		PRIMARY KEY,
	nomSala 	varchar(30) NOT NULL ,
	museu 		int NOT NULL );
	
CREATE TABLE artistes (
	idArtista	serial 		primary key,
	nom 		varchar(30) NOT NULL,
	anyNeixament int ,
	pais 		int);
	
CREATE TABLE obres (
	idobra 		serial 		primary key,
	AnyCreacio 	int ,
	tipus 		varchar(1) 	NOT NULL,
	titol 		varchar(25) NOT NULL,
	artista 	int			NOT NULL,
	sala 		int );
	
CREATE TABLE ciutat (
	idciutat 	serial 		primary key,
	nom_ciutat 	varchar(20) not null,
	pais 		varchar(20) not null);
	
CREATE TABLE paisos (
	idpais 		serial 		primary key ,
	nom_pais 	varchar(20) not null);
	
ALTER TABLE museus
ADD CONSTRAINT fk_nomciutat FOREIGN KEY (ciutat) REFERENCES ciutat(idciutat);

ALTER TABLE sales 
ADD CONSTRAINT fk_nomuseu FOREIGN KEY (museu) REFERENCES museus(idmuseu);
 
ALTER TABLE artistes 
ADD CONSTRAINT fk_nompais FOREIGN key (pais) REFERENCES paisos(idpais);
	
ALTER TABLE obres 
ADD CONSTRAINT fk_sala FOREIGN KEY (sala) references sales(idsala),
ADD CONSTRAINT fk_nomartista FOREIGN KEY (artista) REFERENCES artistes(idartista);
	
	
INSERT INTO ciutat values (default, 'bcn', 'catalunya');
INSERT INTO ciutat values (default, 'paris', 'frança');
INSERT INTO ciutat values (default, 'lisboa', 'portugal');
	
INSERT INTO museus values (DEFAULT, 'MANC', 'plaça madrid', 1, 1979);
INSERT INTO museus values (DEFAULT, 'arts', 'tortuga', 2, 1949);	
INSERT INTO museus values (DEFAULT, 'galeria', 'bitcho', 3, 1979);
	
INSERT INTO sales values (DEFAULT, 'edat de pedra', 1);
INSERT INTO sales values (DEFAULT, 'gabachos', 2);
INSERT INTO sales values (DEFAULT, 'golooo', 3);

INSERT INTO artistes values (DEFAULT, 'transformer', 1200, 1);
INSERT INTO artistes values (DEFAULT, 'macaco', 1745, 2);
INSERT INTO artistes values (DEFAULT, 'transformer', 2012, 3);

INSERT INTO obres values (DEFAULT, 300, 'E', 'cesaro', 1, 1);
INSERT INTO obres values (DEFAULT, 1946, 'P', 'civil war', 2, 2);
INSERT INTO obres values (DEFAULT, 1200, 'P', 'peste negra', 3, 3);

INSERT INTO paisos values (DEFAULT, 'catalunya');
INSERT INTO paisos values (DEFAULT, 'frança');
INSERT INTO paisos values (DEFAULT, 'portugal');


