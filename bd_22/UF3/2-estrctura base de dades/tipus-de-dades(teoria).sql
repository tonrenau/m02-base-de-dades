# Tipus de dades

## Numèric

+ Enters: smallint, integer, bigint
+ Camp auotincremental: smallserial, serial, bigserial
    

##  Caràcters

+ Longitud variable amb límit: varchar(n)
+ Longitud fixa (somple amb espais en blanc): char(n)
+ Longitud il·limitada: text

## Data/hora

+ Data i hora sense zona horària: timestamp
+ Data i hora amb zona horària: timestamp with time zone
+ Data: date
+ Hora sense zona horària: time
+ Hora amb zona horària: time with time zone
+ Interval de temps: interval

## Booleà: boolean

+ Valors per cert: TRUE, 't', 'true', 'y', 'yes', 'on', '1'
+ Valors per fals: FALSE, 'f', 'false', 'n', 'no', 'off', '0'

## Geomètric:

+ Punt: point, '(x,y)'
+ Recta: line, '((x1,y1),(x2,y2))'
+ Segment: lseg, '((x1,y1),(x2,y2))'
+ Rectangle: box, '((x1,y1),(x2,y2))'
+ Polígon: path, polygon, '((x1,y1),...)'
+ Camí: path '[(x1,y1),...]'
+ Cercle: circle, '<(x,y),r>' (centre i radi)

## Adreces de xarxa:

+ Xarxes IPv4 i IPv6: cidr
+ Xarxes i hosts IPv4 i IPv6: inet
+ Adreça MAC: macaddr

## UUID:

+ uuid

## XML:

+ xml: valida que el text introduït estigui ben format, sinó dóna error.




