Examen DML, DDL (Views) CASE

Pregunta 1
Amb una única sentència SQL, establiu la quota a només 1 (1€) als treballadors menors de 35 anys 
i als treballadors de New York i pugeu un 5% a la resta. Pista: feu servir CASE
UPDATE rep_vendes 
set quota = 
	case 
	when edat < 35 or oficina_rep in (SELECT oficina from oficines  where ciutat ='Nova York') then 1
	else quota * 1.05 
	end;

Pregunta 2
Esborreu els representants de vendes que tenen unes vendes inferiors a 300000 
i no són directors d''oficina ni caps d''empleats.ç

DELETE FROm rep_vendes 
where vendes <300000 and num_empl NOT IN (SELECT director from oficines ) 
and num_empl not in  (SELECT cap from rep_vendes where cap is not null)


Pregunta 3
A la base de dades training afegiu un nou client amb:

Nom d''empresa "Z Inc.".

Ha de tenir el límit de crèdit igual al límit més petit de la resta de clients. El representant de vendes ha de ser aquell representant que tingui menys clients assignats, en cas d'empat, s'assignarà el representant de vendes amb menys nombre de vendes (nombre de comandes que ha atès).

L''identificador del client ha de ser el següent valor després del valor més alt de la resta d''identificadors de clients.

Suposarem que tots els venedors tenen al menys 1 client assignat, però podria haver algun venedor sense vendes.

insert into clients 
values (( select MAX(num_empl) from rep_vendes ),'Z Inc', (SELECT rep_clie from clients group by rep_clie, 


Pregunta 4
Creeu una vista anomenada "rep_top_ofi" que llisti totes les dades dels representants de vendes que 
estan assignats a l''oficina que té més vendes.

CREATE VIEW rep_top_ofi 
as select * From rep_vendes where oficina_rep = (SELECT oficina from oficines 
											order by vendes desc LIMIT 1) ;

SELECT * from rep_top_ofi ;
 num_empl |     nom     | edat | oficina_rep |       carrec        | data_contracte | cap |   quota   |  vendes   
----------+-------------+------+-------------+---------------------+----------------+-----+-----------+-----------
      102 | Sue Smith   |   48 |          21 | Representant Vendes | 1986-12-10     | 108 | 350000.00 | 474050.00
      108 | Larry Fitch |   62 |          21 | Dir Vendes          | 1989-10-12     | 106 | 350000.00 | 361865.00
(2 rows)


Pregunta 5
Afegiu un camp a la taula "comandes" anomenat "data_lliurament" que ens permeti emmagatzemar la data 
d''entrega de la comanda i que per defecte sigui la data d''inserció més 30 dies.

ALTER TABLE comandes 
add data_lliurament date default (CURRENT_DATE +30)

Pregunta 6
Amb una única sentència, modifiqueu l''estructura de la taula "rep_vendes". Ja no ens interessa tenir la informàció del "càrrec" i en canvi volem afegir un camp anomenat "email" per emmagatzemar les adreces de correu electrònic. Cal comprovar que les adreces de correu electrònic contenen el caràcter arroba.

ALTER TABLE rep_vendes 
drop carrec,
add email varchar(50) check (email like '%@%'); 

Pregunta 7
Creeu una taula anomenada "clients_baixa" que tingui la mateixa estructura que la taula "clients" i que contingui els clients que tenen totes les seves comandes anteriors a 1990.




