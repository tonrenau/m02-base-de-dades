# Modificació

## Exercici 1:

Inseriu un nou venedor amb nom "Enric Jimenez" amb identificador 1012, oficina 18, títol "Dir Vendes", contracte d''1 de febrer del 2012, cap 101 i vendes 0.

crudtraining=# INSERT INTO  oficines values (18, 'BCN' , 'Nord',101, NULL,NULL);
INSERT 0 1
crudtraining=# SELECT * FROM oficines;
 oficina |   ciutat    | regio | director | objectiu  |  vendes   
---------+-------------+-------+----------+-----------+-----------
      22 | Denver      | Oest  |      108 | 300000.00 | 186042.00
      11 | New York    | Est   |      106 | 575000.00 | 692637.00
      12 | Chicago     | Est   |      104 | 800000.00 | 735042.00
      13 | Atlanta     | Est   |      105 | 350000.00 | 367911.00
      21 | Los Angeles | Oest  |      108 | 725000.00 | 835915.00
      69 | Lisboa      | Nord  |          |           |          
      18 | BCN         | Nord  |      101 |           |          
(7 rows)

crudtraining=# INSERT INTO rep_vendes VALUES (1012, 'Enric Jimenez', NULL, 18, 'Dir Vendes', '2012-02-01', 101, NULL, NULL);
INSERT 0 1
crudtraining=# SELECT * FROM rep_vendes ;
 num_empl |      nom      | edat | oficina_rep |       carrec        | data_contracte | cap |   quota   |  vendes   
----------+---------------+------+-------------+---------------------+----------------+-----+-----------+-----------
      105 | Bill Adams    |   37 |          13 | Representant Vendes | 1988-02-12     | 104 | 350000.00 | 367911.00
      109 | Mary Jones    |   31 |          11 | Representant Vendes | 1989-10-12     | 106 | 300000.00 | 392725.00
      102 | Sue Smith     |   48 |          21 | Representant Vendes | 1986-12-10     | 108 | 350000.00 | 474050.00
      106 | Sam Clark     |   52 |          11 | VP Vendes           | 1988-06-14     |     | 275000.00 | 299912.00
      104 | Bob Smith     |   33 |          12 | Dir Vendes          | 1987-05-19     | 106 | 200000.00 | 142594.00
      101 | Dan Roberts   |   45 |          12 | Representant Vendes | 1986-10-20     | 104 | 300000.00 | 305673.00
      110 | Tom Snyder    |   41 |             | Representant Vendes | 1990-01-13     | 101 |           |  75985.00
      108 | Larry Fitch   |   62 |          21 | Dir Vendes          | 1989-10-12     | 106 | 350000.00 | 361865.00
      103 | Paul Cruz     |   29 |          12 | Representant Vendes | 1987-03-01     | 104 | 275000.00 | 286775.00
      107 | Nancy Angelli |   49 |          22 | Representant Vendes | 1988-11-14     | 108 | 300000.00 | 186042.00
      666 | Calboo        |   68 |          21 | Representant Vendes | 1988-10-14     | 104 | 300000.00 |  88888.00
     1012 | Enric Jimenez |      |          18 | Dir Vendes          | 2012-02-01     | 101 |           |          
(12 rows)


## Exercici 2:

Inseriu un nou client "C1" i una nova comanda pel venedor anterior.
INSERT INTO clients VALUES (1000, 'C1', 666, NULL);
INSERT 0 1

SELECT * FROM clients WHERE empresa = 'C1';
 num_clie | empresa | rep_clie | limit_credit 
----------+---------+----------+--------------
     1000 | C1      |      666 |             
(1 row)

INSERT INTO comandes VALUES (111111,CURRENT_DATE,1000,666,'rei','indi','8',300);
INSERT 0 1

SELECT * FROM comandes WHERE clie = 1000;
 num_comanda |    data    | clie | rep | fabricant | producte | quantitat | import 
-------------+------------+------+-----+-----------+----------+-----------+--------
      111111 | 2023-01-30 | 1000 | 666 | rei       | indi     |         8 | 300.00
(1 row)

## Exercici 3:

Inseriu un nou venedor amb nom "Pere Mendoza" amb identificador 1013, contracte del 15 de agost del 2011 i vendes 0. La resta de camps a null.

crudtraining=> INSERT INTO rep_vendes VALUES (1013, 'Pere Mendoza', NULL, NULL,NULL,'2011-08-15',null,NULL);
INSERT 0 1
crudtraining=> SELECT * FROM rep_vendes ;
 num_empl |      nom      | edat | oficina_rep |       carrec        | data_contracte | cap |   quota   |  vendes   
----------+---------------+------+-------------+---------------------+----------------+-----+-----------+-----------
      105 | Bill Adams    |   37 |          13 | Representant Vendes | 1988-02-12     | 104 | 350000.00 | 367911.00
      109 | Mary Jones    |   31 |          11 | Representant Vendes | 1989-10-12     | 106 | 300000.00 | 392725.00
      102 | Sue Smith     |   48 |          21 | Representant Vendes | 1986-12-10     | 108 | 350000.00 | 474050.00
      106 | Sam Clark     |   52 |          11 | VP Vendes           | 1988-06-14     |     | 275000.00 | 299912.00
      104 | Bob Smith     |   33 |          12 | Dir Vendes          | 1987-05-19     | 106 | 200000.00 | 142594.00
      101 | Dan Roberts   |   45 |          12 | Representant Vendes | 1986-10-20     | 104 | 300000.00 | 305673.00
      110 | Tom Snyder    |   41 |             | Representant Vendes | 1990-01-13     | 101 |           |  75985.00
      108 | Larry Fitch   |   62 |          21 | Dir Vendes          | 1989-10-12     | 106 | 350000.00 | 361865.00
      103 | Paul Cruz     |   29 |          12 | Representant Vendes | 1987-03-01     | 104 | 275000.00 | 286775.00
      107 | Nancy Angelli |   49 |          22 | Representant Vendes | 1988-11-14     | 108 | 300000.00 | 186042.00
      666 | Calboo        |   68 |          21 | Representant Vendes | 1988-10-14     | 104 | 300000.00 |  88888.00
     1012 | Enric Jimenez |      |          18 | Dir Vendes          | 2012-02-01     | 101 |           |          
     1013 | Pere Mendoza  |      |             |                     | 2011-08-15     |     |           |          
(13 rows)

## Exercici 4:

Inseriu un nou client "C2" omplint els mínims camps.

INSERT INTO clients VALUES (2000, 'C2', 666,NULL);
INSERT 0 1


SELECT * FROM clients WHERE num_clie = 2000;
 num_clie | empresa | rep_clie | limit_credit 
----------+---------+----------+--------------
     2000 | C2      |      666 |             
(1 row)

## Exercici 5:

Inseriu una nova comanda del client "C2" al venedor "Pere Mendoza" sense especificar la llista de camps pero si la de valors.

INSERT INTO comandes VALUES (111112, CURRENT_DATE, 2000,666,'rei','c4lbs',3,400); 
INSERT 0 1

crudtraining=> SELECT * FROM comandes WHERE clie = 2000;
 num_comanda |    data    | clie | rep | fabricant | producte | quantitat | import 
-------------+------------+------+-----+-----------+----------+-----------+--------
      111112 | 2023-01-30 | 2000 | 666 | rei       | c4lbs    |         3 | 400.00
(1 row)


## Exercici 6:

Esborreu de la còpia de la base de dades el venedor afegit anteriorment anomenat "Enric Jimenez".

DELETE FROM rep_vendes WHERE num_empl = 1012;
DELETE 1
  
## Exercici 7:

Elimineu totes les comandes del client "C1" afegit anteriorment.

DELETE FROM comandes WHERE clie = 1000;
DELETE 1


## Exercici 8:

Esborreu totes les comandes d''abans del 15-11-1989.

DELETE FROM comandes WHERE data < '1989-11-15';
DELETE 4

## Exercici 9:

Esborreu tots els clients dels venedors: Adams, Jones i Roberts.

DELETE FROM clients WHERE rep_clie = ANY (SELECT num_empl FROM rep_vendes WHERE nom LIKE '%Adams%' OR nom LIKE '%Jones%' OR nom LIKE '%Roberts%');
 

## Exercici 10:

Esborreu tots els venedors contractats abans del juliol del 1988 que encara no se''ls ha assignat una quota.

DELETE FROM rep_vendes 
 WHERE data_contracte <
       '1988-07-01'
   AND quota IS NULL;
   
## Exercici 11:

Esborreu totes les comandes.

DELETE FROM comandes;

## Exercici 12:

Esborreu totes les comandes acceptades per la Sue Smith (cal tornar a disposar de la taula comandes)

DELETE FROM comandes
 WHERE rep =
       (SELECT num_empl 
          FROM rep_vendes 
         WHERE nom = 'Sue Smith');
         
## Exercici 13:

Suprimeix els clients atesos per venedors les vendes dels quals són inferiors al 80% de la seva quota.

DELETE FROM clients 
 WHERE rep_clie IN 
       (SELECT num_empl
          FROM rep_vendes 
         WHERE vendes < 0.8 * quota);

## Exercici 14:

Suprimiu els venedors els quals el seu total de comandes actual (imports) és menor que el 2% de la seva quota.

DELETE FROM rep_vendes
 WHERE 0.02 * quota >
       (SELECT SUM(import) 
          FROM comandes 
         WHERE rep = num_empl);
         
## Exercici 15:

Suprimiu els clients que no han realitzat comandes des del 10-11-1989.

DELETE FROM clients
 WHERE num_clie NOT IN 
       (SELECT clie 
          FROM comandes 
         WHERE data > '1989-11-10');
## Exercici 16:

Eleva el límit de crèdit de l''empresa Acme Manufacturing a 60000 i canvia el seu representant a Mary Jones.

UPDATE clients                                    
   SET limit_credit = 60000, 
       rep_clie = (SELECT num_empl 
                     FROM rep_vendes 
                    WHERE nom = 'Mary Jones')
 WHERE empresa = 'Acme Mfg.';
 
## Exercici 17:

Transferiu tots els venedors de l''oficina de Chicago (12) a la de Nova York (11), i rebaixa les seves quotes un 10%.

UPDATE rep_vendes
   SET oficina_rep = 11,
       quota = quota - 0.1 * quota
 WHERE oficina_rep = 12;
 
## Exercici 18:

Reassigna tots els clients atesos pels empleats 105, 106, 107, a l''empleat 102.

UPDATE clients 
   SET rep_clie = 102
 WHERE rep_clie IN 
       (105, 106, 107);
       
## Exercici 19:

Assigna una quota de 100000 a tots aquells venedors que actualment no tenen quota.

UPDATE rep_vendes
   SET quota = 100000
 WHERE quota IS NULL;

## Exercici 20:

Eleva totes les quotes un 5%.
 UPDATE rep_vendes
	SET quota = quota*1,05;

## Exercici 21:

Eleva en 5000 el límit de crèdit de qualsevol client que ha fet una comanda d''import superior a 25000.
UPDATE clients
   SET limit_credit = limit_credit + 5000
 WHERE num_clie IN 
       (SELECT clie 
          FROM comandes 
         WHERE import > 25000);
## Exercici 22:

Reassigna tots els clients atesos pels venedors les vendes dels quals són menors al 80% de les seves quotes. Reassignar al venedor 105.
UPDATE clients
   SET rep_clie = 105
 WHERE rep_clie IN
       (SELECT num_empl 
          FROM rep_vendes 
         WHERE vendes < 0.8 * quota);

## Exercici 23:

Feu que tots els venedors que atenen a més de tres clients estiguin sota de les ordres de Sam Clark (106).
UPDATE rep_vendes
   SET cap = 106
 WHERE num_empl IN
       (SELECT rep_clie  
          FROM clients
         GROUP BY rep_clie
        HAVING COUNT(*) > 3);
## Exercici 24:

Augmenteu un 50% el límit de credit d''aquells clients que totes les seves comandes tenen un import major a 30000.
UPDATE clients
   SET limit_credit = limit_credit + 0.5 * limit_credit
 WHERE 30000 < ALL 
       (SELECT import 
          FROM comandes 
         WHERE clie = num_clie);
## Exercici 25:

Disminuiu un 2% el preu d''aquells productes que tenen un estoc superior a 200 i no han tingut comandes.

UPDATE productes
   SET preu = preu - 0.02 * preu
 WHERE estoc > 200 
   AND NOT EXISTS
       (SELECT num_comanda 
          FROM comandes 
         WHERE fabricant = id_fabricant
           AND producte = id_producte);
           
## Exercici 26:

Establiu un nou objectiu per aquelles oficines en que l''objectiu actual sigui inferior a les vendes. Aquest nou objectiu serà el doble de la suma de les vendes dels treballadors assignats a l''oficina.
UPDATE oficines
   SET objectiu = 
       2 * (SELECT SUM(vendes) 
              FROM rep_vendes 
             WHERE oficina_rep = oficina)
 WHERE objectiu < vendes;
## Exercici 27:

Modifiqueu la quota dels caps d''oficina que tinguin una quota superior a la quota d'algun empleat de la seva oficina. Aquests caps han de tenir la mateixa quota que l'empleat de la seva oficina que tingui la quota més petita.
SELECT *
  FROM rep_vendes d
 WHERE num_empl IN 
       (SELECT director
          FROM oficines)
   AND quota > ANY
             (SELECT quota
                FROM rep_vendes e
               WHERE e.oficina_rep IN
                     (SELECT oficina
                        FROM oficines
                       WHERE director = d.num_empl));
## Exercici 28:

Cal que els 5 clients amb un total de compres (quantitat) més alt siguin transferits a l'empleat Tom Snyder i que se'ls augmenti el límit de crèdit en un 50%.
UPDATE clients
   SET rep_clie = 
       (SELECT num_empl 
          FROM rep_vendes 
         WHERE nom = 'Tom Snyder'), 
       limit_credit = limit_credit + 0.5 * limit_credit
WHERE num_clie IN
      (SELECT clie 
         FROM comandes 
        GROUP BY clie 
        ORDER BY SUM(quantitat) DESC
        FETCH  FIRST 5 ROWS ONLY);
## Exercici 29:

Es volen donar de baixa els productes dels que no tenen estoc i alhora no se n'ha venut cap des de l'any 89.
DELETE FROM productes
 WHERE estoc = 0
   AND NOT EXISTS
       (SELECT * 
          FROM comandes 
         WHERE id_fabricant = fabricant
           AND id_producte = producte 
           AND DATE_PART('year', data) >= 1990);

## Exercici 30:

Afegiu una oficina de la ciutat de "San Francisco", regió oest, el cap ha de ser "Larry Fitch", les vendes 0, l'objectiu ha de ser la mitja de l'objectiu de les oficines de l'oest i l'identificador de l''oficina ha de ser el següent valor després del valor més alt.
INSERT INTO oficines
VALUES ((SELECT MAX(oficina) + 1 
            FROM oficines), 
       'San Francisco', 'Oest',
       (SELECT num_empl
          FROM rep_vendes
         WHERE nom = 'Larry Fitch'),
       (SELECT AVG(objectiu)
          FROM oficines
         WHERE regio = 'Oest'),
       0);

