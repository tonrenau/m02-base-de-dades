Activitat 4. CASE. Estructura de control - Exercicis

- 1.- Llistar l''identificador i el nom dels representants de vendes. 
Mostrar un camp anomenat "result" que mostri 0 si la quota és inferior a les vendes, 
en cas contrari ha de mostrar 1 a no ser que sigui director d''oficina, en aquest cas ha de mostrar 2.

SELECT num_empl, nom, 
	CASE 
	WHEN quota < vendes then 0
	when num_empl IN (SELECT cap FROM oficines) THEN 2
	ELSE 1
	END AS result
FROM rep_vendes;

 num_empl |      nom      | result 
----------+---------------+--------
      105 | Bill Adams    |      0
      109 | Mary Jones    |      0
      102 | Sue Smith     |      0
      106 | Sam Clark     |      0
      104 | Bob Smith     |      1
      101 | Dan Roberts   |      0
      110 | Tom Snyder    |      1
      108 | Larry Fitch   |      0
      103 | Paul Cruz     |      0
      107 | Nancy Angelli |      1
      666 | Pancho Pencho |      1
(11 rows)

- 2.- Llistar tots els productes amb totes les seves dades afegint un nou camp anomenat "div". 
El camp div ha de contenir el resultat de la divisió entre el preu i les existències. 
En cas de divisió per zero, es canviarà el resultat a 0.

SELECT productes.* , 
	case 
	when estoc = 0 then 0
	ELSE preu / estoc 
	END AS div
FROM productes;
 id_fabricant | id_producte |     descripcio     |  preu   | estoc |          div           
--------------+-------------+--------------------+---------+-------+------------------------
 rei          | 2a45c       | V Stago Trinquet   |   79.00 |   210 | 0.37619047619047619048
 aci          | 4100y       | Extractor          | 2750.00 |    25 |   110.0000000000000000
 qsa          | xk47        | Reductor           |  355.00 |    38 |     9.3421052631578947
 bic          | 41672       | Plate              |  180.00 |     0 |                      0
 imm          | 779c        | Riosta 2-Tm        | 1875.00 |     9 |   208.3333333333333333
 aci          | 41003       | Article Tipus 3    |  107.00 |   207 | 0.51690821256038647343
 aci          | 41004       | Article Tipus 4    |  117.00 |   139 | 0.84172661870503597122
 bic          | 41003       | Manovella          |  652.00 |     3 |   217.3333333333333333
 imm          | 887p        | Pern Riosta        |  250.00 |    24 |    10.4166666666666667
 qsa          | xk48        | Reductor           |  134.00 |   203 | 0.66009852216748768473
 rei          | 2a44l       | Frontissa Esq.     | 4500.00 |    12 |   375.0000000000000000
 fea          | 112         | Coberta            |  148.00 |   115 |     1.2869565217391304
 imm          | 887h        | Suport Riosta      |   54.00 |   223 | 0.24215246636771300448
 bic          | 41089       | Retn               |  225.00 |    78 |     2.8846153846153846
 aci          | 41001       | Article Tipus 1    |   55.00 |   277 | 0.19855595667870036101
 imm          | 775c        | Riosta 1-Tm        | 1425.00 |     5 |   285.0000000000000000
 aci          | 4100z       | Muntador           | 2500.00 |    28 |    89.2857142857142857
 qsa          | xk48a       | Reductor           |  117.00 |    37 |     3.1621621621621622
 aci          | 41002       | Article Tipus 2    |   76.00 |   167 | 0.45508982035928143713
 rei          | 2a44r       | Frontissa Dta.     | 4500.00 |    12 |   375.0000000000000000
 imm          | 773c        | Riosta 1/2-Tm      |  975.00 |    28 |    34.8214285714285714
 aci          | 4100x       | Peu de rei         |   25.00 |    37 | 0.67567567567567567568
 fea          | 114         | Bancada Motor      |  243.00 |    15 |    16.2000000000000000
 imm          | 887x        | Retenidor Riosta   |  475.00 |    32 |    14.8437500000000000
 rei          | 2a44g       | Passador Frontissa |  350.00 |    14 |    25.0000000000000000
(25 rows)


- 3.- Afegir una condició a la sentència de l''exercici anterior per tal de nomès 
mostrar aquells productes que el valor del camp div és menor a 1.

SELECT productes.* , 
	case when estoc = 0 then 0
	ELSE preu / estoc 
	END AS div
FROM productes
WHERE (case 
		when estoc = 0 then 0
		ELSE preu / estoc 
		END)< 1;

 id_fabricant | id_producte |    descripcio    |  preu  | estoc |          div           
--------------+-------------+------------------+--------+-------+------------------------
 rei          | 2a45c       | V Stago Trinquet |  79.00 |   210 | 0.37619047619047619048
 bic          | 41672       | Plate            | 180.00 |     0 |                      0
 aci          | 41003       | Article Tipus 3  | 107.00 |   207 | 0.51690821256038647343
 aci          | 41004       | Article Tipus 4  | 117.00 |   139 | 0.84172661870503597122
 qsa          | xk48        | Reductor         | 134.00 |   203 | 0.66009852216748768473
 imm          | 887h        | Suport Riosta    |  54.00 |   223 | 0.24215246636771300448
 aci          | 41001       | Article Tipus 1  |  55.00 |   277 | 0.19855595667870036101
 aci          | 41002       | Article Tipus 2  |  76.00 |   167 | 0.45508982035928143713
 aci          | 4100x       | Peu de rei       |  25.00 |    37 | 0.67567567567567567568
(9 rows)

- 4.- Amb una única sentència SQL, i fent servir CASE, establiu la quota a només 1 (1€) 
als treballadors menors de 35 anys i als treballadors de New York i pugeu un 5% a la resta.

SELECT num_empl, nom, oficina_rep, quota, 
	case 
	when edat < 35 OR oficina_rep IN 
	(SElect oficina from oficines where ciutat = 'Nova York') then 1
	else quota * 1.05
	END
From rep_vendes;


 num_empl |      nom      | oficina_rep |   quota   |    case     
----------+---------------+-------------+-----------+-------------
      105 | Bill Adams    |          13 | 350000.00 | 367500.0000
      109 | Mary Jones    |          11 | 300000.00 |           1
      102 | Sue Smith     |          21 | 350000.00 | 367500.0000
      106 | Sam Clark     |          11 | 275000.00 | 288750.0000
      104 | Bob Smith     |          12 | 200000.00 |           1
      101 | Dan Roberts   |          12 | 300000.00 | 315000.0000
      110 | Tom Snyder    |             |           |            
      108 | Larry Fitch   |          21 | 350000.00 | 367500.0000
      103 | Paul Cruz     |          12 | 275000.00 |           1
      107 | Nancy Angelli |          22 | 300000.00 | 315000.0000
      666 | Pancho Pencho |             | 322200.00 | 338310.0000
(11 rows)




