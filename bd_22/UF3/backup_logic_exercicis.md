# Backup lògic

![diagrama backup i restauració lògica amb Postgresql](PostgreSQL_dump_restore.svg.png)

## Eines de PostgreSQL

Quan el sistema gestor de Base de Dades Relacional (SGBDR o RDBMS en anglès) és
PostgreSQL, les ordres clàssiques per fer la còpia de base de dades són:

+ `pg_dump` per copiar una base de dades del SGBDR a un fitxer script o de
  tipus arxiu 

+ `pg_dumpall` per copiar totes les bases de dades del SGBDR a un fitxer script
  SQL


Anàlogament, quan volem restaurar una base de dades si el fitxer destí ha estat un script SQL farem la restauració amb l'ordre `pg_restore`, mentre que si el fitxer destí és de tipus binari l'ordre a utilitzar serà `______`.

Respon i després *executa les
instruccions comprovant empíricament que són correctes*.


--Pregunta 1. 

Quina instrucció ens fa una còpia de seguretat lògica de la base de dades training (de tipus script SQL) ?

pg_dump training -f db.sql  --> [desde el usuari postgres]

--Pregunta 2.

El mateix d''abans però ara el destí és un directori.

pg_dump -Fd training -f db.dir

-- Pregunta 3.

El mateix d''abans però ara el destí és un fitxer tar.

+ pg_dump -Ft training -f db.tar

-- Pregunta 4. 

El mateix d''abans però ara el destí és un fitxer tar però la base de dades és mooolt gran.

+ pg_dump -Ft training | gzip > db.tgz

I com restauraries la base de dades a partir del fitxer obtingut?
+  pg_restore 

-- Pregunta 5
Imagina que vols fer la mateixa ordre d''abans però vols optimitzar el temps
d'execució. No pots esperar tant de temps en fer el backup. Quina ordre
aconseguirà treballar en paral·lel? Fes la mateixa ordre d'abans però atacant
la info de les 5 taules de la base de dades al mateix temps.

pg_dump training -F d -f directori5 -j 15 | gzip > training2.txt

-- Pregunta 6

Si no indiquem usuari ni tipus de fitxer quin és el valor per defecte?

El format per defecte es el play (format scrpit de sql)
usuari per defecte es amb que estas conectat 

-- Pregunta 7

Fes una còpia de seguretat lògica només de la taula _comandes_ de tipus script SQL.

pg_dump training -t comandes > script_comandes.sql 

-- Pregunta 8

Fes una còpia de seguretat lògica només de la taula _rep_vendes_ de tipus binari.

pg_dump training -t rep_vendes -Fc > rep_vendes.dat

-- Pregunta 9 

Elimina la taula _comandes_ de la base de dades _training_. 
Quina ordre restaura la taula _comandes_? (recorda que el fitxer és un script SQL)

(pg_dump -d training > /tmp/comandes.sql) 

DROP TABLE comandes FROM training;

restaurar: 
training=# \i /tmp/backups/comandes.sql 

postgres@i22:/home/users/inf/hisx1/a221530tr$ psql -d training < /tmp/comandes.sql

-- Pregunta 10

Fes un cop d''ull al backup tar creat a l'exercici 3 (per exemple amb l'ordre `tar xvf` ). 
Creus que a partir d'aquest fitxer es podria restaurar només una taula? Si és així, 
escriu i comprova-ho amb rep_vendes (abans hauràs d'eliminar la taula amb `DROP`)

pg_dump -Ft training > /tmp/bd_training.tar

pg_restore -d training -t rep_vendes 
-- Pregunta 11

Elimina la taula *rep_vendes* de la base de dades training. 
pg_dumb -t rep_vendes -Fc > rep_vendes.datd
DROP TABLE rep_vendes FROM training;

Quina ordre restaura la taula *rep_vendes* (recorda que el fitxer és binari)
pg_restore -d training -t rep_vendes rep_vendes.dat

-- Pregunta 12
Després de fer una còpia de tota la base de dades training en format binari,elimina-la. 

pg_dump training -Fc > /tmp/copia-training.dat 

Hi ha l''ordre de bash `dropdb` (en realitat és un wrapper de DROP DATABASE). 
I de manera anàloga també n'hi ha l'ordre de bash `createdb`.
Sense utilitzar aquesta última i amb una única ordre restaura la 
base de dades training.

DROP DATABASE training; 

CREATE DATABASE training; 

pg_restore -C -d trianing copia-trianing.dat


-- Pregunta 13

Per defecte, si n''hi ha un error quan fem la restauració `psql` ignora l''error i continuarà executant-se. Si volem que pari just quan hi hagi l'errada quina opció afegirem a l'ordre `psql` ?
	
	psql -d trianing --set ON_ERROR_STOP= ON > copia-bd.sql 

-- Pregunta 14

Si es vol que la restauració es faci en mode transacció, és a dir o es fa tot o no 
es fa res, com seria l''ordre?

psql --single-transaction training < training.sql 


-- Pregunta 15

Quina ordre em permet fer un backup de **totes** les bases de dades d'un cluster? 
Es necessita ser l'administrador de la base de dades?

pg_dumpall -f backup all db.sql

-- Pregunta 16

Quina ordre em permet restaurar **totes** les bases de dades d'un cluster 
a partir del fitxer `backup_all_db.sql`? Es necessita ser l'administrador de la 
base de dades?

psql -d postgres > backup all db.sql

-- Pregunta 17

Quina ordre em permet fer una còpia de seguretat de tipus `tar` la base de dades 
`training` que es troba a 192.168.0.123 ? Canvia la IP per la de l''ordinador del 
teu company.

pg_dump -h 192.168.0.123 -U xxxxxxxxx training > training192_168_0_123.tar

-- Pregunta 18

Es pot fer un backup de la base de dades training del server1 i que es 
restauri _on the fly_ a server2?

pg_dump -h server1 training | sql -h server2 training


