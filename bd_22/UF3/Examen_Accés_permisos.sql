Exercici 1 
	type 		database	usuari		ip addres 		(ip mask)		method		
-- Expliqueu les modificacions que s''han de fer en un sistema gestor de bases de dades postgresql per tal que:
-- Els usuaris no especificats no s''han de poder connectar a cap base de dades des de cap adreça IPv4.
	host 		all			all								reject
-- Existeixi un usuari anomenat "a10" amb contrasenya "10" guardada al sistema gestor de bases.
CREATE user a10 password '10';
-- Existeixi un altre usuari anomenat "b20" amb contrasenya "20" guardada al sistema gestor de bases de dades.

-- L'usuari "a10" ha de poder accedir a qualsevol base de dades usant la contrasenya des de qualsevol adreça IP de la xarxa 10.1.0.0/16.
	host 		all			a10			10.1.0.0/16						md5 
--L'usuari "a10" ha de poder accedir a la base de dades amb el mateix nom que l'usuari sense usar contrasenya des de l'adreça IP 10.1.2.3.
	host 		all			a10			10.1.2.3/32						trust
--L'usuari "a10" no ha de poder accedir des de qualsevol altre lloc que no siguin els anteriorment especificats.
	-
--L'usuari "b20" no ha de poder accedir a la base de dades "b" des de les adreces IP de la xarxa 10.1.2.0/24.
	host 		b			b20 		10.1.2.0/24						reject
--L'usuari "b20" ha de poder accedir a qualsevol base de dades des de qualsevol altre lloc amb contrasenya.
	host 		all			b20 		0.0.0.0/0						md5
--L'usuari "b20" no ha de poder accedir a partir del 6 d'abril de 2022
alter user b20 valid until '2022-04-06';
-- Expliqueu també com comprovaríeu el correcte funcionament.
al fitxer de postgres.conf 
	listen_adresses='*'
amb les directives a pg_hba.conf creades 
systemctl restart postgresql
 
Exercici 2

--Suposant que tenim una base de dades amb 3 taules anomenades "proveidors", "productes" i "clients" propietat de l'usuari "distribuidor. Quines sentències s'han d'executar per implementar el següent escenari:
-- Un usuari anomenat "admin" amb poders absoluts.
create user admin password 'a' superuser;
--Un usuari anomenat "gestor" que ha de poder modificar les dades i l'estructura de la base de dades, només ha de poder realitzar 2 connexions concurrents.
create user gestor password 'g' connection_limit 2
--Un usuari anomenat "lectura" que només ha de poder llegir les dades de la base de dades.
create user lectura	password 'l' ;
--Un usuari anomenat "compres" que ha de poder consultar tota la base de dades però només ha de poder modificar (afegir, editar i esborrar) les dades de les taules "proveidors" i "productes".
create user compres password 'c';
--Un usuari anomenat "vendes" que ha de poder consultar i modificar (afegir, editar i esborrar) les dades de la taula "clients", només ha de poder modificar (editar) la columna "estoc" de la taula "productes" especificant quin producte a partir de la columna "id_prod" i no ha de poder veure les dades de la taula "proveidors" (assegurar-se que se li treu aquest privilegi).
create user vendes password 'v';
--Expliqueu també com comprovaríeu el correcte funcionament.

GRANT distribuidor TO gestor;
GRANT SELECT ON ALL TABLES IN SCHEMA PUBLIC TO lectura, compres;
GRANT INSERT, UPDATE, DELETE ON proveidors, productes TO compres;
GRANT SELECT, INSERT, UPDATE, DELETE ON clients TO vendes;
GRANT UPDATE(estoc), SELECT(id_prod) ON productes TO vendes;
REVOKE SELECT ON proveidors FROM vendes;






