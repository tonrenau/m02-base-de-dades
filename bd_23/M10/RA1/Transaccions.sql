-- Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. ç
-- Finalment digues quin valor s'obtindrà amb l'últim SELECT


-- 1 ----------------------------------------------------------

INSERT INTO punts (id, valor) VALUES (10,5);
BEGIN;
UPDATE punts SET valor = 4 WHERE id = 10;
ROLLBACK;  
SELECT valor FROM punts WHERE id = 10;

Valors finals:
id | valor 
----+-------
 10 |     5

Perque el ROLLBACK fa que el update no tingui efecte, 
es fa pero sense efecte.

-- 2 --------------------------------------------------------------
INSERT INTO punts (id, valor) VALUES (20,5);
BEGIN;
UPDATE punts SET valor = 4 WHERE id = 20;
COMMIT;
SELECT valor FROM punts WHERE id = 20;

Valors finals: 
id | valor 
----+-------
 10 |     5
 20 |     4

 En aquest cas el update si que es fa i es guarda ja que es fa un commit.

--3 -------------------------------------------------------------

INSERT INTO punts (id, valor) VALUES (30,5);
BEGIN;
UPDATE punts SET valor = 4 WHERE id = 30;
SAVEPOINT a;
INSERT INTO punts (id, valor) VALUES (31,7);
ROLLBACK;
SELECT valor FROM punts WHERE id = 30;

valors finals:
 id | valor 
----+-------
 10 |     5
 20 |     4
 30 |     5

 El update es fara ja que es fa un savepoint despres per guardar el que s''ha fet abns.
 i el insert no es fara ja que hi ha un ROLLBACK i dirigeix cap al INSERT

--4 -----------------------------------------------------------

DELETE FROM punts;
INSERT INTO punts (id, valor) VALUES (40,5);
BEGIN;
UPDATE punts SET valor = 4 WHERE id = 40;
SAVEPOINT a;
INSERT INTO punts (id, valor) VALUES (41,7);
ROLLBACK TO a;
SELECT COUNT(*) FROM punts;

valors final:
 count     
-------
     1
no hi ha commit llavorens la transaccio no ha acabat.

--5 ------------------------------------------------------------

INSERT INTO punts (id, valor) VALUES (50,5);
BEGIN;
SELECT id, valor WHERE punts;
UPDATE punts SET valor = 4 WHERE id = 50;
COMMIT;
SELECT valor FROM punts WHERE id = 50;

valors finals:
 valor 
-------   
     5

no fa res ja que hi ha un error (WHERE en comptes de FROM)
-- 6 --------------------------------------------------------------

DELETE FROM punts;
INSERT INTO punts (id, valor) VALUES (60,5);
BEGIN;
UPDATE punts SET valor = 4 WHERE id = 60;
SAVEPOINT a;
INSERT INTO punts (id, valor) VALUES (61,8);
SAVEPOINT b;
INSERT INTO punts (id, valor) VALUES (61,9);
ROLLBACK TO b;
COMMIT;
SELECT SUM(valor) FROM punts;

valors finals:
 sum 
----- 
  12


  El segon insert no es fa ja que hi ha un rollback i es fa la suma de 4 + 8 