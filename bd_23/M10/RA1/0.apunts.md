
docker exec -it postgresql psql -d template1 -U postgres
# roles 
qi dona permisos:
    DBT (postgres) --> ell pot donar a totom
    propietari

    grant   permisos    ON objecte TO       user
    revoke  privilegis  ""   ""    FROM     role

    set role perm;
    set role none;

SELECT current_user; --> 
SELECT session_user;

show search_path; 
# 1- 
    create role scotonlyread
    create role trainigread
# 2- assignar roles
    grant scotonlyread to ton
    grant trainigread to ton
# 3- assignar privilegis role 
    "grant privilegis ON objectes to user" 

    scott=> grant select ON emp to scotonlyread
# 4-
    trainig=> grant update,delete on pedido to trainingread
# 5-
    select current_user, session_user
# 6-
    set role scotonlyread






