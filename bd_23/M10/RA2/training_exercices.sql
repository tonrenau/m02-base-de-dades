-- 1------------------------------------------------
-- Funció
-- existeixClient

-- Paràmetres
-- p_cliecod

-- Tasca
-- comprova si existeix el client passat com argument

-- Retorna
-- booleà
CREATE OR REPLACE FUNCTION existeixClient(p_cliecod smallint)
RETURNS BOOLEAN
AS $$
DECLARE
    v_clie smallint;
BEGIN
    SELECT num_clie INTO strict v_clie FROM cliente WHERE cliecod = p_cliecod;

    RETURN TRUE;

    EXCEPTION
        WHEN NO_DATA_FOUND THEN RETURN FALSE;
END;
$$ LANGUAGE 'plpgsql';


-- postgres=# select * from existeixClient(2000::smallint);
--  existeixclient 
-- ----------------
--  f
-- (1 row)
-- postgres=# select * from existeixClient(2120::smallint);
--  existeixclient 
-- ----------------
--  t
-- (1 row)


--2 -------------------------------------------
-- Funció
-- altaClient

-- Paràmetres
-- p_nombre ,p_repcod, p_limcred

-- Tasca
-- Donarà d’alta un client

-- Retorna
-- Missatge Client X s’ha donat d’alta correctament

-- Nota
-- si no està creada, creem una seqüència per donar valors a 
-- la clau primària. Començarà en el següent valor que hi hagi 
-- a la base de dades.

CREATE OR REPLACE FUNCTION altaClient(p_nombre varchar(30), p_repcod smallint)
RETURNS varchar(200)
AS $$
DECLARE 
    v_nombre varchar;
    v_repcod smallint;
BEGIN
    select nombre, repcod into strict v_nombre, v_repcod
    from repventa 
    where nombre = p_nombre and p_repcod = repcod;

    return 'El client '||p_nombre||' amb l''identificador '||p_repcod||' ja esta a la base de dades';

    EXCEPTION
        WHEN NO_DATA_FOUND THEN 
        RETURN 'Missatge Client '||p_nombre||' amb l''identificador '||p_repcod||' s’ha donat d’alta correctament';
    
END;
$$ LANGUAGE 'plpgsql'; 
  


/*
postgres=# select altaClient('Bill Adams'::varchar, 105::smallint);
                               altaclient                                
-------------------------------------------------------------------------
 El client Bill Adams amb l'identificador 105 ja esta a la base de dades
(1 row)
postgres=# select altaClient('Bill Adams'::varchar, 10::smallint);
                                    altaclient                                    
----------------------------------------------------------------------------------
 Missatge Client Bill Adams amb l'identificador 10 s’ha donat d’alta correctament
(1 row)
*/



--3---------------------------------------------------
-- Funció
-- stockOk

-- Paràmetres
-- p_cant , p_fabcod,p_prodcod

-- Tasca
-- Comprova que hi ha prou existències del producte demanat.

-- Retorna
-- booleà

CREATE OR REPLACE FUNCTION stockOk
(p_cant smallint, p_fabcod varchar(3), p_prodcod varchar(5))
RETURNS BOOLEAN
AS $$
DECLARE 
   existencias_actuals RECORD;
BEGIN 
    select exist into strict existencias_actuals 
    from pedido 
    where p_fabcod = fabcod and p_prodcod = prodcod;

    return (existencias_actuals >= p_cant);

  EXCEPTION 
    when NO_DATA_FOUND then 
    RETURN FALSE;

END;
$$ LANGUAGE 'plpgsql';

/*
training=# select stockOk(7::smallint,'rei'::varchar,'2a44l'::varchar);
 stockok 
---------
 t
(1 row)

training=# select stockOk(2::smallint,'rei'::varchar,'2a44l'::varchar); 
 stockok 
---------
 t
(1 row)
*/


-- 4 ------------------------------------------------------------
-- Funció
-- altaComanda

-- Paràmetres
-- Segons els exercicis anteriors i segons necessitat, 
-- definiu vosaltres els paràmetres mínims que necessita la funció, 
-- tenint en compte que cal contemplar l'opció per defecte de no posar data, 
-- amb el què agafarà la data de sistema. Si no hi és, creeu una seqüència per 
-- la clau primària de pedido.

-- Tasca
-- Per poder donar d'alta una comanda es tindrà que comprovar que existeix 
-- el client i que hi ha prou existències. En aquesta funció heu d'utilitzar 
-- les funcions  existeixClient i stockOK (recordeu de no posar select 
-- function(... ). Evidentment, s'haura de calcular el preu de l'import en 
-- funció del preu unitari i de la quantitat d'unitats.

-- Retorna
-- missatge indicant el que ha passat

create or replace function alta_comanda
(p_fecha DATE, p_cliecod INT, p_repcod INT, p_fabcod VARCHAR, p_prodcod VARCHAR, p_cant INT)
returns varchar
as $$
DECLARE
    v_preu numeric;
    txt varchar;
BEGIN
     -- Comprovació de la existència del client
      IF existeixClient(p_cliecod) THEN
        IF stockOK(p_cant,p_fabcod,p_prodcod) THEN
          -- Si hi ha estocatge suficient, obtinc el preu del producte
          SELECT  precio
          INTO STRICT v_preu
          FROM producto
          WHERE fabcod = p_fabcod and prodcod = p_prodcod;
          -- Dono d'alta la nova comanda
          INSERT INTO pedido (pednum,fecha,cliecod,repcod, fabcod,prodcod,cant,importe)
          VALUES (NEXTVAL('pednum_seq'),p_fecha, p_cliecod, p_repcod, p_fabcod, p_prodcod, p_cant, p_cant * v_preu);

          txt := 'Comanda donada d''alta correctament';
          -- actualització de l'estocatge
          UPDATE producto
      	  SET exist = (exist - p_cant)
      	  WHERE (fabcod, prodcod) = (p_fabcod, p_prodcod);
        ELSE
          txt := 'Error: No hi ha suficient estocatge disponible del producte ' || p_fabcod || p_prodcod;
        END IF;
      ELSE
        txt := 'Error: El client no existeix';
      END IF;
      return txt;
  EXCEPTION   -- cas que no existeixi el producte
    WHEN NO_DATA_FOUND THEN
      return 'Error: el producte amb codi ' || p_fabcod || p_prodcod || ' no existeix';
    END;
$$ LANGUAGE PLPGSQL;


-- 5------------------------------------------------------------
-- Funció
-- preuSenseIVA

-- Paràmetres
-- p_precio (preu amb IVA)

-- Tasca
-- Donat un preu amb IVA, es calcularà el preu sense IVA 
-- (es considera un 21 % d'IVA) i serà retornat.

CREATE OR REPLACE FUNCTION preu_sense_iva(p_precio NUMERIC)
RETURNS NUMERIC AS $$
DECLARE 
    preu_no_iva NUMERIC;
BEGIN 
   preu_no_iva := p_precio - ((p_precio * 21) /100);

   RETURN preu_no_iva;
END;
$$ LANGUAGE plpgsql;


--6--------------------------------------------------------------------
-- Funció
-- preuAmbIVA

-- Paràmetres
-- p_precio (preu sense IVA)

-- Tasca
-- Donat un preu sense IVA, es calcularà el preu amb IVA 
-- (es considera un 21 % d'IVA) i serà retornat.


CREATE OR REPLACE FUNCTION preu_amb_iva(p_precio NUMERIC)
RETURNS NUMERIC AS $$
DECLARE 
    preu_iva NUMERIC;
BEGIN 
   preu_iva := p_precio + ((p_precio * 21) /100);

   RETURN preu_iva;
END;
$$ LANGUAGE plpgsql;