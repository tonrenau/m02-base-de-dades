create or replace function documentPrestat(p_id_user int, p_format varchar)
returns varchar
as $$
DECLARE
    txt varchar;
    num int;
BEGIN
    select count(*) into strict num
    from prestec p 
    join exemplar e 
    on p.idexemplar = e.idexemplar
    join document
    on d.iddocument = e.iddocument
    where p_id_user = p.idusuari and p_format = d.format;
    /*
    if p_format = 'llibre' or p_format = 'revista' then
        txt := "Te "||num||" llibres i/o revistes prestats";
    elsif p_format = ''
    */
END;
$$ LANGUAGE plpgsql;