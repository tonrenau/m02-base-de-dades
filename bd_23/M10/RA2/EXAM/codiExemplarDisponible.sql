--Ton Renau
/*
Per facilitar la implementació d'aquesta funció, 
farem en primer lloc la funció codiExemplarDisponible, 
la qual donat un titol de document, ens retornarà:

    - Codi del primer exemplar que es pugui afagar en prèstec 
    - Un zero, en cas que no hi hagi cap exemplar disponible
en aquests moments.
*/



/*
Titols de Docs: 
    Fundamentos de bases de datos,
    Highway to hell,
    La La Land , 
    Lonely Planet traveller 
*/



Create or replace function codiExemplarDisponible
(p_titol varchar)
returns smallint
as $$
DECLARE
    v_exemplar smallint;
BEGIN
    select p.idexemplar into strict v_exemplar
    from prestec p
    join exemplar e
    on p.idexemplar = e.idexemplar 
    join document d
     on e.iddocument = d.iddocument
    where p_titol = titol and e.estat='Disponible' and datadev is not null 
    LIMIT 1;

    return v_exemplar;

  EXCEPTION
    when NO_DATA_FOUND then 
    return 0;
END;
$$ LANGUAGE plpgsql;




/*
select idexemplar, document.iddocument, titol 
from exemplar 
join document 
on exemplar.iddocument = document.iddocument;

 idexemplar | iddocument |             titol             
------------+------------+-------------------------------
          2 |          1 | Fundamentos de bases de datos
          3 |          1 | Fundamentos de bases de datos
          1 |          1 | Fundamentos de bases de datos
          5 |          2 | Highway to hell
          4 |          2 | Highway to hell
          7 |          3 | La La Land
          6 |          3 | La La Land
          8 |          4 | Lonely Planet traveller
(8 rows)
*/
