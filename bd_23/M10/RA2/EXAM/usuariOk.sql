-- Ton Renau

Create or replace function usuariOk(p_id_user int)
returns  int
as $$
DECLARE
    existeix_user int ;
    blocat boolean;
BEGIN
    select idusuari into strict existeix_user 
    from usuari 
    where p_id_user = idusuari;
    
    if existeix_user is not null then 
        select bloquejat into strict blocat 
        from usuari 
        where p_id_user = idusuari;
    end if;

    if blocat then 
      return 1;
    else 
      return 0;
    end if;

  EXCEPTION
    when NO_DATA_FOUND then 
        return 2;

END;
$$ LANGUAGE plpgsql;