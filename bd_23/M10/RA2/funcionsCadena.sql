-- Ton Renau

-- 1 --------------------------------
Create or replace function text_pla(p_line text)
returns text
as $$
DECLARE
        converted_line text;
BEGIN
        select translate(p_line, 
         'àáÀÁèéÈÉìïíÌÏÏòóÒÓùúüÙÚÜçÇñÑ', 
         'aaAAeeEEiiiIIIooOOuuuUUUcCnN')
        Into strict converted_line;

        return converted_line;
END;
$$ LANGUAGE plpgsql;

training=> select text_pla('HòlÁç');
 text_pla 
----------
 HolAc
(1 row)


--2 -------------------------------
/*
En aquest exercici practicarem la validació d'un DNI
S'ha d'implementar la funció validaDNI, la qual rep un DNI com a paràmetre formal. 
El DNI està composat per 8 xifres i una lletra, amb el què la longitud serà de 9. 
La lletra de l'DNI s'obté a partir d'un algoritme conegut com a mòdul 23.

L'algorisme és el següent:

La següent cadena conté les diferentes lletres d'un DNI: TRWAGMYFPDXBNJZSQVHLCKE.  
No s'utilitzen les lletres: I, Ñ, O, U. La I i l'O s'eviten per evitar confusions 
amb altres caràcters, com 1, l o 0. S'usen vint-i-tres lletres per ser aquest un 
nombre primer.
Es calcula el mòdul 23 de la part numèrica del DNI
El resultat es fa servir d'index sobre la cadena de lletres del dni i s'obté 
una lletra. Teniu en compte que, a diferència d'altres llenguatges informàtics, 
a SQL (i PLpgSQL per extensió) la primera posició de la cadena és la 1, no la 0.
Si la lletra que retorna la "taula" es igual a la del DNI introduit, 
la funció retornarà missatge "DNI Correcte". Si no fos així, el missatge que 
tornaria seria "DNI Incorrecte".
*/

create or replace function validar_dni(dni varchar)
returns varchar
as $$
DECLARE
        dni_str varchar;
        lletra_dni_donada varchar;
        dni_int int;
        modul int;
        leter_modul varchar;
        leters varchar := 'TRWAGMYFPDXBNJZSQVHLCKE';
BEGIN
        dni_str := SUBSTR(dni, 1, 8);
        lletra_dni_donada := upper(substr(dni, 9, 9));

        dni_int := CAST(dni_str as integer);
        modul := dni_int % 23; 
        leter_modul := substr(leters, modul+1, 1);

        if lletra_dni_donada = leter_modul then 
                return dni ||' correcte ';
        else 
                return dni||' incorrecte';
        end if;

END;
$$ LANGUAGE PLpgSQL;

--3 --------------------------------

/*
Es demana una sentència en SQL (no funció!!) per crea la taula anomenada repventa2, 
la qual contindrà tots els camps de la taula original i un de nou, 
login (usuari de connexió de cada representant), tenint en compte que el login 
se conformarà amb la inicial del nom i els caràcters del cognom. 
La longitud màxima del login serà de 8. Si el cognom fos més llarg, quedaria tallat.
Per aquest exercici, només us calen dues funcions de cadena diferents: 
una és substr (no confondre amb substring, fa el mateix però la seva sintaxi 
no és estandar!!) i l'altra l'heu de trobar vosaltres.

Per facilitar la implementació d'aquest exercici, l'he descomposat en petits exercicis,
 de manera que el que feu a cada apartat, l'utilitzareu en el següent. 
 Això vol dir que vull un codi SQL per cada apartat.

3.a. Per a cada representant, mostrar el camp  nombre i la posició on comença el cognom.

3.b. Per cada representant, mostra el el camp nombre (tal com està a la base de dades) 
     i la columna Apellido

3.c. Per cada representant, mostra el camp nombre i el seu login.

3.d. A partir del codi implementat, escriu la sentència (només 1!!) en SQL 
     per crear la taula repventa2. Evidentment començarà per "create table". 
     Utilitzeu la funció d'usuari textPla del primer exercici.

*/

create table repventa2 as
        select *, text_pla(lower(substr(nombre,1,1) || substr(nombre, POSITION(' ' IN nombre)+1,7))) as login 
        from repventa;


/*
training=> select login from repventa2;
  login   
----------
 badams
 mjones
 ssmith
 sclark
 bsmith
 droberts
 tsnyder
 lfitch
 pcruz
 nangelli
(10 rows)

*/