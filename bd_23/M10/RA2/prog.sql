do $$
declare 
    num_empleats smallint;
begin
    --get num 
    select count(*) 
    INTO num_empleats 
    FROM emp;

    -- display a message
    raise notice 'Num empleats es %', num_empleats;
end $$;


--BD scott
-- funcio mostrar usuari, li paso com a a arg el codi empleat  
-- ma de sortit el seu nom 
CREATE OR REPLACE FUNCTION mostrar_user(p_empno smallint)
RETURNS varchar
AS $$ 
DECLARE
    v_ename varchar(50); 
    v_job varchar(50);
BEGIN
    SELECT ename, job INTO strict v_ename, v_job FROM emp WHERE empno = p_empno;

    RETURN 'El nom de l''empleat amb codi ' ||p_empno||' es '||v_ename||' i treball de '||v_job;
    --CONTROL ERROR: NO_DATA_FOUND: ficar un empno que no existeixi.
    EXCEPTION 
        WHEN NO_DATA_FOUND THEN return 'ERROR: dont''t found '|| p_empno; 
END;
$$ LANGUAGE 'plpgsql';

-- desde la BD per provarala => SELECT mostrar_user(7788::smallint);
-- si surt el ERROR: "cast" es pq per defecte agafa un int i nosltres em ficant smallint i llavorens ho em d'especificar.

--\df mirar si la funcio existeix.



-- Exemple amb la var. RECORD on guarda les variables: 
create or REPLACE FUNCTION record_user(p_empno smallint)
RETURNS varchar
as $$
DECLARE 
    v_emp RECORD;
BEGIN 
    select * into strict v_emp from emp where empno = p_empno;
    return 'El nom de l''empleat amb codi ' ||p_empno||' es '||v_emp.ename||' i treball de '||v_emp.job;
EXCEPTION 
        WHEN NO_DATA_FOUND THEN return 'ERROR: dont''t found '|| p_empno; 
        WHEN TOO_MANY_ROWS THEN return 'ERROR: Sorry, I won''t do again';
END;
$$ LANGUAGE 'plpgsql';



--Control error TOO_MANY_ROWS:
create or REPLACE FUNCTION record_user(p_empno smallint)
RETURNS varchar
as $$
DECLARE 
    v_emp RECORD;
BEGIN 
    select * into strict v_emp from emp; -- where empno = p_empno;
    return 'El nom de l''empleat amb codi ' ||p_empno||' es '||v_emp.ename||' i treball de '||v_emp.job;
EXCEPTION 
        WHEN NO_DATA_FOUND THEN return 'ERROR: dont''t found '|| p_empno; 
        WHEN TOO_MANY_ROWS THEN return 'ERROR: Sorry, I won''t do again';
END;
$$ LANGUAGE 'plpgsql';








